<?php
	// Verbindung
	require("../../../db.php");
	
	if ($_POST["suchbegriff"]){
		// eingegebenes falsch geschriebenes Wort
		$input = $_POST['suchbegriff'];

		$sql = $db->query("SELECT * FROM users WHERE username LIKE '%".$input."%' AND username NOT LIKE '".USERNAME."' LIMIT 3");
		$result = "";
		if($sql->num_rows > 0){
			while($row = $sql->fetch_assoc()){
				$lev = levenshtein($input, $row['username']);
				if($lev > 0){
					$result .= "Meintest du: <a onclick='replaceOldtoNewUsername(\"".$row['username']."\");'>".$row['username']."</a>?<br>";
				}else{
					$result = "Exakter Treffer gefunden: <a onclick='replaceOldtoNewUsername(\"".$row['username']."\");'>".$row['username']."</a>";
				}
			}
		}else{
			$result .= "Die Suche ergab keinen Treffer.";
		}

		echo $result;
	}else{
		echo "Gebe doch bitte mindestens 1 Buchstaben ein :)";
	}
?>