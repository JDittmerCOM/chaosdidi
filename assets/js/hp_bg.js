$(document).ready(function() {
	if(!jQuery.browser.mobile){
		$.vegas('slideshow', {
			delay:5000,
			backgrounds:[
				{ src:'assets/images/hp_bg/cs_go.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/life_is_strange.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/evoland.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/the_evil_within.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/castle_storm.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/mirrors_edge.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/singularity.jpg', fade:1500 },
				{ src:'assets/images/hp_bg/mass_effect.jpg', fade:1500 },
			]
		})('overlay', {src:'assets/images/overlay.png'});
	};
});