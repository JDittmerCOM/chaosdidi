<?php
	// Verbindung
	require("../../../db.php");
	
	if ($_POST["suchbegriff"]){

		$query = $db->real_escape_string($_POST['suchbegriff']);
		
		// Ergebnis wird ausgegeben
		if(preg_match("/#\w+/", $_POST['suchbegriff'])){
			$query = str_replace("#", "", $query);
			$count_tags_sql = $db->query("SELECT * FROM videos WHERE tags LIKE '%".$query."%'");
		}

		if(preg_match("/@\w+/", $_POST['suchbegriff'])){
			$query = str_replace("@", "", $query);
			$count_users_sql = $db->query("SELECT * FROM users WHERE username LIKE '%".$query."%'");
		}

		if(!preg_match("/#|@\w+/", $_POST['suchbegriff'])){
			$query = explode(" ", $_POST["suchbegriff"]);
			$count_null = -1;
			$count_suche = count($query)-1;
			$query_erg = "";
			while ($count_null < $count_suche) {$count_null++; $query_erg .= "+".$query[$count_null]." ";}
			
			$count_videos_sql = $db->query("SELECT * FROM videos WHERE MATCH(title) AGAINST('".$query_erg."*' IN BOOLEAN MODE)");
		}

		echo number_format(($count_tags_sql->num_rows + $count_users_sql->num_rows + $count_videos_sql->num_rows));
	}else{
		echo "0";
	}
?>