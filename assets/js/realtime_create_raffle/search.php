<?php
	// Verbindung
	require("../../../db.php");
	
	if ($_POST["suchbegriff"]){

		$query = $db->real_escape_string($query);
		$query = htmlentities($_POST['suchbegriff'], ENT_QUOTES);
		
		// Ergebnis wird ausgegeben
		$sql = $db->query("SELECT * FROM raffle_keys WHERE game_name = '".$query."'");
		$row = $sql->fetch_assoc();

		if($sql->num_rows > 0){
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Spielname:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_name']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>USK:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_usk']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Betriebssystem/e:</strong></div>";
				echo "<div class='col-md-8'>".$row['bs']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Genre:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_genre']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Entwickler:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_developer']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Publisher:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_publisher']."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-4 text-right'><strong>Veröffentlichung:</strong></div>";
				echo "<div class='col-md-8'>".$row['game_publishing']."</div>";
			echo "</div>";
			echo "<br>";
			if(strlen($row['game_more_info']) > 0){
				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Steam-Link:</strong></div>";
					echo "<div class='col-md-8'><a href='http://store.steampowered.com/app/".$row['game_more_info']."/' target='_blank'>".$row['game_name']." <sub>(Neues Fenster)</sub></a></div>";
				echo "</div>";
			}
		}else{
			echo "<samp>".$query."</samp><br>";
			echo "<span class='text-danger'><strong>Der Suchbegriff ist ungültig!<br>Bitte kontaktiere einen Administratoren!</strong></span>";
		}
	}else{
		echo "<samp>".$query."</samp><br>";
		echo "<span class='text-danger'><strong>Der Suchbegriff ist ungültig!<br>Bitte kontaktiere einen Administratoren!</strong></span>";
	}
?>