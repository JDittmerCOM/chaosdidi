/*------------------------------
 * Copyright 2015 Pixelized
 * http://www.pixelized.cz
 *
 * Progaming theme v1.0
------------------------------*/

$(document).ready(function() {	
	
	//NAVBAR
	$('.navbar .nav > li.dropdown').mouseenter(function() {
		$(this).addClass('open');
	});
	
	$('.navbar .nav > li.dropdown').mouseleave(function() {
		$(this).removeClass('open');
	});
	
	//SCROLLING
	$("a.scroll[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$('html, body').animate({ scrollTop: $(this.hash).offset().top - 110}, 1000, function(){window.location.hash = hash;});
	});
	
	//BLOG POST - CAROUSEL
	$(".gallery-post .owl-carousel").owlCarousel({
		navigation : true,
		singleItem : true,
		pagination : false,
		autoPlay: 5000,
		slideSpeed : 500,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	//OWL CAROUSEL
	$("#jumbotron-slider").owlCarousel({
		pagination : false,
		itemsDesktop :[1200,3],
		itemsDesktopSmall :[991,2],
		items : 3,
		navigation : true,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	$('#open-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#close-search').toggleClass('show2 hidden');
		$("#navbar-search-form").toggleClass('show2 hidden animated fadeInDown');
		return false;
	});
	$('#close-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#open-search').toggleClass('show2 hidden');;
		$("#navbar-search-form").toggleClass('fadeInDown fadeOutUp');
		setTimeout(function(){
			$("#navbar-search-form").toggleClass('show2 hidden animated fadeOutUp');
		}, 500);
		return false;
	});
	
	$('.gallery-page').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'div a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
	}); 

	//TWITTER QUERY
	var twitterOptions = {
		"id": '408985435788943360',
		"domId": 'twitter-wrapper',
		"customCallback": handleTweets,
		"maxTweets": 3,
		"enableLinks": true,
		"showUser": false,
		"showImages": false,
		"showInteraction": true,
		"showPermalinks": false,
		"lang": "de"
	};

	
	function handleTweets(tweets) {
		var n = tweets.length;
		var i = 0;
		var element = document.getElementById('twitter-wrapper');
		var html = '<ul class="list-unstyled">';
		while(i < n) {
		  	html += '<li>' + tweets[i] + '</li>';
		  	i++;
		}
		html += '</ul>';
		element.innerHTML = html;
	}
	
	twitterFetcher.fetch(twitterOptions);
	
	//FORM TOGGLE
	$('#reset-password-toggle').click(function() {
		$('#reset-password').slideToggle(500);
	});
});

// FADEOUT OF A SUCCESS REPORT [start]
setTimeout(function(){
    $('#success_report').fadeOut("slow");
}, 2000);
// FADEOUT OF A SUCCESS REPORT [end]


//DELETE OWN WISH
function wish_delete(id){
	if(confirm("Bist du dir sicher, dass du dein Wunsch entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/wishbox/wish_delete.php",
			data: {
				id: id
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Dein Wunsch wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#wish_box_'+id).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: C73AD02B");
			}
		});
	}
}


//ADD COMMENT - POST
function addComment(){
    var date = $('#comment_date').val();
    var postID = $('#comment_postID').val();
    var userID = $('#comment_userID').val();
    var username = $('#comment_username').val();
    var user_avatar = $('#comment_user_avatar').val();
    var comment = $('.comment_comment').val();
    $.ajax({
        type: "POST",
        url: "assets/js/comments/addComment.php",
        data: {
    		date: date, 
    		postID: postID, 
    		userID: userID, 
    		username: username, 
    		user_avatar: user_avatar, 
    		comment: comment
        },
        success: function(html){
            $("#addComment").html("\
                <li class='comment'>\
                	<div class='avatar'><img src='http://images.chaosdidi.de/avatars/"+user_avatar+"' alt='"+username+" Avatar'></div>\
            		<div class='comment-body'>\
            			<div class='author'>\
            				<h3>"+username+"</h3>\
            				<div class='meta'><span class='date'>"+date+"</span></div>\
            			</div>\
            			<p class='message'>"+comment+"</p>\
            			<div class='reply'>\
            				<button type='button' class='btn btn-inverse' onclick='delComment();'><i class='fa fa-trash fa-fw'></i>Löschen</button>\
            			</div>\
            		</div>\
            	</li>\
            ");
        },
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: C73AD02B");
		}
    });
    
    $('.comment_comment').val("");
}


//ADD CHILDREN COMMENT
function addChildrenComment(){
    var rowID = $('#comment_children_rowID').val();
    var date = $('#comment_children_date').val();
    var children = $('#comment_children_children').val();
    var postID = $('#comment_children_postID').val();
    var userID = $('#comment_children_userID').val();
    var username = $('#comment_children_username').val();
    var user_avatar = $('#comment_children_user_avatar').val();
    var comment = $('.comment_children_comment').val();
    $.ajax({
        type: "POST",
        url: "assets/js/comments/addChildrenComment.php",
        data: {
    		rowID: rowID, 
    		date: date, 
    		children: children, 
    		postID: postID, 
    		userID: userID, 
    		username: username, 
    		user_avatar: user_avatar, 
    		comment: comment
        },
        success: function(html){
            $("#addChildrenComment_"+rowID).html("\
                <li class='comment'>\
                	<div class='avatar'><img src='http://images.chaosdidi.de/avatars/"+user_avatar+"' alt='"+username+" Avatar'></div>\
            		<div class='comment-body'>\
            			<div class='author'>\
            				<h3>"+username+"</h3>\
            				<div class='meta'><span class='date'>"+date+"</span></div>\
            			</div>\
            			<p class='message'>"+comment+"</p>\
            			<div class='reply'>\
            				<button type='button' class='btn btn-inverse' onclick='delComment();'><i class='fa fa-trash fa-fw'></i>Löschen</button>\
            			</div>\
            		</div>\
            	</li>\
            ");
        },
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: C73AD02B");
		}
    });
    
    $('.comment_children_comment').val("");
    $('#ChildrenModal_'+rowID).modal('hide');
}


//DELETE COMMENT - POST
function delComment(commentID, postID){
	if(confirm("Bist du dir sicher, das du dein Kommentar entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/comments/delComment.php",
			data: {
				commentID: commentID,
				postID: postID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Dein Kommentar wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#'+commentID).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 66E8E955");
			}
		});
	}
}


//MAKE TO PROFILE IMAGE
function make_to_profile_image(id){
	if(confirm("Bist du dir sicher, das du dieses Profilbild als Standard setzen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/profile_image/make_to_profile_image.php",
			data: {
				id: id
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Dein Profilbild wurde ersetzt.',
				        message:'',
				        info:'Wunderbar :)',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#btnMakeNEW_profile_image_'+id).remove();
				$('#btnDelNEW_profile_image_'+id).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 398542A0");
			}
		});
	}
}


//DELETE PROFILE IMAGE
function del_profile_image(id){
	if(confirm("Bist du dir sicher, das du dieses Profilbild entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/profile_image/delete_profile_image.php",
			data: {
				id: id
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Dein Profilbild wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#profile_image_'+id).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 278A82C8");
			}
		});
	}
}


//EVENT PARTICIPATE
function event_participate(eventID, userID){
	if(confirm("Bist du dir sicher, das du an diesem Event teilnehmen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/event_participate.php",
			data: {
				eventID: eventID,
				userID: userID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Deine Zusage an diesem Event wurde abgesendet.',
				        message:'',
				        info:'Vielen Dank :)',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#event_participate').prop("disabled", true);
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: B1A5FC46");
			}
		});
	}
}


//LIKE SURVEY
function surveyBTN(questionID, ID, userID){
	if(confirm("Bist du dir sicher, das du hierfür abstimmen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/survey_like.php",
			data: {
				questionID: questionID,
				ID: ID,
				userID: userID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Deine Abstimmung wurde erfolgreich abgesendet.',
				        message:'',
				        info:'Vielen Dank :)',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('button[name="sbtn_'+questionID+'"]').prop("disabled", true);
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 17AC619F");
			}
		});
	}
}


//DELETE CONVERSATION
function del_conversation(pnID){
	if(confirm("Bist du dir sicher, das du diese Konversation entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/delete_conversation.php",
			data: {
				pnID: pnID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Die Konversation wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#conversation_'+pnID).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 278A82C8");
			}
		});
	}
}


// SWITCH
// NOTI EMAIL NEWVIDEO [start]
$('input[name="noti_email_newvideo"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/noti_email_newvideo.php",
		data: {
			noti_email_newvideo: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'eMail Benachrichtigung für "neue Videos" wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// NOTI EMAIL NEWVIDEO [end]
// SECRET PROFILE IMGS [start]
$('input[name="secret_profile_imgs"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/secret_profile_imgs.php",
		data: {
			secret_profile_imgs: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'Offenlegung der eigenen Profilbilder wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// SECRET PROFILE IMGS [end]
// NOTI NEW PN [start]
$('input[name="noti_new_pn"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/noti_new_pn.php",
		data: {
			noti_new_pn: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'eMail Benachrichtigung für "neue Private Nachrichten" wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// NOTI NEW PN [end]
// NOTI NEW NOTI [start]
$('input[name="noti_new_noti"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/noti_new_noti.php",
		data: {
			noti_new_pn: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'eMail Benachrichtigung für "neue Benachrichtigungen" wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// NOTI NEW NOTI [end]


// TOOLTIP
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});