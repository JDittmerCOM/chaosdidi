<div class="col-sm-4 hidden-xs">
    <!-- SIDEBAR BOX - START -->
    <?
        // SIDEBOX FOR RAFFLES
        $raffle_sql = $db->query("SELECT * FROM raffle_keys WHERE key_status = 1");
        $raffle_row = $raffle_sql->fetch_assoc();
        echo "<div class='box sidebar-box widget-wrapper'>";
            echo "<h3>Aktuelle Verlosung</h3>";

            $image_link = $raffle_row['game_cover'];
            $raffle_alink = "<a href='KeyRaffle'><img src='//images.chaosdidi.de/raffles/".$image_link."' class='img-responsive' alt='".$raffle_row['game_name']."'></a>";
            $more_infos = "<div class='text-center'><a href='KeyRaffle' class='btn btn-primary'>Mehr Informationen</a></div>";
            if(strlen($raffle_row['end_date']) > 0){
                $raffle_date = date("d.m.", $raffle_row['start_date'])."-";
                $raffle_date .= date("d.m.Y", $raffle_row['end_date'])." ";
            }else{
                $raffle_date = date("d.m.Y", $raffle_row['start_date']);
            }

            echo "<div class='tournament'>";
                echo $raffle_alink;
                echo "<h4>Steam-Key Verlosung!</h4>";
                echo "<p>Bist du bereit für die neue Verlosung? Ja? Dann nimm sofort daran teil und Benachrichtige deine Freunde darüber!</p>";
                echo "<div class='date'>".$raffle_date."</div>";
                echo $more_infos;
            echo "</div>";
        echo "</div>";


        // SIDEBOX FOR EVENTS
        /*
        $event_sql = $db->query("SELECT * FROM events");
        $event_row = $event_sql->fetch_assoc();
        if($db->query("SELECT * FROM events")) > 0 || time() <= ($event_row['event_enddate'] + 86400)->num_rows{
            echo "<div class='box sidebar-box widget-wrapper'>";
                echo "<h3>Nächstes Event</h3>";

                $image_link = $event_row['image_link'];
                $event_alink = "<a href='Event/".$event_row['id']."'><img src='assets/images/events/".$image_link."' class='img-responsive' alt='".$event_row['title']."'></a>";
                $title = $event_row['title'];
                $description = $event_row['description'];
                $more_infos = "<div class='text-center'><a href='Event/".$event_row['id']."' class='btn btn-primary'>Mehr Informationen</a></div>";
                if(strlen($event_row['event_enddate']) > 0){
                    $event_date = date("d.m.", $event_row['event_startdate'])."-";
                    $event_date .= date("d.m.Y", $event_row['event_enddate'])." ";
                }else{
                    $event_date = date("d.m.Y", $event_row['event_startdate']);
                }

                echo "<div class='tournament'>";
                    echo $event_alink;
                    echo "<h4>".$title."</h4>";
                    echo "<p>".$description."</p>";
                    echo "<div class='date'>".$event_date."</div>";
                    echo $more_infos;
                echo "</div>";
            echo "</div>";
        }
        */
    ?>
    <!-- SIDEBAR BOX - END -->

    <!-- SIDEBAR BOX - START -->
    <div class="box sidebar-box widget-wrapper widget-matches">
    	<h3>Statistiken</h3>

        <?
            $STATS_DAYS = 7;

            $stats_TODAY_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".strtotime(date("d.m.Y"))."'");
            $stats_TODAY_row = $stats_TODAY_sql->fetch_assoc();
            $stats_30_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".(strtotime(date("d.m.Y")) - (86400 * $STATS_DAYS))."'");
            $stats_30_row = $stats_30_sql->fetch_assoc();

            $cd_member = str_replace(array(",","."), "", CD_MEMBER) - $stats_30_row['cd_member'];
            if($cd_member > 0){$cd_member2 = "<span class='badge badge-success'>+".number_format($cd_member)."</span>";}
            if($cd_member == 0){$cd_member2 = "<span class='badge badge-default'>0</span>";}
            if(!$stats_30_row['cd_member']){$cd_member2 = "<span class='badge badge-default'>-/-</span>";}
            if($cd_member < 0){$cd_member2 = "<span class='badge badge-danger'>-".str_replace("-", "", number_format($cd_member))."</span>";}

            $fb_likes = str_replace(array(",","."), "", FB_LIKES) - $stats_30_row['fb_likes'];
            if($fb_likes > 0){$fb_likes2 = "<span class='badge badge-success'>+".number_format($fb_likes)."</span>";}
            if($fb_likes == 0){$fb_likes2 = "<span class='badge badge-default'>0</span>";}
            if(!$stats_30_row['fb_likes']){$fb_likes2 = "<span class='badge badge-default'>-/-</span>";}
            if($fb_likes < 0){$fb_likes2 = "<span class='badge badge-danger'>-".str_replace("-", "", number_format($fb_likes))."</span>";}
        
            $yt_abo = str_replace(array(",","."), "", YT_ABO) - $stats_30_row['yt_abo'];
            if($yt_abo > 0){$yt_abo2 = "<span class='badge badge-success'>+".number_format($yt_abo)."</span>";}
            if($yt_abo == 0){$yt_abo2 = "<span class='badge badge-default'>0</span>";}
            if(!$stats_30_row['yt_abo']){$yt_abo2 = "<span class='badge badge-default'>-/-</span>";}
            if($yt_abo < 0){$yt_abo2 = "<span class='badge badge-danger'>-".str_replace("-", "", number_format($yt_abo))."</span>";}

            $yt_videos = str_replace(array(",","."), "", YT_VIDEOS) - $stats_30_row['yt_videos'];
            if($yt_videos > 0){$yt_videos2 = "<span class='badge badge-success'>+".number_format($yt_videos)."</span>";}
            if($yt_videos == 0){$yt_videos2 = "<span class='badge badge-default'>0</span>";}
            if(!$stats_30_row['yt_videos']){$yt_videos2 = "<span class='badge badge-default'>-/-</span>";}
            if($yt_videos < 0){$yt_videos2 = "<span class='badge badge-danger'>-".str_replace("-", "", number_format($yt_videos))."</span>";}

            $yt_aufrufe = str_replace(array(",","."), "", YT_AUFRUFE) - $stats_30_row['yt_aufrufe'];
            if($yt_aufrufe > 0){$yt_aufrufe2 = "<span class='badge badge-success'>+".number_format($yt_aufrufe)."</span>";}
            if($yt_aufrufe == 0){$yt_aufrufe2 = "<span class='badge badge-default'>0</span>";}
            if(!$stats_30_row['yt_aufrufe']){$yt_aufrufe2 = "<span class='badge badge-default'>-/-</span>";}
            if($yt_aufrufe < 0){$yt_aufrufe2 = "<span class='badge badge-danger'>-".str_replace("-", "", number_format($yt_aufrufe))."</span>";}
        ?>
        
        <a>
            <table class="table match-wrapper">
                <tbody>
                    <tr>
                        <td colspan="2"></td>
                        <td class="team-score"><i class="fa fa-clock-o fa-fw" data-toggle="tooltip" data-placement="right" <? echo "title='Veränderungen seit dem ".date("d.m.Y", (strtotime(date("d.m.Y")) - (86400 * $STATS_DAYS)))."'"; ?>></i></td>
                    </tr>
                    <tr>
                        <td class="team-name"><i class="fa fa-home fa-2x fa-fw"></i> Registrierte Mitglieder</td>
                        <td class="team-score"><? echo CD_MEMBER ?></td>
                        <td class="team-score"><? echo $cd_member2 ?></td>
                    </tr>
                    <tr>
                        <td class="team-name"><i class="fa fa-facebook-square fa-2x fa-fw"></i> Facebook Likes</td>
                        <td class="team-score"><? echo FB_LIKES ?></td>
                        <td class="team-score"><? echo $fb_likes2 ?></td>
                    </tr>
                    <tr>
                        <td class="team-name"><i class="fa fa-youtube-square fa-2x fa-fw"></i> YouTube Abonnenten</td>
                        <td class="team-score"><? echo YT_ABO ?></td>
                        <td class="team-score"><? echo $yt_abo2 ?></td>
                    </tr>
                    <tr>
                        <td class="team-name"><i class="fa fa-youtube-square fa-2x fa-fw"></i> YouTube Videos</td>
                        <td class="team-score"><? echo YT_VIDEOS ?></td>
                        <td class="team-score"><? echo $yt_videos2 ?></td>
                    </tr>
                    <tr>
                        <td class="team-name"><i class="fa fa-youtube-square fa-2x fa-fw"></i> YouTube Aufrufe</td>
                        <td class="team-score"><? echo YT_AUFRUFE ?></td>
                        <td class="team-score"><? echo $yt_aufrufe2 ?></td>
                    </tr>
                </tbody>
            </table>
        </a>
    </div>
    <!-- SIDEBAR BOX - END -->

    <!-- SIDEBAR BOX - START -->
    <div class="box sidebar-box widget-wrapper">
    	<h3>Playlists</h3>
        <ul class="nav nav-sidebar">
            <?
                $playlist_sql = $db->query("SELECT * FROM playlists ORDER BY count DESC LIMIT 10");
                while($row = $playlist_sql->fetch_assoc()){
                    echo "<li><a href='Playlist/".$row['id']."'>".htmlspecialchars($row['name'], ENT_QUOTES)."<span>".$row['count']."</span></a></li>";
                }
                echo "<br><center>";
                    echo "<a href='Playlist' class='btn btn-primary'>weitere Playlisten</a>";
                echo "</center>";
            ?>
        </ul>
    </div>
    <!-- SIDEBAR BOX - END -->

    <!-- SIDEBAR BOX - START -->
    <div class="box sidebar-box widget-wrapper">
    	<h3>Letzte Tweets</h3>
        <div id="twitter-wrapper"></div>
    </div>
    <!-- SIDEBAR BOX - END -->
</div>