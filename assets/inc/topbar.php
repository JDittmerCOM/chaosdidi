<header class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="./" class="navbar-brand visible-xs">ChaosDidi</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="./">Startseite</a></li>
				<li><a href="Playlist">Playlist</a></li>
				<li><a href="Gameslist">Gameslist</a></li>
				<li><a href="Surveys">Umfragen</a></li>
				<li><a href="Wishbox">Wunschbox</a></li>
				<li><a href="Members">Mitglieder</a></li>
				<li class="visible-xs"><a href="KeyRaffle">Verlosungen</a></li>
				<?
					if(ONLINE != '1'){
						echo "<li class='visible-xs'><a href='Login/Register'>Registrieren</a></li>";
						echo "<li class='visible-xs'><a href='Login/Login'>Einloggen</a></li>";
					}else{
						echo "<li class='visible-xs dropdown'>";
							echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'> ".USERNAME." (".NOTIFICATION_NUM.") <strong class='caret'></strong></a>";
							echo "<ul class='dropdown-menu'>";
								echo "<li><a href='MyProfile'>Mein Profil</a></li>";
								echo "<li><a href='Notifications'>Benachrichtigungen (".NOTIFICATIONS.")</a></li>";
								echo "<li><a href='PN'>Konversationen (".PN_NEW_NUM.")</a></li>";
								echo "<li><a href='MyProfile/Edit'>Profil bearbeiten</a></li>";
								if(MOD == 1){
									echo "<li><a href='Mod'>Moderatoren-Bereich</a></li>";
								}
								if(ADM == 1){
									echo "<li><a href='Admin'>Administratoren-Bereich</a></li>";
								}
							echo "</ul>";
						echo "</li>";
						echo "<li class='visible-xs'><a href='Login/Logout'>Ausloggen</a></li>";
					}
				?>
			</ul>
			<div class="pull-right navbar-buttons hidden-xs">
				<?
					if(ONLINE != '1'){
						echo "<a href='Login/Register' class='btn btn-primary'>Registrieren</a>";
						echo "<a href='Login/Login' class='btn btn-inverse'>Einloggen</a>";
					}else{
						echo "<div class='btn-group'>";
							echo "<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
								echo USERNAME." (".NOTIFICATION_NUM.") <span class='caret'></span>";
							echo "</button>";
							echo "<ul class='dropdown-menu'>";
								echo "<li><a href='MyProfile'>Mein Profil</a></li>";
								echo "<li><a href='Notifications'>Benachrichtigungen (".NOTIFICATIONS.")</a></li>";
								echo "<li><a href='PN'>Konversationen (".PN_NEW_NUM.")</a></li>";
								echo "<li role='separator' class='divider'></li>";
								echo "<li class='dropdown-header'>Kontostand: ".CREDITS." Pkt.</li>";
								echo "<li><a href='Balance/Overview'>Übersicht</a></li>";
								echo "<li><a href='Balance/AccountStatements'>Kontoauszüge</a></li>";
								echo "<li><a href='Balance/Donations'>Spenden</a></li>";
								echo "<li role='separator' class='divider'></li>";
								echo "<li><a href='MyProfile/Edit'>Profil bearbeiten</a></li>";
								if(MOD == 1 || ADM == 1){echo "<li role='separator' class='divider'></li>";}
								if(MOD == 1){echo "<li><a href='Mod'>Moderatoren-Bereich</a></li>";}
								if(ADM == 1){echo "<li><a href='Admin'>Administratoren-Bereich</a></li>";}
							echo "</ul>";
						echo "</div>";
						echo "<a href='Login/Logout' class='btn btn-inverse'>Ausloggen</a>";
					}
				?>

				<a class="navbar-icon show2" id="open-search"><i class="fa fa-search"></i></a>
				<a class="navbar-icon hidden" id="close-search"><i class="fa fa-times"></i></a>
				<div class="hidden" id="navbar-search-form">
					<form action="Search" method="post" role="search">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3"><h4 class="label-search-pos"><span class="label label-default"><span id="searchResult">0</span> Ergebnisse</span></h4></div>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" name="q" name="navbar-search" id="navbar-search" class="form-control" placeholder="Suche n. Titel, @Benutzername o. #Tag" onkeyup="searchFor(this.value);">
										<span class="input-group-btn"><button type="submit" name="sub_search" class="btn btn-primary" id="navbar-search-submit"><i class="fa fa-search"></i></button></span>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</header>