<!-- SIDEBAR BOX - START -->
<div class="box sidebar-box widget-wrapper">
	<h3>Navigation</h3>
    <a href="Mod"><b><u>Mod-Startseite</u></b></a>
    <br>
    <div class="btn-group-vertical" style="width:100%">
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bar-chart fa-fw"></i> Analytics <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Mod/Analytics/CDMembers"><i class="fa fa-bar-chart fa-fw"></i> Mitglieder</a></li>
                <li><a href="Mod/Analytics/CDVisitors"><i class="fa fa-bar-chart fa-fw"></i> Einmalige Besucher</a></li>
                <li class="divider"></li>
                <li><a href="Mod/Analytics/FBLikes"><i class="fa fa-bar-chart fa-fw"></i> FB Likes</a></li>
                <li class="divider"></li>
                <li><a href="Mod/Analytics/YTAbos"><i class="fa fa-bar-chart fa-fw"></i> YT Abonnenten</a></li>
                <li><a href="Mod/Analytics/YTVids"><i class="fa fa-bar-chart fa-fw"></i> YT Videos</a></li>
                <li><a href="Mod/Analytics/YTCounts"><i class="fa fa-bar-chart fa-fw"></i> YT Aufrufe</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-users fa-fw"></i> Benutzer <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Mod/Members/All"><i class="fa fa-users fa-fw"></i> Alle Benutzer</a></li>
                <li><a href="Mod/Members/Create"><i class="fa fa-user-plus fa-fw"></i> Benutzer erstellen</a></li>
                <li class="divider"></li>
                <li><a href="Mod/Members/Activated"><i class="fa fa-users fa-fw"></i> Aktiv. Benutzer</a></li>
                <li><a href="Mod/Members/NotUnlocked"><i class="fa fa-circle-o-notch fa-fw"></i> N. n. Freig. Benutzer</a></li>
                <li><a href="Mod/Members/Closures"><i class="fa fa-user-times fa-fw"></i> Gesp. Benutzer</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- SIDEBAR BOX - END -->