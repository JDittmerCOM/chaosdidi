<!-- SIDEBAR BOX - START -->
<div class="box sidebar-box widget-wrapper">
	<h3>Navigation</h3>
    <a href="Admin"><b><u>Admin-Startseite</u></b></a>
    <br>
    <div class="btn-group-vertical" style="width:100%">
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bar-chart fa-fw"></i> Analytics <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Analytics/CDMembers"><i class="fa fa-bar-chart fa-fw"></i> Mitglieder</a></li>
                <li><a href="Admin/Analytics/CDVisitors"><i class="fa fa-bar-chart fa-fw"></i> Einmalige Besucher</a></li>
                <li class="divider"></li>
                <li><a href="Admin/Analytics/FBLikes"><i class="fa fa-bar-chart fa-fw"></i> FB Likes</a></li>
                <li class="divider"></li>
                <li><a href="Admin/Analytics/YTAbos"><i class="fa fa-bar-chart fa-fw"></i> YT Abonnenten</a></li>
                <li><a href="Admin/Analytics/YTVids"><i class="fa fa-bar-chart fa-fw"></i> YT Videos</a></li>
                <li><a href="Admin/Analytics/YTCounts"><i class="fa fa-bar-chart fa-fw"></i> YT Aufrufe</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-file fa-fw"></i> Beiträge <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Posts/All"><i class="fa fa-file fa-fw"></i> Alle Beiträge</a></li>
                <li><a href="Admin/Posts/CreateAPI"><i class="fa fa-plus-square fa-fw"></i> Beitrag erstellen (API)</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-users fa-fw"></i> Benutzer <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Members/All"><i class="fa fa-users fa-fw"></i> Alle Benutzer</a></li>
                <li><a href="Admin/Members/Create"><i class="fa fa-user-plus fa-fw"></i> Benutzer erstellen</a></li>
                <li class="divider"></li>
                <li><a href="Admin/Members/Activated"><i class="fa fa-users fa-fw"></i> Aktiv. Benutzer</a></li>
                <li><a href="Admin/Members/NotUnlocked"><i class="fa fa-circle-o-notch fa-fw"></i> N. n. Freig. Benutzer</a></li>
                <li><a href="Admin/Members/Closures"><i class="fa fa-user-times fa-fw"></i> Gesp. Benutzer</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bullhorn fa-fw"></i> Events <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Events/All"><i class="fa fa-list fa-fw"></i> Alle Events</a></li>
                <li><a href="Admin/Events/Create"><i class="fa fa-plus-square fa-fw"></i> Event erstellen</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-list fa-fw"></i> Gameslist <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Gameslist/Edit"><i class="fa fa-pencil-square fa-fw"></i> Gameslist bearbeiten</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-list fa-fw"></i> Playlists <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Playlists/All"><i class="fa fa-list fa-fw"></i> Alle Playlisten</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-list fa-fw"></i> Umfragen <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Surveys/All"><i class="fa fa-list fa-fw"></i> Alle Umfragen</a></li>
                <li><a href="Admin/Surveys/Add"><i class="fa fa-plus-square fa-fw"></i> Umfrage hinzufügen</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ticket fa-fw"></i> Verlosungen <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Raffles/All"><i class="fa fa-list fa-fw"></i> Alle Verlosungen</a></li>
                <li><a href="Admin/Raffles/Create"><i class="fa fa-plus-square fa-fw"></i> Verlosung erstellen</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter fa-fw"></i> Wortfilter <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Wordfilter/Add"><i class="fa fa-plus-square fa-fw"></i> Wort hinzufügen</a></li>
                <li><a href="Admin/Wordfilter/Edit"><i class="fa fa-pencil-square fa-fw"></i> Wort bearbeiten</a></li>
                <li><a href="Admin/Wordfilter/Delete"><i class="fa fa-minus-square fa-fw"></i> Wort löschen</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- SIDEBAR BOX - END -->