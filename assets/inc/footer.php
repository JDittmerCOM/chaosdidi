<div class="container">
	<div class="footer-boxes">
    	<div class="row">
                    
            <!-- FOOTER ABOUT US - START -->
        	<div class="col-md-4 hidden-xs hidden-sm">
            	<div class="box">
                	<h4>Über uns</h4>
                    <p>
                        Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen.<br>
                        Meist PS3 Spiele, aber auf Wunsch auch PC.<br>
                        Enjoy it ^^
                    </p>
                </div>
        	</div>
            <!-- FOOTER ABOUT US - END -->
            
            <!-- FOOTER TAGS - START -->
            <div class="col-md-4 hidden-xs hidden-sm">
            	<div class="box footer-tags">
					<h4>Tags</h4>
                    <?
                        $tags_sql = $db->query("SELECT * FROM tags ORDER BY RAND() LIMIT 16");
                        while($row = $tags_sql->fetch_assoc()){
                            echo "<a href='Tag/".htmlspecialchars($row['tag'], ENT_QUOTES)."'><span class='text-".$row['size']."'>".$row['tag']."</span></a> ";
                        }
                    ?>               
				</div>
        	</div>
            <!-- FOOTER TAGS - END -->
            
            <!-- FOOTER LAST POSTS - START -->
            <div class="col-sm-6 col-md-4">
            	<div class="box footer-posts">
                    <h4>Letzte Beiträge</h4>
                    <?
                        echo "<ul class='list-unstyled'>";
                            $last_posts_sql = $db->query("SELECT * FROM videos WHERE planned_status = 0 ORDER BY published DESC LIMIT 0, 5");
                            while($row = $last_posts_sql->fetch_assoc()){
                                if(strlen($row['title']) >= 35){
                                    $title = substr($row['title'], 0, 35)."...";
                                }else{
                                    $title = $row['title'];
                                }
                                echo "<li><a href='Post/".$row['id']."'>".$title."</a> <span class='post-date'>".date("d.m.Y", $row['published'])."</span></li>";
                            }
                        echo "</ul>";
                    ?>	
                </div>
        	</div>
            <!-- FOOTER LAST POSTS - END -->
            
        </div>
    </div>
	<footer class="navbar navbar-default">
    	<div class="row">
        	<div class="col-md-6 hidden-xs hidden-sm">
            	<ul class="nav navbar-nav">
                    <li><a href="./">Startseite</a></li>
                    <li><a href="Bug">Fehler gefunden?</a></li>
                    <li><a href="Imprint">Impressum</a></li>
                    <li><a href="Dpd">Datenschutzerklärung</a></li>
                </ul>
            </div>
            <div class="col-md-6">
            	<p class="copyright">© ChaosDidi 2014 - <? echo date("Y"); ?> Alle Rechte vorbehalten. Design von <a href="http://pixelized.cz/" target="_blank">Pixelized Studio.</a></p>
            </div>
        </div>
    </footer>
</div>