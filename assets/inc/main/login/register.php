<? if(ONLINE == '1'){header("Location:".SERVER_NAME);} ?>

<div class="box registration-form">
	<h2>Registration</h2>
    <p>Erstelle dir ein Konto und mische dich mit in die Community ein. Hast du bereits ein Konto? Dann kannst du dich mit deinem bestehenden Benutzernamen und Passwort <a href="Login/Login">anmelden</a>.</p>
    
    <?
        if($_GET['success'] == '1'){
            echo good("
                Du hast dich <strong><u>erfolgreich</u></strong> auf ChaosDidi.de Registriert.<br>
                Bitte <strong>bestätige</strong> deine eMail, die du eben von uns erhalten hast.
            ");
            echo warning("<i class='fa fa-exclamation-triangle fa-fw'></i> Bitte schaue auch in deinem Spam-Ordner hinein!");
        }

        if(isset($_POST['sub_register'])){
            $ip = $_SERVER['REMOTE_ADDR'];
            $date = time();
            $username = $db->real_escape_string($_POST['username']);
            $email = $db->real_escape_string($_POST['email']);
            $pass1 = $db->real_escape_string($_POST['pass1']);
            $pass2 = $db->real_escape_string($_POST['pass2']);
            $activate_code = rand(100000000000,999999999999);

            $xml_utrace = file_get_contents("http://xml.utrace.de/?query=".$ip);

            $sql = $db->query("SELECT * FROM users WHERE username = '".$username."' OR email = '".$email."'");
            $row = $sql->fetch_assoc();

            //CHECK BOT
            $bot_xml = simplexml_load_file("http://xml.utrace.de/?query='".$ip."'");
            $bot_countrycode = $bot_xml->result->countrycode;

            if(!in_array($bot_countrycode, array("DE", "AT", "CH", "GB"))){
                echo bad("Es dürfen sich aus <strong>Spamschutzgründen</strong>, nur Leute aus <strong>DE</strong>, <strong>AT</strong>, <strong>CH</strong> und <strong>GB</strong> auf dieser Webseite registrieren.<br>Falls du aus einem anderen Land kommen solltest, melde dich bitte <a href='mailto:support@chaosdidi.de'>hier</a>.");
            }else{
                $check__bot = true;
            }


            //CHECK IP ADDRESS
            $IPOC = file_get_contents("http://api.ip-checklist.com/?ip=".$ip);
            $IPOC_Data = json_decode($IPOC, true);
            if($IPOC_Data['reports'] >= 1){
                echo bad("Deine <strong>IP-Adresse</strong> hat einen Eintrag in der IP-Checklist, wodurch diese <strong><u>blockiert</u></strong> wird.");
            }else{
                $check__ip = true;
            }


            //CHECK USERNAME
            if(strlen($username) < 4){
                echo bad("Dein <strong>Benutzername</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 4 Zeichen</u></strong> verwenden.");
            }else{
                $check__username = true;
            }

            if($username == $row['username']){
                echo bad("Dein <strong>Benutzername</strong> ist bereits <strong><u>vergeben</u></strong>.");
            }else{
                $check__username2 = true;
            }

            if(!preg_match("/^[a-zA-Z0-9_-]*$/", $username)){
                echo bad("Dein <strong>Benutzername</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong><u>a-z A-Z 0-9 _ -</u></strong> verwenden.");
            }else{
                $check__username3 = true;
            }

            $IPOC = file_get_contents("http://api.ip-checklist.com/?username=".$username);
            $IPOC_Data = json_decode($IPOC, true);
            if($IPOC_Data['username']['reports'] >= 1){
                echo bad("Dein <strong>Benutzername</strong> hat einen Eintrag in der IP-Checklist, wodurch dieser <strong><u>blockiert</u></strong> wird.");
            }else{
                $check__username4 = true;
            }


            //CHECK EMAIL
            if(strlen($email) < 8){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__email = true;
            }

            if($email == $row['email']){
                echo bad("Deine <strong>eMail Adresse</strong> ist bereits <strong><u>vergeben</u></strong>.");
            }else{
                $check__email2 = true;
            }

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $email)){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong><u>a-z A-Z 0-9 _ . - @</u></strong> verwenden.");
            }else{
                $check__email3 = true;
            }

            $IPOC = file_get_contents("http://api.ip-checklist.com/?email=".$email);
            $IPOC_Data = json_decode($IPOC, true);
            if($IPOC_Data['email']['reports'] >= 1){
                echo bad("Deine <strong>eMail Adresse</strong> hat einen Eintrag in der IP-Checklist, wodurch diese <strong><u>blockiert</u></strong> wird.");
            }else{
                $check__email4 = true;
            }


            //CHECK PASSWORD
            if(strlen($pass1) < 8){
                echo bad("Dein <strong>Passwort</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password = true;
            }

            if($pass1 != $pass2){
                echo bad("Die beiden <strong>Passwörter</strong> sind <strong><u>ungleich</u></strong>.");
            }else{
                $check__password2 = true;
            }

            if($check__password == true && $check__password2 == true){
                $pass1 = crypt($pass1, CRYPT_PW);
            }


            if(
                $check__ip == true
                && $check__username == true && $check__username2 == true && $check__username3 == true && $check__username4 == true
                && $check__email == true && $check__email2 == true && $check__email3 == true && $check__email4 == true
                && $check__password == true && $check__password2 == true
                && $check__bot == true){

                //CHECK IP-ADDRESS
                $usersIP_sql = $db->query("SELECT * FROM users WHERE username NOT LIKE '%".$username."%' ORDER BY id ASC");
                while($row = $usersIP_sql->fetch_assoc()){
                    $c1 = explode(".", $ip);
                    $c2 = explode(".", $row['ip']);

                    similar_text($c1[0], $c2[0], $erg1);
                    similar_text($c1[1], $c2[1], $erg2);
                    similar_text($c1[2], $c2[2], $erg3);
                    similar_text($c1[3], $c2[3], $erg4);

                    $erg = ($erg1 + $erg2 + $erg3 + $erg4) / 4;

                    if($erg >= 75){
                        $erg_username .= $row['username'];
                        $datasIP .= "<div class='row'>";
                            $datasIP .= "<div class='col-md-3 text-right'>".$row['username']."</div>";
                            $datasIP .= "<div class='col-md-9'>".$row['ip']."</div>";
                        $datasIP .= "</div>";
                    }
                }

                if(strlen($erg_username) < 1){goto end;}
                
                $to = "dipa@chaosdidi.de";
                $subject = "[Duplicate IP] Gefahr auf Doppelaccount";
                $message = '
                <html>
                    <head>
                        <title>[Duplicate IP] Gefahr auf Doppelaccount</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container">
                            Es besteht die Möglichkeit, dass <strong>'.$username.'</strong> ein Doppelaccount ist.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername</strong></div>
                                <div class="col-md-9"><strong>IP-Adresse</strong></div>
                            </div>
                            '.$datasIP.'
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to,$subject,$message,$headers);

                end:

                $db->query("INSERT INTO users (ip,
                                               `date`,
                                               username,
                                               email,
                                               password,
                                               activate_code) 
                                VALUES ('".$ip."',
                                        '".$date."',
                                        '".$username."',
                                        '".$email."',
                                        '".$pass1."',
                                        '".$activate_code."')");

                $to1 = $email;
                $subject1 = "Deine Registrierung auf ChaosDidi.de";
                $message1 = '
                <html>
                    <head>
                        <title>Deine Registrierung auf ChaosDidi.de</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container">
                            <img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
                            <br><br>
                            Vielen Dank für deine Registrierung auf ChaosDidi.de :)
                            <br><br>
                            Damit du dich nun auch Einloggen kannst, musst du erst deinen Account aktivieren. Klicke dafür auf den unten stehenden Link.<br>
                            <a href="http://chaosdidi.de/Activate/'.$activate_code.'">http://chaosdidi.de/Activate/'.$activate_code.'</a>
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong><u>Deine Logindaten:</u></strong></div>
                                <div class="col-md-9"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
                                <div class="col-md-9">'.$username.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Passwort:</strong></div>
                                <div class="col-md-9">'.$pass2.'</div>
                            </div>
                            <hr/>
                            Du hast Fragen oder Anregungen? Dann schreibe unseren Support an: <a href="mailto:support@chaosdidi.de">support@chaosdidi.de</a><br>
                            Du hast Ideen, Tipps, Spielewünsche oder Ähnliches? Dann äußere deine Wünsche doch in unserer <a href="http://chaosdidi.de/Wishbox">Wunschbox</a>.
                            <br><br>
                            Viel Spaß wünscht dir,<br>
                            Das ChaosDidi Team<br><br>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers1 = "MIME-Version: 1.0" . "\n";
                $headers1 .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers1 .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to1,$subject1,$message1,$headers1);


                $to2 = "admin@chaosdidi.de";
                $subject2 = "[Neues Mitglied] Ein neues Mitglied hat sich auf ChaosDidi registriert";
                $message2 = '
                <html>
                    <head>
                        <title>[Neues Mitglied] Ein neues Mitglied hat sich auf ChaosDidi registriert</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container">
                            Ein <strong>neues Mitglied</strong> hat sich heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> registriert.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>utrace:</strong></div>
                                <div class="col-md-9">'.$xml_utrace.'</div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
                                <div class="col-md-9">'.$ip.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
                                <div class="col-md-9">'.$username.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>eMail Adresse:</strong></div>
                                <div class="col-md-9">'.$email.'</div>
                            </div>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers2 = "MIME-Version: 1.0" . "\n";
                $headers2 .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers2 .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to2,$subject2,$message2,$headers2);

                header("Location:".SERVER_NAME."Login/Register/S1");
            }
        }
    ?>

    <form method="post">
    	<div class="form-group">
            <label for="reg_username">Benutzername</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
                <input type="text" name="username" class="form-control" id="reg_username" placeholder="Benutzername" <? echo "value='".$username."'"; ?> maxlength="35" required>
            </div>
        </div>
        <div class="form-group">
            <label for="reg_email">eMail Adresse</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></div>
                <input type="email" name="email" class="form-control" id="reg_email" placeholder="eMail Adresse" <? echo "value='".$email."'"; ?> maxlength="100" required>
            </div>
        </div>
        <div class="form-group">
            <label for="reg_pass">Passwort</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                <input type="password" name="pass1" class="form-control" id="reg_pass" placeholder="Passwort" maxlength="35" required>
            </div>
        </div>
        <div class="form-group">
            <label for="reg_pass2">Passwort erneut eingeben</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                <input type="password" name="pass2" class="form-control" id="reg_pass2" placeholder="Passwort" maxlength="35" required>
            </div>
        </div>
        <button type="submit" name="sub_register" class="btn btn-primary">Registrieren</button>
    </form>
</div>