<div class="box">
	<h2><a href="Wishbox">Wunschbox</a> <i class="fa fa-arrow-right fa-fw"></i> Alle öffentlichen Wünsche</h2>

	<hr/>
	<?
		if(ADM != '1'){
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a>";
		}else{
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a><br>";
			echo "<a href='Wishbox/PrivateAll'>Alle aktuell privaten Wünsche einsehen</a>";
		}
	?>
	<hr/>

	<?
		$sql = $db->query("SELECT * FROM wishbox WHERE status = '1' ORDER BY id DESC");
		while($row = $sql->fetch_assoc()){
			$thumbs_total = 100 / ($row['good'] + $row['bad']);
			$thumbs_good = round($thumbs_total * $row['good'], 0, PHP_ROUND_HALF_EVEN);
			$thumbs_bad = round($thumbs_total * $row['bad'], 0, PHP_ROUND_HALF_EVEN);

			//CHECK HAVE'T REVIEW
			$thumbs_btn_sql = $db->query("SELECT * FROM wishbox_reviews WHERE wishID = '".$row['id']."' AND username LIKE '%".USERNAME."%'");
			
			//CHECK DELETE BUTTON
			$own_wish_sql = $db->query("SELECT * FROM wishbox WHERE id = '".$row['id']."' AND username LIKE '%".USERNAME."%'");

			echo "<div class='box' id='wish_box_".$row['id']."'>";
				echo "<blockquote>";
					echo $row['wish_message'];
					echo "<footer>".$row['username']."</footer>";
				echo "</blockquote>";

				echo "<div class='progress'>";
					echo "<div class='progress-bar progress-bar-success progress-bar-striped' style='width: ".$thumbs_good."%'>";
						echo $thumbs_good."%&ensp;<i class='fa fa-thumbs-up'></i>";
					echo "</div>";
					echo "<div class='progress-bar progress-bar-danger progress-bar-striped' style='width: ".$thumbs_bad."%'>";
						echo $thumbs_bad."%&ensp;<i class='fa fa-thumbs-down'></i>";
					echo "</div>";
				echo "</div>";


				if(ONLINE == '1'){
					if($thumbs_btn_sql->num_rows > 0 || $own_wish_sql->num_rows == 1){
						echo "<button type='button' class='btn btn-success' disabled>Gute Idee&ensp;<i class='fa fa-thumbs-up'></i></button>";
						echo "&emsp;";
						echo "<button type='button' class='btn btn-danger' disabled>Schlechte Idee&ensp;<i class='fa fa-thumbs-down'></i></button>";
					}else{
						echo "<button type='button' id='wishbox_thumbs_up_".$row['id']."' class='btn btn-success' onclick='wishbox_thumbs_up(".$row['id'].")'>Gute Idee&ensp;<i class='fa fa-thumbs-up'></i></button>";
						echo "&emsp;";
						echo "<button type='button' id='wishbox_thumbs_down_".$row['id']."' class='btn btn-danger' onclick='wishbox_thumbs_down(".$row['id'].")'>Schlechte Idee&ensp;<i class='fa fa-thumbs-down'></i></button>";
					}
					if(ADM == '1' || $own_wish_sql->num_rows == 1){
						echo "<div class='pull-right'>";
							echo "<button type='button' id='wish_delete' class='btn btn-inverse' onclick='wish_delete(".$row['id'].")'>Löschen</button>";
						echo "</div>";
					}
				}else{
					echo bad("Nur als Mitglied, kannst du Ideen bewerten. Bitte <a href='Login/Login'>logge</a> dich dafür ein :)");
				}
			echo "</div>";
		}
	?>
</div>