<?
    $eventID = $_GET['id'];
    $event_sql = mysqli_query($db, "SELECT * FROM events WHERE id = '".$eventID."'");
    $event_row = mysqli_fetch_assoc($event_sql);

    $eventBTN_sql = mysqli_query($db, "SELECT * FROM events_participate WHERE eventID = '".$eventID."' AND userID = '".ID."'");

    if(strlen($event_row['event_enddate']) > 0){
        $event_date = date("d.m.", $event_row['event_startdate'])."-";
        $event_date .= date("d.m.Y", $event_row['event_enddate'])." ";
    }else{
        $event_date = date("d.m.Y", $event_row['event_startdate']);
    }
?>

<?
    echo "<div class='box tournament-detail'>";
        echo "<div class='row'>";
            echo "<div class='col-xs-9'>";
                echo "<h2>".$event_row['title']."</h2>";
                echo "<p class='date'>".$event_date."<br>".$event_row['place']."</p>";
                if(mysqli_num_rows($eventBTN_sql) == 0 && ONLINE == '1'){
                    echo "<button type='button' id='event_participate' class='btn btn-primary' onclick='event_participate(".$eventID.",".ID.");'>Ich nehme am Event teil.</button>";
                }else{
                    echo "<button type='button' class='btn btn-primary' disabled>Ich nehme am Event teil.</button>";
                }
            echo "</div>";

            echo "<div class='col-xs-3'>";
                echo "<img src='assets/images/events/".$event_row['image_link']."' class='img-responsive center-block' alt='event_image'>";
            echo "</div>";
        echo "</div>";
        echo "<p class='countdown-info'>Event beginnt in:</p>";
        echo "<div class='countdown'></div>";
    echo "</div>";

    echo "<div class='box tournament-videos'>";
        // MATCH VIDEOS - START
        echo "<div class='match-videos'>";
            echo "<div class='row'>";
                echo "<div class='col-sm-12'>";
                    echo "<div class='flex-video widescreen'>";
                        echo "<iframe width='1280' height='720' src='https://www.youtube.com/embed/".$event_row['video_link']."?rel=0' allowfullscreen></iframe>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";
        echo "</div>";
    echo "</div>";
?>

<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var countdown_date = '<? echo date("Y/m/d", $event_row["event_startdate"]); ?>';
    $('.countdown').countdown(countdown_date, function(event) {
        var $this = $(this).html(event.strftime(''
            + '<div><span class="countdown-number">%w</span> <span class="countdown-title">Wochen</span></div> '
            + '<div><span class="countdown-number">%d</span> <span class="countdown-title">Tage</span></div> '
            + '<div><span class="countdown-number">%H</span> <span class="countdown-title">Stunden</span></div> '
            + '<div><span class="countdown-number">%M</span> <span class="countdown-title">Minuten</span></div> '
            + '<div><span class="countdown-number">%S</span> <span class="countdown-title">Sekunden</span></div>'));
    });
});
</script>