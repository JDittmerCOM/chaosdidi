<?
	if(isset($_POST['message_sub'])){
		$pnID = hash("adler32", time().rand(100,999));
		$date = time();
		$ip = IP;
		$fromID = ID;
		$toID = uda::USERNAME($_POST['username'], "id");
		$subject = $db->real_escape_string($_POST['subject']);
		$message = $_POST['message'];
			$message = trim($message);
			$message = nl2br($message);
			$DB_message = $db->real_escape_string($message);

		$original_message = $message;

		// CHECK USERNAME (FROM/TO) [start]
		if($_POST['username'] == USERNAME){
			goto toID_FAIL;
		}
		// CHECK USERNAME (FROM/TO) [end]

		// WORDFILTER [start]
	    $wordfilter_sql = $db->query("SELECT * FROM wordfilter");
	    $bad_word = array();
	    $word_replace = array();
	    while ($row = $wordfilter_sql->fetch_assoc()) {
	       $bad_word[] = $row['word'];
	       $word_replace[] = $row['word_replace'];
	    }

	    $matches = array();
	    $matchFound = preg_match_all("/\b(" . implode($bad_word,"|") . ")\b/i", $message, $matches);

	    $users_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
	    $users_row = $users_sql->fetch_assoc();

	    if ($matchFound) {
	        $words = array_unique($matches[0]);
	        foreach($words as $word) {
	            if($users_row['reports'] >= 3){
	                $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

	                $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");

	                $mail_to = "ban@chaosdidi.de";
	                $mail_subject = "[User Banned] ".USERNAME." wurde gesperrt";
	                $mail_message = '
	                <html>
	                    <head>
	                        <title>[User Banned] '.USERNAME.' wurde gesperrt</title>
	                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	                    </head>
	                    <body>
	                        <br>
	                        <div class="container">
	                            Das Mitglied <strong>'.USERNAME.'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
	                            <hr/>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
	                                <div class="col-md-9">Dein Account wurde aufgrund zu vieler Meldungen gesperrt.</div>
	                            </div>
	                        </div>
	                    </body>
	                </html>
	                ';

	                // Always set content-type when sending HTML email
	                $mail_headers = "MIME-Version: 1.0" . "\n";
	                $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
	                $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

	                mail($mail_to,$mail_subject,$mail_message,$mail_headers);
	            }else{
	                $db->query("UPDATE users SET reports = (reports + 1) WHERE username = '".USERNAME."'");

	                $db->query("INSERT INTO report_conversation (
                                                      			 `date`, 
                                                      			 ip, 
                                                      			 userID, 
                                                      			 username, 
                                                      			 bad_word, 
                                                      			 original_message) 
                                    VALUES ('".$date."', 
                                            '".$ip."', 
                                            '".ID."', 
                                            '".USERNAME."', 
                                            '".$word."', 
                                            '".$original_message."')
	                ");

	                $report_to = "report@chaosdidi.de";
	                $report_subject = "[Report] Es wurde eine Konversation gemeldet";
	                $report_message = '
	                <html>
	                    <head>
	                        <title>[Report] Es wurde eine Konversation gemeldet</title>
	                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	                    </head>
	                    <body>
	                        <br>
	                        <div class="container">
	                            Eine <strong>Konversation</strong> wurde heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> vom System gemeldet.
	                            <hr/>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
	                                <div class="col-md-9">'.$ip.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
	                                <div class="col-md-9">'.USERNAME.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Link zur Konversation:</strong></div>
	                                <div class="col-md-9"><a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a></div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Wort:</strong></div>
	                                <div class="col-md-9">'.$word.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Original Kommentar:</strong></div>
	                                <div class="col-md-9">'.$original_message.'</div>
	                            </div>
	                        </div>
	                    </body>
	                </html>
	                ';

	                // Always set content-type when sending HTML email
	                $report_headers = "MIME-Version: 1.0" . "\n";
	                $report_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
	                $report_headers .= 'From: ChaosDidi <report@chaosdidi.de>' . "\n";

	                mail($report_to,$report_subject,$report_message,$report_headers);
	            }
	        }
	    }

	    $users_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
	    $users_row = $users_sql->fetch_assoc();

	    if($users_row['reports'] >= 3){
	        $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

	        $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");

	        $mail_to = "ban@chaosdidi.de";
            $mail_subject = "[User Banned] ".USERNAME." wurde gesperrt";
            $mail_message = '
            <html>
                <head>
                    <title>[User Banned] '.USERNAME.' wurde gesperrt</title>
                    <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                </head>
                <body>
                    <br>
                    <div class="container">
                        Das Mitglied <strong>'.USERNAME.'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
                        <hr/>
                        <div class="row">
                            <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
                            <div class="col-md-9">Dein Account wurde aufgrund zu vieler Meldungen gesperrt.</div>
                        </div>
                    </div>
                </body>
            </html>
            ';

            // Always set content-type when sending HTML email
            $mail_headers = "MIME-Version: 1.0" . "\n";
            $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
            $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

            mail($mail_to,$mail_subject,$mail_message,$mail_headers);
	    }

	    $message = str_ireplace($bad_word, $word_replace, $message);
	    $DB_message = str_ireplace($bad_word, $word_replace, $DB_message);
	    // WORDFILTER [end]

		$db->query("INSERT INTO pn_conversation (pnID,
										   		 `date`, 
										   		 ip, 
										   		 fromID, 
										   		 toID, 
										   		 subject, 
										   		 message) 
						VALUES ('".$pnID."',
								'".$date."', 
								'".$ip."', 
								'".$fromID."', 
								'".$toID."', 
								'".$subject."', 
								'".$DB_message."')
		");

		$db->query("INSERT INTO backup_pn_conversation (pnID,
										   			    `date`, 
										   			    ip, 
										   			    fromID, 
										   			    toID, 
										   			    subject, 
										   			    message) 
						VALUES ('".$pnID."',
								'".$date."', 
								'".$ip."', 
								'".$fromID."', 
								'".$toID."', 
								'".$subject."', 
								'".$DB_message."')
		");

		if(uda::ID($toID, "noti_new_pn") == 1){
			$mail_to = uda::ID($toID, "email");
	        $mail_subject = "[ChaosDidi] Neue Private Nachricht";
	        $mail_message = '
	        <html>
	            <head>
	                <title>[ChaosDidi] Neue Private Nachricht</title>
	                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	            </head>
	            <body>
	                <br>
	                <div class="container">
	                	<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
	                	<br><br>
	                    Du hast heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> eine neue Private Nachricht erhalten.
	                    <hr/>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Von:</strong></div>
	                        <div class="col-md-9">'.uda::ID(ID, "username").'</div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Thema:</strong></div>
	                        <div class="col-md-9">'.$subject.'</div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Nachricht:</strong></div>
	                        <div class="col-md-9">'.preg_replace("/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/", "<a href='$1$2'>$1$2</a>", $message).'</div>
	                    </div>
	                    <hr/>
	                    <a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a>
	                	<br><br>
	                </div>
	            </body>
	        </html>
	        ';

	        // Always set content-type when sending HTML email
	        $mail_headers = "MIME-Version: 1.0" . "\n";
	        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
	        $mail_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

	        mail($mail_to,$mail_subject,$mail_message,$mail_headers);

	        header("Location: ".SERVER_NAME."PN/Conversation/".$pnID);
	    }

		toID_FAIL:
		echo bad("Du kannst dir <strong>selber <u>keine Privaten Nachrichten</u> schreiben</strong>!");
	}
?>

<form method="post">
	<div class="form-group">
		<label>An:</label>
		<input type="text" name="username" id="username" class="form-control" placeholder="MaxMuster" onkeyup="searchForConvUsername(this.value);" autocomplete="off" required>
		<span id="searchResultConvUsername">--</span>
	</div>
	<div class="form-group">
		<label>Thema:</label>
		<input type="text" name="subject" class="form-control" placeholder="Thema" required>
	</div>
	<div class="form-group">
		<label>Nachricht:</label>
		<textarea name="message" class="form-control" placeholder="Deine Nachricht" required></textarea>
	</div>
	<div class="form-group">
		<button type="submit" name="message_sub" class="btn btn-primary">Absenden</button>
	</div>
</form>