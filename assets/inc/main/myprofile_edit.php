<? if(ONLINE != '1'){header("Location:".SERVER_NAME);} ?>

<?
    if($_GET['success'] == '1'){
        echo "<div id='success_report' class='alert alert-success'>";
            echo "Wir haben dein <strong>Profilbild</strong> nun <strong><u>aktualisiert</u></strong> :)";
        echo "</div>";
    }

    if(isset($_POST['sub_upload_picture'])){
        $date = time();
        $userID = ID;
        $image_link = ID."_".date("dmY")."_".time();

        //CHECK POST IMAGE
        if(strlen($image_link) > 0){
            if($_FILES['image_link']['size'] < 3072000){
                $image_allowed = array('gif', 'png' ,'jpg', 'jpeg');
                $image_name = $_FILES['image_link']['name'];
                $image_ext = pathinfo($image_name, PATHINFO_EXTENSION);
                if(in_array($image_ext, $image_allowed) ) {
                    $check__image_link = true;
                    $image_link = ID."_".date("dmY")."_".time().".".$image_ext;

                    $remote_file = '/images.chaosdidi.de/avatars/'.$image_link;
                    $file = $_FILES['image_link']['tmp_name'];
                    $conn_id = ftp_connect("hostblock.de");
                    $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

                    ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
                    ftp_close($conn_id);
                }else{
                    $check__image_link = false;
                    echo bad("Es dürfen nur Bilder von <strong>.gif/.png/.jpg/.jpeg</strong> hochgeladen werden.");
                }
            }else{
                $check__image_link = false;
                echo bad("Das Profilbild ist zu groß.<br><strong>Maximal 3,0 MB!</strong>");
            }
        }else{
            $check__image_link = true;
        }


        if($check__image_link == true){
            $db->query("INSERT INTO users_images (`date`, 
                                                  userID, 
                                                  image_link) 
                             VALUES ('".$date."', 
                                     '".$userID."', 
                                     '".$image_link."')");

            $db->query("UPDATE users 
                           SET avatar = '".$image_link."' 
                         WHERE id = '".ID."'");

            $db->query("UPDATE videos_comments
                           SET user_avatar = '".$image_link."'
                         WHERE userID = '".ID."'");

            header("Location:".SERVER_NAME."MyProfile/Edit/S1");
        }
    }
?>

<div class="box">
    <!-- TEAM MEMBER - START -->   
        <div class="team-member single">
            <div class="row">
                <?
                    if(strlen(LINK_FACEBOOK) >= 7){
                        $link_facebook = LINK_FACEBOOK;
                    }
                    if(strlen(LINK_TWITTER) >= 7){
                        $link_twitter = LINK_TWITTER;
                    }
                    if(strlen(LINK_YOUTUBE) >= 7){
                        $link_youtube = LINK_YOUTUBE;
                    }
                    if(strlen(LINK_GPLUS) >= 7){
                        $link_gplus = LINK_GPLUS;
                    }
                    if(strlen(LINK_STEAM) >= 7){
                        $link_steam = LINK_STEAM;
                    }

                    echo "<div class='col-sm-4 col-md-3'>";
                        $AgetHeaders = @get_headers("https://images.chaosdidi.de/avatars/".AVATAR);
                        if (preg_match("|200|", $AgetHeaders[0])) {
                            echo "<img src='//images.chaosdidi.de/avatars/".AVATAR."' class='img-responsive user-profile-borderradius center-block' alt='".USERNAME."'>";
                        } else {
                            echo "<img src='//images.chaosdidi.de/avatars/default.png' class='img-responsive user-profile-borderradius center-block' alt='".USERNAME."'>";
                        }

                    echo "</div>";
                    echo "<div class='col-sm-8 col-md-9'>";
                        echo "<h2>Benutzername<small>".USERNAME."</small></h2>";
                        echo "<div class='row'>";
                            echo "<div class='col-sm-4 col-md-4 text-right'><strong>Mitglied seit:</strong></div>";
                            echo "<div class='col-sm-8 col-md-8'>".MEMBER_SINCE."</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-sm-4 col-md-4 text-right'><strong>Letzte Aktivität am:</strong></div>";
                            echo "<div class='col-sm-8 col-md-8'>".LAST_ACTIVITY."</div>";
                        echo "</div>";

                        echo "<div class='row'>";
                            echo "<div class='col-xs-5 col-md-4 text-right'><strong>Rang:</strong></div>";
                            echo "<div class='col-xs-7 col-md-8'>".points_to_level(CREDITS_TOTAL, "level")."</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-xs-5 col-md-4 text-right'><strong>Punkte:</strong></div>";
                            echo "<div class='col-xs-7 col-md-8'>".CREDITS_TOTAL."</div>";
                        echo "</div>";

                        echo "<div class='row'>";
                            echo "<div class='col-sm-4 col-md-4 text-right'><strong>Über mich:</strong></div>";
                            echo "<div class='col-sm-8 col-md-8'>".ABOUT_ME."</div>";
                        echo "</div>";
                    echo "</div>";
                ?>
            </div>
            <hr/>
            <h2><i class="fa fa-image fa-fw"></i> Profilbild hochladen</h2>
            <form method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <input type="file" name="image_link">
                    </div>
                    <div class="col-md-6">
                        <button type="submit" name="sub_upload_picture" class="btn btn-primary btn-block">Hochladen</button>
                    </div>
                </div>
            </form>

        </div>
    <!-- TEAM MEMBER - END --> 
</div>


<div class="box">
    <?
        if($_GET['success'] == '2'){
            echo "<div id='success_report' class='alert alert-success'>";
                echo "Wir haben deine <strong>eMail Adresse</strong> für dich <strong><u>aktualisiert</u></strong> :)";
            echo "</div>";
        }

        if(isset($_POST['sub_email'])){
            $password = $db->real_escape_string(crypt($_POST['password'], CRYPT_PW));
            $email1 = $db->real_escape_string($_POST['email1']);
            $email2 = $db->real_escape_string($_POST['email2']);

            $email_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
            $email_row = $email_sql->fetch_assoc();
            $email_sql2 = $db->query("SELECT * FROM users WHERE email = '".$email2."'");
            $email_row2 = $email_sql2->fetch_assoc();

            //CHECK PASSWORD
            if($password != $email_row['password']){
                echo bad("Dein <strong>Passwort</strong> ist <strong><u>falsch</u></strong>.");
            }else{
                $check__password = true;
            }

            if(strlen($_POST['password']) < 8){
                echo bad("Dein <strong>Passwort</strong> ist zu <strong><u>kurz</u></strong>. Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password2 = true;
            }


            //CHECK EMAIL ADDRESS
            if($email1 != $email2){
                echo bad("Beide <strong>eMail Adressen stimmen nicht überein</strong>.");
            }else{
                $check__email = true;
            }

            if($email1 == $email_row2['email']){
                echo bad("Diese <strong>eMail Adresse</strong> ist bereits <strong><u>vergeben</u></strong>.");
            }else{
                $check__email2 = true;
            }

            if(strlen($email1) < 8){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>zu kurz</u></strong>.");
            }else{
                $check__email3 = true;
            }

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $email1)){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.");
            }else{
                $check__email4 = true;
            }

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $email2)){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.");
            }else{
                $check__email5 = true;
            }


            if($check__password == true && $check__password2 == true
                && $check__email == true && $check__email2 == true && $check__email3 == true && $check__email4 == true && $check__email5 == true){
                $db->query("UPDATE users SET email = '".$email2."' WHERE id = '".ID."'");
                header("Location:".SERVER_NAME."MyProfile/Edit/S2");
            }
        }


        if($_GET['success'] == '3'){
            echo "<div id='success_report' class='alert alert-success'>";
                echo "Wir haben dein <strong>Passwort</strong> für dich <strong><u>aktualisiert</u></strong> :)";
            echo "</div>";
        }

        if(isset($_POST['sub_password'])){
            $pass1 = $db->real_escape_string(crypt($_POST['pass1'], CRYPT_PW));
            $pass2 = $db->real_escape_string(crypt($_POST['pass2'], CRYPT_PW));
            $pass3 = $db->real_escape_string(crypt($_POST['pass3'], CRYPT_PW));

            $password_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
            $password_row = $password_sql->fetch_assoc();

            //CHECK PASSWORDS
            if(strlen($pass1) < 8){
                echo bad("Dein <strong>aktuelles Passwort</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password = true;
            }

            if(strlen($pass2) < 8){
                echo bad("Dein <strong>neues Passwort</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password2 = true;
            }

            if(strlen($pass3) < 8){
                echo bad("Das <strong>wiederholte Passwort</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password3 = true;
            }

            if($pass1 != $password_row['password']){
                echo bad("Dein <strong>aktuelles Passwort</strong> ist <strong><u>falsch</u></strong>.");
            }else{
                $check__password4 = true;
            }

            if($pass2 != $pass3){
                echo bad("Das <strong>neue Passwort, stimmt nicht mit dem wiederholten Passwort überein</strong>.");
            }else{
                $check__password5 = true;
            }


            if($check__password == true && $check__password2 == true && $check__password3 == true && $check__password4 == true && $check__password5 == true){
                $db->query("UPDATE users SET password = '".$pass3."' WHERE id = '".ID."'");
                header("Location:".SERVER_NAME."MyProfile/Edit/S3");
            }
        }



        if($_GET['success'] == '4'){
            echo "<div id='success_report' class='alert alert-success'>";
                echo "Wir haben deine <strong>Kontaktmöglichkeiten</strong> für dich <strong><u>aktualisiert</u></strong> :)";
            echo "</div>";
        }

        if(isset($_POST['sub_contact'])){
            $lnk_facebook = $db->real_escape_string($_POST['lnk_facebook']);
            $lnk_twitter = $db->real_escape_string($_POST['lnk_twitter']);
            $lnk_youtube = $db->real_escape_string($_POST['lnk_youtube']);
            $lnk_gplus = $db->real_escape_string($_POST['lnk_gplus']);
            $lnk_steam = $db->real_escape_string($_POST['lnk_steam']);

            //CHECK FACEBOOK LNK
            if(strlen($lnk_facebook) > 0 && strlen($lnk_facebook) < 12){
                echo bad("Dein <strong>Facebook Link</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 12 Zeichen</u></strong> verwenden.");
            }else{
                $check__lnk_facebook = true;
            }


            //CHECK TWITTER LNK
            if(strlen($lnk_twitter) > 0 && strlen($lnk_twitter) < 12){
                echo bad("Dein <strong>Twitter Link</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 12 Zeichen</u></strong> verwenden.");
            }else{
                $check__lnk_twitter = true;
            }


            //CHECK YOUTUBE LNK
            if(strlen($lnk_youtube) > 0 && strlen($lnk_youtube) < 12){
                echo bad("Dein <strong>YouTube Link</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 12 Zeichen</u></strong> verwenden.");
            }else{
                $check__lnk_youtube = true;
            }


            //CHECK GOOGLE PLUS LNK
            if(strlen($lnk_gplus) > 0 && strlen($lnk_gplus) < 12){
                echo bad("Dein <strong>Google Plus Link</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 12 Zeichen</u></strong> verwenden.");
            }else{
                $check__lnk_gplus = true;
            }


            //CHECK STEAM LNK
            if(strlen($lnk_steam) > 0 && strlen($lnk_steam) < 12){
                echo bad("Dein <strong>Steam Link</strong> ist <strong><u>zu kurz</u></strong>. Bitte <strong><u>mindestens 12 Zeichen</u></strong> verwenden.");
            }else{
                $check__lnk_steam = true;
            }


            if($check__lnk_facebook == true
                && $check__lnk_twitter == true
                && $check__lnk_youtube == true
                && $check__lnk_gplus == true
                && $check__lnk_steam == true){
                $db->query($db, "UPDATE users 
                                  SET link_facebook = '".$lnk_facebook."', 
                                      link_twitter = '".$lnk_twitter."', 
                                      link_youtube = '".$lnk_youtube."', 
                                      link_gplus = '".$lnk_gplus."',
                                      link_steam = '".$lnk_steam."' 
                                WHERE id = '".ID."'");
                header("Location:".SERVER_NAME."MyProfile/Edit/S4");
            }
        }
    ?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#notifications" aria-controls="notifications" role="tab" data-toggle="tab"><i class="fa fa-bullhorn fa-fw"></i> Benachrichtigungen</a></li>
            <li role="presentation"><a href="#email_address" aria-controls="email_address" role="tab" data-toggle="tab"><i class="fa fa-envelope fa-fw"></i> eMail Adresse</a></li>
            <li role="presentation"><a href="#password" aria-controls="password" role="tab" data-toggle="tab"><i class="fa fa-lock fa-fw"></i> Passwort</a></li>
            <li role="presentation"><a href="#contact_possibilities" aria-controls="contact_possibilities" role="tab" data-toggle="tab"><i class="fa fa-tty fa-fw"></i> Kontaktmöglichkeiten</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="notifications">
                <br>
                <form method="post">
                    <h2><i class="fa fa-bullhorn fa-fw"></i> Benachrichtigungen</h2>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-9 text-right">Bei <u>neuen Videos</u> eine eMail erhalten</div>
                            <div class="col-md-3">
                                <? if(NOTI_EMAIL_NEWVIDEO == 1){$checked1 = "checked";} ?>
                                <input type="checkbox" name="noti_email_newvideo" id="switch-state" data-size="mini" data-on-text="Ja" data-on-color="success" data-off-text="Nein" data-off-color="danger" <? echo $checked1 ?>>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-9 text-right"><u>Bilder im eigenen Profil</u> Geheim halten</div>
                            <div class="col-md-3">
                                <? if(SECRET_PROFILE_IMGS == 1){$checked2 = "checked";} ?>
                                <input type="checkbox" name="secret_profile_imgs" id="switch-state" data-size="mini" data-on-text="Ja" data-on-color="success" data-off-text="Nein" data-off-color="danger" <? echo $checked2 ?>>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-9 text-right">Bei <u>neuen Benachrichtigungen</u> eine eMail erhalten</div>
                            <div class="col-md-3">
                                <? if(NOTI_NEW_NOTI == 1){$checked3 = "checked";} ?>
                                <input type="checkbox" name="noti_new_noti" id="switch-state" data-size="mini" data-on-text="Ja" data-on-color="success" data-off-text="Nein" data-off-color="danger" <? echo $checked3 ?>>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-9 text-right">Bei <u>neuen Privaten Nachrichten</u> eine eMail erhalten</div>
                            <div class="col-md-3">
                                <? if(NOTI_NEW_PN == 1){$checked4 = "checked";} ?>
                                <input type="checkbox" name="noti_new_pn" id="switch-state" data-size="mini" data-on-text="Ja" data-on-color="success" data-off-text="Nein" data-off-color="danger" <? echo $checked4 ?>>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="email_address">
                <br>
                <form method="post">
                    <h2><i class="fa fa-envelope fa-fw"></i> eMail Adresse ändern</h2>
                    <div class="form-group">
                        <label>Aktuelles Passwort eingeben</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                            <input type="password" name="password" class="form-control" placeholder="Aktuelles Passwort" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Neue eMail Adresse</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></div>
                            <input type="email" name="email1" class="form-control" placeholder="Neue eMail Adresse" <? echo "value='".$email1."'"; ?> required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Neue eMail Adresse wiederholen</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></div>
                            <input type="email" name="email2" class="form-control" placeholder="Neue eMail Adresse wiederholen" <? echo "value='".$email2."'"; ?> required>
                        </div>
                    </div>
                    <button type="submit" name="sub_email" class="btn btn-primary">Speichern</button>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="password">
                <br>
                <form method="post">
                    <h2><i class="fa fa-lock fa-fw"></i> Passwort ändern</h2>
                    <div class="form-group">
                        <label>Aktuelles Passwort eingeben</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                            <input type="password" name="pass1" class="form-control" placeholder="Aktuelles Passwort" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Neues Passwort</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                            <input type="password" name="pass2" class="form-control" placeholder="Neues Passwort" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Neues Passwort wiederholen</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                            <input type="password" name="pass3" class="form-control" placeholder="Neues Passwort wiederholen" required>
                        </div>
                    </div>
                    <button type="submit" name="sub_password" class="btn btn-primary">Speichern</button>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="contact_possibilities">
                <br>
                <form method="post">
                    <h2><i class="fa fa-tty fa-fw"></i> Kontaktmöglichkeiten</h2>
                    <div class="form-group">
                        <label>Facebook</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-facebook fa-fw"></i></div>
                            <? echo "<input type='text' name='lnk_facebook' class='form-control' placeholder='https://facebook.com/MaxMuster' value='".$link_facebook."'>"; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-twitter fa-fw"></i></div>
                            <? echo "<input type='text' name='lnk_twitter' class='form-control' placeholder='https://twitter.com/MaxMuster' value='".$link_twitter."'>"; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>YouTube</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-youtube fa-fw"></i></div>
                            <? echo "<input type='text' name='lnk_youtube' class='form-control' placeholder='https://youtube.com/user/MaxMuster' value='".$link_youtube."'>"; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Google Plus</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-google-plus fa-fw"></i></div>
                            <? echo "<input type='text' name='lnk_gplus' class='form-control' placeholder='https://plus.google.com/+MaxMuster' value='".$link_gplus."'>"; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Steam</label>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-steam fa-fw"></i></div>
                            <? echo "<input type='text' name='lnk_steam' class='form-control' placeholder='http://steamcommunity.com/id/MaxMuster/' value='".$link_steam."'>"; ?>
                        </div>
                    </div>
                    <button type="submit" name="sub_contact" class="btn btn-primary">Speichern</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="box profile-gallery">
    <h2>Profil Gallerie</h2>
    <div class="gallery-page">
        <div class="row masonry">
            <?
                $profile_gallery_sql = $db->query("SELECT * FROM users_images WHERE userID = '".ID."' ORDER BY id DESC");
                while($row = $profile_gallery_sql->fetch_assoc()){
                    if($row['image_link'] == AVATAR){
                        $make_to_image = "";
                        $delete_image = "";
                    }else{
                        $make_to_image = "<button type='button' id='btnMakeNEW_profile_image_".$row['id']."' class='btn btn-no-bbr btn-success btn-block btn-xs' style='margin-bottom:-5px;' onclick='make_to_profile_image(".$row['id'].");'><i class='fa fa-bookmark fa-fw'></i>Als Profilbild verwenden</button>";
                        $delete_image = "<button type='button' id='btnDelNEW_profile_image_".$row['id']."' class='btn btn-no-btr btn-danger btn-block btn-xs' onclick='del_profile_image(".$row['id'].");'><i class='fa fa-trash fa-fw'></i>Foto Löschen</button>";
                    }
                    echo "<div id='profile_image_".$row['id']."' class='masonry-item col-md-4 col-xs-6 img-thumbnail'>";
                        echo "<a href='//images.chaosdidi.de/avatars/".$row['image_link']."'><img src='//images.chaosdidi.de/avatars/".$row['image_link']."' class='img-responsive' alt='".$row['image_link']."'></a>";
                        echo $make_to_image;
                        echo $delete_image;
                        echo "<center><strong>Hochgeladen am:</strong> ".date("d.m.Y H:i", $row['date'])."</center>";
                    echo "</div>";
                }
            ?>  
        </div>
    </div>
</div>