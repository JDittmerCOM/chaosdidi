<div class="box">
                        
    <!-- POST - START -->
    <?
        $postID = $db->real_escape_string($_GET['id']);
        $post_sql = $db->query("SELECT * FROM videos WHERE id = '".$postID."'");
        $post_row = $post_sql->fetch_assoc();
        echo head_title($post_row['title']);

        $description = str_replace("\n", "<br>", $post_row['description']);

        echo "<article class='post post-single'>";
            echo "<div class='post-date-wrapper'>";
                echo "<div class='post-date'>";
                    echo "<div class='day'>".date("d", $post_row['published'])."</div>";
                    echo "<div class='month'>".date("M Y", $post_row['published'])."</div>";
                echo "</div>";
                echo "<div class='post-type'>";
                    echo "<i class='fa fa-video-camera'></i>";
                echo "</div>";
            echo "</div>";
            echo "<div class='post-body'>";
                echo "<h2>".$post_row['title']."</h2>";
                echo "<div class='flex-video widescreen'><iframe width='1280' height='720' src='https://www.youtube.com/embed/".$post_row['ytID']."?rel=0' allowfullscreen></iframe></div>";
                echo "<p>".preg_replace("/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/", "<a href='$1$2' target='_blank'>$1$2</a>", $description)."</p>";
                echo "<hr/>";
                $tag = explode(",", $post_row['tags']);
                foreach($tag as $tag) {
                    $tag = trim($tag);
                    $tag = htmlspecialchars($tag, ENT_QUOTES);
                    $tags .= "<a href='Tag/".$tag."' target='_blank'><span class='label label-default post-label'><i class='fa fa-tag'></i> ".$tag."</span></a>&ensp;";
                }
                echo $tags;
                echo "<div class='post-info'>";
                    echo "<span>Gepostet von: ".$post_row['author']."</span>";
                echo "</div>";
            echo "</div>";
        echo "</article>";
    ?>
    <!-- POST - END -->

    <!-- COMMENT WRAPPER - START -->
    <div class="comments"></div>

    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'chaosdidi';
        
        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the comments.</noscript>
    <!-- COMMENT WRAPPER - END -->
</div>