<link href="assets/js/pagination/pagination.css" rel="stylesheet" type="text/css"></link>
<div class="box">
	<? 
		$playlistID = $_GET['id']; 
		$playlist_sql = $db->query("SELECT * FROM playlists WHERE id = '".$playlistID."'");
		$playlist_row = $playlist_sql->fetch_assoc();
	?>
	<h2><a href="Playlist">Playlist Übersicht</a> <i class="fa fa-arrow-right fa-fw"></i> <? echo $playlist_row['name'] ?></h2>

	<!-- POST - START -->
    <div id="hiddenresult" style="display:none;">
        <?
            $playlist_post_sql = $db->query("SELECT * FROM videos WHERE playlists = '".addslashes($playlist_row['name'])."' AND planned_status = '0' ORDER BY id ASC LIMIT 250");
            while($row = $playlist_post_sql->fetch_assoc()){
                if(strlen($row['description']) >= 300){
                    $description = htmlentities(str_replace("<br>", "\n", substr($row['description'], 0, 300)." ..."));
                }else{
                    $description = htmlentities(str_replace("<br>", "\n", $row['description']));
                }

                if(!file("http://images.chaosdidi.de/uploads/".$row['image_link'])){
                    $image_link = "no-cover.jpg";
                }else{
                    if(isMobile()){
                        $resize_mobi = explode(".", $row['image_link']);
                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
                        $image_link = $resize_mobi;
                    }else{
                        $resize_mobi = explode(".", $row['image_link']);
                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
                        $image_link = $resize_mobi;
                        //$image_link = $row['image_link'];
                    }
                }

                echo "<article class='post result'>";
                    echo "<div class='post-date-wrapper'>";
                        echo "<div class='post-date'>";
                            echo "<div class='day'>".date("d", $row['published'])."</div>";
                            echo "<div class='month'>".date("M Y", $row['published'])."</div>";
                        echo "</div>";
                        echo "<div class='post-type'>";
                            echo "<i class='fa fa-video-camera'></i>";
                        echo "</div>";
                    echo "</div>";
                    echo "<div class='post-body'>";
                        echo "<h2>".htmlspecialchars($row['title'], ENT_QUOTES)."</h2>";
                        echo "<p><img src='//images.chaosdidi.de/uploads/".$image_link."' class='img-responsive' alt='".$row['title']."'>".$description."</p>";
                        echo "<div class='post-info'>";
                            echo "<span>Gepostet von: ".$row['author']."</span>";
                            echo "<a href='Post/".$row['id']."' class='btn btn-inverse'>Zeige mehr</a>";
                        echo "</div>";
                    echo "</div>";
                echo "</article>";
            }
        ?>
    </div>
    <!-- POST - END -->

    <div id="Pagination"></div>
    <br style="clear:both;" />
    <div id="Searchresult">
        <div class="alert alert-warning">
            <i class="fa fa-spinner fa-pulse fa-fw"></i>
            Einen Augenblick bitte, wir laden gerade alle Folgen für dich :)
        </div>
    </div>
</div>