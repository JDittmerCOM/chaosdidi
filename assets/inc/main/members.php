<div class="box">
	<h2>Mitglieder</h2>

	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#active" aria-controls="active" role="tab" data-toggle="tab">Aktiv. Mitglieder</a></li>
		<li role="presentation"><a href="#not_unlocked" aria-controls="not_unlocked" role="tab" data-toggle="tab">N. n. Freig. Mitglieder</a></li>
		<li role="presentation"><a href="#ban" aria-controls="ban" role="tab" data-toggle="tab">Gesp. Mitglieder</a></li>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in active" id="active">
			<table class="table">
				<thead>
					<tr>
						<th>Avatar</th>
						<th>Benutzername</th>
						<th class="text-center">Mitglied seit</th>
						<th class="text-center">Letzter Besuch</th>
					</tr>
				</thead>
				<tbody>
				<?

					$sql = $db->query("SELECT * FROM users WHERE status = 2 ORDER BY id ASC");
					while($row = $sql->fetch_assoc()){
						if($row['mod'] == 0 && $row['adm'] == 0){
							if($row['status'] == 2){
								$username = $row['username'];
								$group = "Mitglied";
							}elseif($row['status'] == 1){
								$username = $row['username'];
								$group = "Nicht Freigeschaltet";
							}elseif($row['status'] == 0){
								$username = "<s>".$row['username']."</s>";
								$group = "<p class='text-muted'>Gesperrt</p>";
							}
						}else{
							if($row['adm'] == 1){
								$username = $row['username'];
								$group = "<p class='text-danger'><strong>Administrator</strong></p>";
							}else{
								$username = $row['username'];
								$group = "<p class='text-success'><strong>Moderator</strong></p>";
							}
						}

						echo "<tr>";
							echo "<td><img src='//images.chaosdidi.de/avatars/".$row['avatar']."' class='members_avatar' alt='".$row['avatar']."'></td>";
							echo "<td><a href='MyProfile/".$row['id']."'>".$username."</a><br>".$group."</td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['date'])."</td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['last_activity'])."</td>";
						echo "</tr>";
					}
				?>
				</tbody>
			</table>
		</div>
		<div role="tabpanel" class="tab-pane fade" id="not_unlocked">
			<table class="table">
				<thead>
					<tr>
						<th>Avatar</th>
						<th>Benutzername</th>
						<th class="text-center">Mitglied seit</th>
						<th class="text-center">Letzter Besuch</th>
					</tr>
				</thead>
				<tbody>
				<?

					$sql = $db->query("SELECT * FROM users WHERE status = 1 ORDER BY id ASC");
					while($row = $sql->fetch_assoc()){
						echo "<tr>";
							echo "<td><img src='//images.chaosdidi.de/avatars/".$row['avatar']."' class='members_avatar' alt='".$row['avatar']."'></td>";
							echo "<td><a href='MyProfile/".$row['id']."'>".$row['username']."</a><br><p class='text-muted'>Nicht Freigeschaltet</p></td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['date'])."</td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['last_activity'])."</td>";
						echo "</tr>";
					}
				?>
				</tbody>
			</table>
		</div>
		<div role="tabpanel" class="tab-pane fade" id="ban">
			<table class="table">
				<thead>
					<tr>
						<th>Avatar</th>
						<th>Benutzername</th>
						<th class="text-center">Mitglied seit</th>
						<th class="text-center">Letzter Besuch</th>
					</tr>
				</thead>
				<tbody>
				<?

					$sql = $db->query("SELECT * FROM users WHERE status = 0 ORDER BY id ASC");
					while($row = $sql->fetch_assoc()){
						echo "<tr>";
							echo "<td><img src='//images.chaosdidi.de/avatars/".$row['avatar']."' class='members_avatar' alt='".$row['avatar']."'></td>";
							echo "<td><a href='MyProfile/".$row['id']."'><s>".$row['username']."</s></a><br><p class='text-muted'>Gesperrt</p></td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['date'])."</td>";
							echo "<td class='text-center'>".date("d.m.Y, H:i", $row['last_activity'])."</td>";
						echo "</tr>";
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
	
</div>