<div class="box">
	<?
		$postID = $_GET['id'];
		$post_sql = $db->query("SELECT * FROM videos WHERE id = '".$postID."'");
		$post_row = $post_sql->fetch_assoc();
	?>
	<h2>Beitrag bearbeiten<br><? echo $post_row['title'] ?></h2>

	<?
		if($_GET['success'] == '1'){
			echo "<div class='alert alert-success' id='success_report'>";
				echo "Dein Beitrag wurde gespeichert.";
			echo "</div>";
		}

		//BEITRAG EDITIEREN [start]
		if(isset($_POST['sub_edit'])){
			$title = $db->real_escape_string($_POST['title']);
			$description = $db->real_escape_string(str_replace(array("\r\n"), "<br>", $_POST['description']));
			$playlists = $db->real_escape_string($_POST['playlists']);
			if(!preg_match("/([\'])/", $playlists)){
				$playlists = str_replace("\\", "", $playlists);
				$playlists_old = $post_row['playlists'];
			}else{
				$playlists_old = str_replace("\\", "\'", $post_row['playlists']);
			}

			$tags = $db->real_escape_string($_POST['tags']);
			$image_link = $_FILES['image_link']['name'];
			$planned_time = $_POST['planned_time'];

			//PLAYLISTS
			if(str_replace("\\", "\'", $post_row['playlists']) == $playlists){
				$playlists = str_replace("\\", "\'", $post_row['playlists']);
			}else{
				$db->query("UPDATE playlists SET count = (count - 1) WHERE name = '".addslashes($playlists_old)."'");

				$db->query("UPDATE playlists SET count = (count + 1) WHERE name = '".$playlists."'");
			}

			//CHECK POST IMAGE
			if(strlen($image_link) > 0){
				if($_FILES['image_link']['size'] < 3072000){
					$image_allowed = array('gif', 'png' ,'jpg', 'jpeg');
	                $image_name = $_FILES['image_link']['name'];
	                $image_ext = pathinfo($image_name, PATHINFO_EXTENSION);
	                if(in_array($image_ext, $image_allowed) ) {
		                $__check_image_link = true;
		                
		                $remote_file = '/images.chaosdidi.de/uploads/'.$post_row['image_link'];
		                $file = $_FILES['image_link']['tmp_name'];
		                $conn_id = ftp_connect("hostblock.de");
		                $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

		                ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
		                ftp_close($conn_id);
		            }else{
		            	$__check_image_link = false;
                    	echo bad("Es dürfen nur Bilder von <strong>.gif/.png/.jpg/.jpeg</strong> hochgeladen werden.");
		            }
	            }else{
					$__check_image_link = false;
					echo bad("Das Beitragsbild ist zu groß.<br><strong>Maximal 3,0 MB!</strong>");
				}
			}else{
				$__check_image_link = true;
			}

			//PLANNED POST
			if(strlen($planned_time) >= 1){
				$planned_status = "1";
				$planned_time = strtotime($planned_time);
				$__check_planned_post = true;
			}else{
				$planned_status = "0";
				$planned_time = "";
				$__check_planned_post = true;
			}


			if($__check_image_link == true && $__check_planned_post == true){
				$db->query("UPDATE videos 
							  SET title = '".$title."', 
								  description = '".$description."',
								  playlists = '".$playlists."',
								  tags = '".$tags."',
								  planned_status = '".$planned_status."',
								  planned_time = '".$planned_time."'
							WHERE id = '".$postID."'");

				if(strlen($tags) > 0){
					$tag_size = array("12", "14", "16", "18", "20");

					$tags_count = explode(",", $tags);
					$tags_count_null = -1;
					while ($tags_count_null <= count($tags_count)-2) {
						$tags_count_null++;
						$db->query("INSERT INTO tags (tag, size) VALUES ('".$tags_count[$tags_count_null]."', '".$tag_size[array_rand($tag_size)]."')");
					}
				}

				// POST ON FACEBOOK [start]
			    define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/../../../facebook/');
			    require_once __DIR__ . '/../../../facebook/autoload.php';

			    $fb = new Facebook\Facebook([
			        'app_id' => FB_APP_ID,
			        'app_secret' => FB_APP_SECRET,
			        'default_graph_version' => FB_APP_VERSION,
			    ]);

			    $linkData = [
			        'picture' => "http://images.chaosdidi.de/uploads/".$post_row['image_link'],
			        'message' => str_replace("<br>", "\n", $description),
			    ];

			    try {
			        // Returns a `Facebook\FacebookResponse` object
			        $response = $fb->post('/'.$post_row['fb_id'], $linkData, FB_APP_ACCESS_TOKEN);
			    } catch(Facebook\Exceptions\FacebookResponseException $e) {
			        echo 'Graph returned an error: ' . $e->getMessage();
			        exit;
			    } catch(Facebook\Exceptions\FacebookSDKException $e) {
			        echo 'Facebook SDK returned an error: ' . $e->getMessage();
			        exit;
			    }
				// POST ON FACEBOOK [end]

				header("Location:".SERVER_NAME."Admin/Posts/Edit/".$postID."/S1");
			}
		}
		//BEITRAG EDITIEREN [end]


		//BEITRAG UPDATEN ÜBER YOUTUBE [start]
		if(isset($_POST['sub_update'])){
			$yt_link = $post_row['ytID'];

			$json = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$yt_link.'&key=AIzaSyAXeU4P8HJRQKTTaErpt81Vyw6cBlLVyEo&part=snippet,contentDetails,statistics,status');
			$yt_obj = json_decode($json, true);
			
			$title = $db->real_escape_string($yt_obj['items'][0]['snippet']['title']);
			$description = $db->real_escape_string(str_replace("\n", "<br>", $yt_obj['items'][0]['snippet']['description']));
			$image_link = $yt_obj['items'][0]['snippet']['thumbnails']['maxres']['url'];
			$image_link_mobi = $yt_obj['items'][0]['snippet']['thumbnails']['medium']['url'];

			// ---- THUMBNAIL FOR NON-MOBILES [start]
			$imageType = explode("maxresdefault.", $image_link);
            $image_link_short = $yt_link.".".$imageType[1];

			$remote_file = '/images.chaosdidi.de/uploads/'.$image_link_short;
            $file = $image_link;
            $conn_id = ftp_connect("hostblock.de");
            $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

            ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
            ftp_close($conn_id);
            // ---- THUMBNAIL FOR NON-MOBILES [end]

            // ---- THUMBNAIL FOR MOBILES [start]
			$imageType = explode("mqdefault.", $image_link_mobi);
            $image_link_short_mobi = $yt_link."-MOBI.".$imageType[1];

			$remote_file = '/images.chaosdidi.de/uploads/'.$image_link_short_mobi;
            $file = $image_link_mobi;
            $conn_id = ftp_connect("hostblock.de");
            $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

            ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
            ftp_close($conn_id);
            // ---- THUMBNAIL FOR MOBILES [end]

			$ytID = $yt_obj['items'][0]['id'];
			$planned_time = $_POST['planned_time'];

			//CHECK POST IMAGE
			if($_FILES['image_link']['size'] < 3072000){
				$__check_image_link = true;
			}else{
				$__check_image_link = false;
				echo bad("Das Beitragsbild ist zu groß.<br><strong>Maximal 3,0 MB!</strong>");
			}

			//CHECK TAGS
			$db->query("UPDATE videos SET tags = '' WHERE id = '".$postID."'");


			$tags = "";
			if(strlen($tags) == 0){
				$count_null = 0;
				$count = count($yt_obj['items'][0]['snippet']['tags']);

				$auto_tags = "";
				while ($count_null < $count) {
					$auto_tags .= str_replace(array(" ", "(", ")", "[", "]", ":"), array(",", "", "", "", "", ""), $yt_obj['items'][0]['snippet']['tags'][$count_null]).",";
					$count_null++;
				}
				$auto_tags = implode(',', array_unique(explode(',', $auto_tags)));
				$tags = substr_replace($auto_tags, '', -1, 1);
				$tags = $db->real_escape_string($tags);
			}


			if($__check_image_link == true){
				$db->query("UPDATE videos 
							  SET title = '".$title."',
							  	  description = '".$description."',
							  	  tags = '".$tags."',
							  	  image_link = '".$image_link_short."'
							WHERE id = '".$postID."'
				");

				if(strlen($tags) > 0){
					$tag_size = array("12", "14", "16", "18", "20");

					$tags_count = explode(",", $tags);
					$tags_count_null = -1;
					while ($tags_count_null <= count($tags_count)-2) {
						$tags_count_null++;
						$db->query("INSERT INTO tags (tag, size) VALUES ('".$tags_count[$tags_count_null]."', '".$tag_size[array_rand($tag_size)]."')");
					}
				}

				// POST ON FACEBOOK [start]
			    define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/../../../facebook/');
			    require_once __DIR__ . '/../../../facebook/autoload.php';

			    $fb = new Facebook\Facebook([
			        'app_id' => FB_APP_ID,
			        'app_secret' => FB_APP_SECRET,
			        'default_graph_version' => FB_APP_VERSION,
			    ]);

			    $linkData = [
			        'picture' => "http://images.chaosdidi.de/uploads/".$image_link_short,
			        'message' => str_replace("<br>", "\n", $description),
			    ];

			    try {
			        // Returns a `Facebook\FacebookResponse` object
			        $response = $fb->post('/'.$post_row['fb_id'], $linkData, FB_APP_ACCESS_TOKEN);
			    } catch(Facebook\Exceptions\FacebookResponseException $e) {
			        echo 'Graph returned an error: ' . $e->getMessage();
			        exit;
			    } catch(Facebook\Exceptions\FacebookSDKException $e) {
			        echo 'Facebook SDK returned an error: ' . $e->getMessage();
			        exit;
			    }
				// POST ON FACEBOOK [end]

				header("Location:http:".SERVER_NAME."Admin/Posts/Edit/".$postID);
			}
		}
		//BEITRAG UPDATEN ÜBER YOUTUBE [end]
		

		//BEITRAG LÖSCHEN [start]
		if(isset($_POST['sub_del'])){
			//FTP DELETE THUMBNAIL [start]
			$web = 'hostblock.de';
			$user = FTP_USERNAME;
			$pass = FTP_PASSWORD;
			$server_file = '/images.chaosdidi.de/uploads/'.$post_row['image_link'];
			$conn_id = ftp_connect($web);
			$login_result = ftp_login($conn_id,$user,$pass);
			ftp_delete($conn_id, $server_file);
			ftp_close($conn_id);

			$web = 'hostblock.de';
			$user = FTP_USERNAME;
			$pass = FTP_PASSWORD;
			$ftp_mobi = explode(".", $post_row['image_link']);
            $ftp_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
			$server_file = '/images.chaosdidi.de/uploads/'.$ftp_mobi;
			$conn_id = ftp_connect($web);
			$login_result = ftp_login($conn_id,$user,$pass);
			ftp_delete($conn_id, $server_file);
			ftp_close($conn_id);
			//FTP DELETE THUMBNAIL [end]

			$playlists = str_replace("'", "\'", $post_row['playlists']);

			$db->query("UPDATE playlists SET count = (count - 1) WHERE name = '".$playlists."'");

			$db->query("DELETE FROM videos_comments WHERE postID = '".$postID."'");
			
			$db->query("DELETE FROM videos WHERE id = '".$postID."'");

			header("Location:".SERVER_NAME."Admin/Posts/All");
		}
		//BEITRAG LÖSCHEN [end]
	?>

	<form method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<input type="text" name="title" class="form-control" placeholder="Gib hier den Titel an" <? echo "value='".htmlspecialchars($post_row['title'], ENT_QUOTES)."'"; ?> required>
				</div>
				<hr/>
				<div class="form-group">
					<div class="row">
						<div class="col-md-5 text-right"><h2>Beitrag Planen</h2></div>
						<div class="col-md-7">
							<div class="input-group date" id="posts_createapi_datetimepicker">
								<input type='text' name="planned_time" class="form-control" <? echo "value='".date("d.m.Y H:i", $post_row['planned_time'])."'"; ?> placeholder="Für Planen, auf Icon rechts Klicken">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
							</div><br>
							<button type="button" id="del_planned_time" class="btn btn-inverse pull-right">Datum löschen</button>
							<button type="button" id="save_planned_time" class="btn btn-primary pull-right">Datum speichern</button>
						</div>
					</div>
				</div>
				<hr/>
				<div class="form-group">
					<textarea name="description" class="form-control"><? echo str_replace("<br>", "\n", $post_row['description']) ?></textarea>
				</div>
			</div>
			<div class="col-md-4">
				<button type="submit" name="sub_edit" class="btn btn-primary btn-block">Speichern</button>
				<button type="submit" name="sub_update" class="btn btn-inverse btn-block" onclick="return confirm('Möchtest du wirklich alle Daten, von YouTube noch einmal erneut übernehmen?');">Post Update (YouTube)</button>
				<button type="submit" name="sub_del" class="btn btn-danger btn-block" onclick="return confirm('Möchtest du wirklich diesen Beitrag entfernen?');">Löschen</button>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-6">
				<div id="tournament-groups">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Playlist</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="radio">
										<span id="addPlaylist"></span>
										<?
											$sql = $db->query("SELECT * FROM playlists ORDER BY name ASC");
											while($row = $sql->fetch_assoc()){
												if($post_row['playlists'] == $row['name']){
													$playlists_checked = "checked";
												}else{
													$playlists_checked = "";
												}
												echo "<label><input type='radio' name='playlists' value='".htmlspecialchars($row['name'], ENT_QUOTES)."' ".$playlists_checked.">[".$row['count']."] ".$row['name']."</label><br>";
											}
										?>
									</div>
									<hr/>
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="namePlaylist" id="namePlaylist" class="form-control" placeholder="Playlist hinzufügen">
										</div>
										<div class="col-md-6">
											<button type="button" class="btn btn-inverse btn-block" onclick="addPlaylist();">Hinzufügen</button>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div id="tournament-groups">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Schlagworte</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="tagbox">
										<span class="tags">
											<span id="addTag">
												<?
													$tag = explode(",", $post_row['tags']);
													foreach($tag as $tag) {
														$tag = trim($tag);
														$tag = htmlspecialchars($tag, ENT_QUOTES);
														$tags .= "<a class='tag' data-value='".$tag."'><span class='label label-default'><i class='fa fa-tag'></i> ".$tag." <i class='fa fa-times'></i></span>&ensp;</a>";
													}
													if(strlen($tag) > 0){
														echo $tags;
													}
												?>
											</span>
										</span>
										<? echo "<input type='hidden' name='tags' class='taghidden' value='".htmlspecialchars($post_row['tags'], ENT_QUOTES)."'>"; ?>
										<hr/>
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control nameTag" placeholder="Tag hinzufügen">
											</div>
											<div class="col-md-6">
												<button type="button" class="btn btn-inverse btn-block addTag">Hinzufügen</button>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="tournament-groups">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Thumbnail</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<?
										if(!file("https://images.chaosdidi.de/uploads/".$post_row['image_link'])){
						                    $image_link_ = "no-cover.jpg";
						                }else{
						                    $image_link_ = $post_row['image_link'];
						                }

										echo "<center>";
											echo "<img src='//images.chaosdidi.de/uploads/".$image_link_."' class='thumbnail-max-size img-responsive img-thumbnail'>";
											echo "<hr/>";
											echo "<input type='file' name='image_link'>";
										echo "</center>";
									?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>