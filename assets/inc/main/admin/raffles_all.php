<div class="box">
	<h2>Alle Verlosungen</h2>
	<div class="box">
		<h2><u>Aktuelle Verlosung</u></h2>
		<table class="table">
			<thead>
				<tr>
			  		<th width="30%">Datum</th>
			  		<th width="43%">Spielname</th>
			  		<th width="27%">Teilnehmer</th>
				</tr>
		  	</thead>
		  	<tbody>
		  	<?
		  		$sql = $db->query("SELECT * FROM raffle_keys WHERE key_status = 1");
		  		$numParticipates = $db->query("SELECT * FROM raffles_participate")->num_rows;
		  		while($row = $sql->fetch_assoc()){
		  			echo "<tr>";
		  				echo "<td>".date("d.m.Y", $row['start_date'])." - ".date("d.m.Y", $row['end_date'])."</td>";
		  				echo "<td>".$row['game_name']."</td>";
		  				echo "<td>";
		  					echo "<button type='button' class='btn btn-primary' data-toggle='collapse' data-target='#showParticipates'>Teilnehmerliste (".$numParticipates.")</button><br>";
		  					echo "<div class='collapse' id='showParticipates'>";
		  						$showParticipates = $db->query("SELECT * FROM raffles_participate");
		  						while($row = $showParticipates->fetch_assoc()){
		  							echo "<i class='fa fa-arrow-right fa-fw'></i> <a href='MyProfile/".$row['userID']."' target='_blank'>".uda::ID($row['userID'], "username")."</a><br>";
		  						}
		  					echo "</div>";
		  				echo "</td>";
		  			echo "</tr>";
		  		}
		  	?>
		  	</tbody>
		</table>
	</div>

	<div class="box">
		<h2><u>Geplante Verlosungen</u></h2>
		<table class="table">
			<thead>
				<tr>
			  		<th width="30%">Datum</th>
			  		<th width="70%">Spielname</th>
				</tr>
		  	</thead>
		  	<tbody>
		  	<?
		  		$sql1 = $db->query("SELECT * FROM raffle_keys WHERE key_status = 2 ORDER BY start_date DESC");
		  		while($row = $sql1->fetch_assoc()){
		  			echo "<tr>";
		  				echo "<td>".date("d.m.Y", $row['start_date'])." - ".date("d.m.Y", $row['end_date'])."</td>";
		  				echo "<td>".$row['game_name']."</td>";
		  			echo "</tr>";
		  		}
		  	?>
		  	</tbody>
		</table>
	</div>

	<div class="box">
		<h2><u>Abgeschlossene Verlosungen</u></h2>
		<table class="table">
			<thead>
				<tr>
			  		<th width="20%">Datum</th>
			  		<th width="43%">Spielname</th>
			  		<th width="27%">Gewinner</th>
			  		<th width="10%">Teiln.</th>
				</tr>
		  	</thead>
		  	<tbody>
		  	<?
		  		$sql2 = $db->query("SELECT * FROM raffle_keys WHERE key_status = 0 ORDER BY win_date DESC");
		  		while($row = $sql2->fetch_assoc()){
		  			echo "<tr>";
		  				echo "<td>".date("d.m.Y", $row['win_date'])."</td>";
		  				echo "<td>".$row['game_name']."</td>";
		  				echo "<td><a href='MyProfile/".uda::USERNAME($row['win_username'], "id")."' target='_blank'>".$row['win_username']."</a></td>";
		  				echo "<td>".$row['participates']."</td>";
		  			echo "</tr>";
		  		}
		  	?>
		  	</tbody>
		</table>
	</div>
</div>