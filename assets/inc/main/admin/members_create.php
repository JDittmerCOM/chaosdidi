<div class="box registration-form">
	<h2>Neuen Benutzer erstellen</h2>
    
    <?
        if($_GET['success'] == '1'){
            echo good("
                Der Benutzer wurde <strong>erfolgreich</strong> erstellt.<br>
                Es wurde eine Aktivierungsemail an den neuen Benutzer gesendet.
            ");
        }

        if(isset($_POST['sub_register'])){
            $ip = $_SERVER['REMOTE_ADDR'];
            $date = time();
            $username = $db->real_escape_string($_POST['username']);
            $email = $db->real_escape_string($_POST['email']);
            $pass1 = $db->real_escape_string($_POST['pass1']);
            $pass2 = $db->real_escape_string($_POST['pass2']);
            $activate_code = rand(100000000000,999999999999);

            $sql = $db->query("SELECT * FROM users");
            $row = $sql->fetch_assoc();

            //CHECK USERNAME
            if(strlen($username) < 4){
                echo bad("Der <strong>Benutzername</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 4 Zeichen</u></strong> verwenden.");
            }else{
                $check__username = true;
            }

            if($username == $row['username']){
                echo bad("Der <strong>Benutzername</strong> ist bereits <strong><u>vergeben</u></strong>.");
            }else{
                $check__username2 = true;
            }

            if(!preg_match("/^[a-zA-Z0-9_.-]*$/", $username)){
                echo bad("Der <strong>Benutzername</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong><u>a-z A-Z 0-9 _ . -</u></strong> verwenden.");
            }else{
                $check__username3 = true;
            }


            //CHECK EMAIL
            if(strlen($email) < 8){
                echo bad("Die <strong>eMail Adresse</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__email = true;
            }

            if($email == $row['email']){
                echo bad("Die <strong>eMail Adresse</strong> ist bereits <strong><u>vergeben</u></strong>.");
            }else{
                $check__email2 = true;
            }

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $email)){
                echo bad("Die <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong><u>a-z A-Z 0-9 _ . - @</u></strong> verwenden.");
            }else{
                $check__email3 = true;
            }


            //CHECK PASSWORD
            if(strlen($pass1) < 8){
                echo bad("Das <strong>Passwort</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__password = true;
            }

            if($pass1 != $pass2){
                echo bad("Die beiden <strong>Passwörter</strong> sind <strong><u>ungleich</u></strong>.");
            }else{
                $check__password2 = true;
            }

            if($check__password == true && $check__password2 == true){
                $pass1 = crypt($pass1, CRYPT_PW);
            }


            if(
                $check__username == true && $check__username2 == true && $check__username3 == true
                && $check__email == true && $check__email2 == true && $check__email3 == true
                && $check__password == true && $check__password2){

                //CHECK IP-ADDRESS
                $usersIP_sql = $db->query("SELECT * FROM users WHERE username NOT LIKE '%".$username."%' ORDER BY id ASC");
                while($row = $usersIP_sql->fetch_assoc()){
                    $c1 = explode(".", $ip);
                    $c2 = explode(".", $row['ip']);

                    similar_text($c1[0], $c2[0], $erg1);
                    similar_text($c1[1], $c2[1], $erg2);
                    similar_text($c1[2], $c2[2], $erg3);
                    similar_text($c1[3], $c2[3], $erg4);

                    $erg = ($erg1 + $erg2 + $erg3 + $erg4) / 4;

                    if($erg >= 75){
                        $datasIP .= "<div class='row'>";
                            $datasIP .= "<div class='col-md-3 text-right'>".$row['username']."</div>";
                            $datasIP .= "<div class='col-md-9'>".$row['ip']."</div>";
                        $datasIP .= "</div>";
                    }
                }
                $to = "dipa@chaosdidi.de";
                $subject = "[Duplicate IP] Gefahr auf Doppelaccount";
                $message = '
                <html>
                    <head>
                        <title>[Duplicate IP] Gefahr auf Doppelaccount</title>
                        <link href="https://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container-fluid">
                            Es besteht die Möglichkeit, dass <strong>'.$username.'</strong> ein Doppelaccount ist.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername</strong></div>
                                <div class="col-md-9"><strong>IP-Adresse</strong></div>
                            </div>
                            '.$datasIP.'
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to,$subject,$message,$headers);

                $db->query("INSERT INTO users (ip,
                                               `date`,
                                               username,
                                               email,
                                               password,
                                               activate_code) 
                                VALUES ('".$ip."',
                                        '".$date."',
                                        '".$username."',
                                        '".$email."',
                                        '".$pass1."',
                                        '".$activate_code."')");

                $to1 = $email;
                $subject1 = "Deine Registrierung auf ChaosDidi.de";
                $message1 = '
                <html>
                    <head>
                        <title>Deine Registrierung auf ChaosDidi.de</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container-fluid">
                            <img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
                            <br><br>
                            Vielen Dank für deine Registrierung auf ChaosDidi.de :)
                            <br><br>
                            Damit du dich nun auch Einloggen kannst, musst du erst deinen Account aktivieren. Klicke dafür auf den unten stehenden Link.<br>
                            <a href="http://chaosdidi.de/Activate/'.$activate_code.'">http://chaosdidi.de/Activate/'.$activate_code.'</a>
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong><u>Deine Logindaten:</u></strong></div>
                                <div class="col-md-9"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
                                <div class="col-md-9">'.$username.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Passwort:</strong></div>
                                <div class="col-md-9">'.$pass2.'</div>
                            </div>
                            <hr/>
                            Viel Spaß wünscht dir,<br>
                            Das ChaosDidi Team<br><br>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers1 = "MIME-Version: 1.0" . "\n";
                $headers1 .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers1 .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to1,$subject1,$message1,$headers1);


                $to2 = "admin@chaosdidi.de";
                $subject2 = "[Neues Mitglied] Ein neues Mitglied hat sich auf ChaosDidi registriert";
                $message2 = '
                <html>
                    <head>
                        <title>[Neues Mitglied] Ein neues Mitglied hat sich auf ChaosDidi registriert</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container-fluid">
                            Ein <strong>neues Mitglied</strong> hat sich heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> registriert.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
                                <div class="col-md-9">'.$ip.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
                                <div class="col-md-9">'.$username.'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>eMail Adresse:</strong></div>
                                <div class="col-md-9">'.$email.'</div>
                            </div>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers2 = "MIME-Version: 1.0" . "\n";
                $headers2 .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers2 .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to2,$subject2,$message2,$headers2);

                header("Location:".SERVER_NAME."Admin/Members/Create/S1");
            }
        }
    ?>

    <form method="post">
    	<div class="form-group">
            <label for="reg_username">Benutzername</label>
            <input type="text" name="username" class="form-control" id="reg_username" placeholder="Benutzername" <? echo "value='".$username."'"; ?> maxlength="35" required>
        </div>
        <div class="form-group">
            <label for="reg_email">eMail Adresse</label>
            <input type="email" name="email" class="form-control" id="reg_email" placeholder="eMail Adresse" <? echo "value='".$email."'"; ?> maxlength="100" required>
        </div>
        <div class="form-group">
            <label for="reg_pass">Passwort</label>
            <input type="password" name="pass1" class="form-control" id="reg_pass" placeholder="Passwort" maxlength="35" required>
        </div>
        <div class="form-group">
            <label for="reg_pass2">Passwort erneut eingeben</label>
            <input type="password" name="pass2" class="form-control" id="reg_pass2" placeholder="Passwort" maxlength="35" required>
        </div>
        <button type="submit" name="sub_register" class="btn btn-primary">Registrieren</button>
    </form>
</div>