<div class="box">
	<h2>Verlosung erstellen</h2>

	<?
		$rs_sql = $db->query("SELECT * FROM raffle_keys ORDER BY end_date DESC LIMIT 1");
		$rs_row = $rs_sql->fetch_assoc();

		if(isset($_POST['create_sub'])){
			$username = USERNAME;
			$date = time();
			$game_name = htmlentities($_POST['game_name'], ENT_QUOTES);
			$crs_planned_time = ($rs_row['start_date'] + 604800);
			$cre_planned_time = ($rs_row['end_date'] + 604800);

			$db->query("UPDATE raffle_keys 
						  SET start_date = '".$crs_planned_time."',
						  	  end_date = '".$cre_planned_time."',
						  	  key_status = '2'
						WHERE game_name = '".$game_name."'");
			
			header("Location:".SERVER_NAME."Admin/Raffles/Create");
		}
	?>

	<form method="post">
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					<select name="game_name" class="form-control" id="game_name" required>
						<option selected disabled>Spiel zum Verlosen auswählen</option>
					<?
						$keys_sql = $db->query("SELECT * FROM raffle_keys WHERE key_status = 3 ORDER BY game_name ASC");
						while($row = $keys_sql->fetch_assoc()){
							if(empty($row['game_price'])){
								$game_price = "--,--";
							}else{
								if($row['game_price'] < 10){
									$game_price = "0".$row['game_price'];
								}else{
									$game_price = $row['game_price'];
								}
							}
							echo "<option value='".$row['game_name']."'>[".$game_price."] ".$row['game_name']."</option>";
						}
					?>
					</select>
				</div>
				<div class="form-group">
				<?
					echo "Diese Verlosung würde vom <strong>".date("d.m.Y", ($rs_row['start_date'] + 604800))."</strong> bis zum <strong>".date("d.m.Y", ($rs_row['end_date'] + 604800))."</strong> laufen.";
				?>
				</div>
				<hr/>
				<div class="form-group">
					<span id="searchResult_CreateRaffle"></span>
				</div>
			</div>
			<div class="col-md-3">
				<button type="submit" name="create_sub" class="btn btn-primary btn-block">Speichern</button>
			</div>
		</div>
	</form>
</div>