<div class="box">
	<h2>Noch nicht Freigeschaltete Mitglieder</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" width="10%">UID</th>
					<th>Datum</th>
					<th>Benutzername</th>
					<th>eMail Adresse</th>
				</tr>
			</thead>
			<tbody>
				<?
					$members_sql = $db->query("SELECT * FROM users WHERE status = 1 ORDER BY id DESC");
					while($row = $members_sql->fetch_assoc()){
						echo "<tr>";
							echo "<td class='text-center'>".$row['id']."</td>";
							echo "<td>".date("d.m.Y", $row['date'])."</td>";
							echo "<td><a href='Admin/Members/User/".$row['id']."'>".$row['username']."</a></td>";
							echo "<td>".$row['email']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>