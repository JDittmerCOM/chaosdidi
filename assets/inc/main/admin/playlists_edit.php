<div class="box">
	<?
		$playlistsID = $_GET['id'];
		$playlists_sql = $db->query("SELECT * FROM playlists WHERE id = '".$playlistsID."'");
		$playlists_row = $playlists_sql->fetch_assoc();
	?>
	<h2>Playlist bearbeiten (<? echo $playlists_row['name'] ?>)</h2>

	<?
		if($_GET['success'] == '1'){
            echo "<div class='alert alert-success' id='success_report'>";
                echo "Dein Beitrag wurde gespeichert.";
            echo "</div>";
        }

		if(isset($_POST['sub_edit'])){
			$playlists = $db->real_escape_string($_POST['playlists']);
			if($playlists != $playlists_row['name']){
				$db->query("UPDATE playlists SET name = '".$playlists."' WHERE id = '".$playlistsID."'");

				$db->query("UPDATE videos SET playlists = '".$playlists."' WHERE playlists = '".$playlists_row['name']."'");

				header("Location:".SERVER_NAME."Admin/Playlists/Edit/".$playlistsID."/S1");
			}else{
				echo bad("Dein Beitrag wurde <b><u>nicht</u></b> gespeichert.<br>Es wurden <b><u>keine</u></b> Änderungen vorgenommen.");
			}
		}

		if(isset($_POST['sub_del'])){
			$db->query("UPDATE videos SET playlists = '' WHERE playlists = '".$playlists_row['name']."'");

			$db->query("DELETE FROM playlists WHERE id = '".$playlistsID."'");

			header("Location:".SERVER_NAME."Admin/Playlists/All");
		}
	?>

	<form method="post">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<input type="text" name="playlists" class="form-control" placeholder="Playlist Name" <? echo "value='".htmlspecialchars($playlists_row['name'], ENT_QUOTES)."'"; ?> required>
				</div>
			</div>
			<div class="col-md-4">
				<button name="sub_edit" class="btn btn-primary btn-block">Speichern</button>
				<button name="sub_del" class="btn btn-danger btn-block" onclick="return confirm('Möchtest du diese Playlist wirklich entfernen?');">Löschen</button>
			</div>
		</div>
	</form>
</div>