<iframe src="assets/inc/main/admin/analytics/iframe_cdmembers.php" width="100%" height="320px" frameborder="0"></iframe>

<?
	function AvgValues($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $date = (strtotime(date("d.m.Y")) - 86400);
        $days = ($days - 1);

        $avg1_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".$date."'");
        $avg2_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".($date - ($days * 86400))."'");
        $avg1_row = $avg1_sql->fetch_assoc();
        $avg2_row = $avg2_sql->fetch_assoc();

        $avg = number_format(($avg1_row['cd_member'] - $avg2_row['cd_member']));

        if($avg < 0){
        	$avg = "<span class='text-danger'><strong>".$avg." Mitglieder</strong></span>";
        }elseif($avg == 0){
        	$avg = "<span class='text-default'><strong>".$avg." Mitglieder</strong></span>";
        }elseif($avg > 0){
        	$avg = "<span class='text-success'><strong>".$avg." Mitglieder</strong></span>";
        }
        return $avg;
    }

    function AvgValues_Banned($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $date = (strtotime(date("d.m.Y")) - 86400);
        $days = ($days - 1);

        $avg1_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".$date."'");
        $avg2_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".($date - ($days * 86400))."'");
        $avg1_row = $avg1_sql->fetch_assoc();
        $avg2_row = $avg2_sql->fetch_assoc();

        $avg = number_format(($avg1_row['cd_member_bans'] - $avg2_row['cd_member_bans']));

        if($avg < 0){
        	$avg = "<span class='text-success'><strong>".$avg." Gesp. Mitglieder</strong></span>";
        }elseif($avg == 0){
        	$avg = "<span class='text-default'><strong>".$avg." Gesp. Mitglieder</strong></span>";
        }elseif($avg > 0){
        	$avg = "<span class='text-danger'><strong>".$avg." Gesp. Mitglieder</strong></span>";
        }
        return $avg;
    }

    echo "<div class='row'>";
    	echo "<div class='col-md-6'>";
			echo "<center><strong><u>Durchschnitts-Werte (&oslash;)</u></strong></center>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>gestern</div>";
				echo "<div class='col-md-6'>".AvgValues(2)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 7 Tage</div>";
				echo "<div class='col-md-6'>".AvgValues(7)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 2 Wochen</div>";
				echo "<div class='col-md-6'>".AvgValues(7*2)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 4 Wochen</div>";
				echo "<div class='col-md-6'>".AvgValues(7*4)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 3 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues(7*4*3)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 6 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues(7*4*6)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 12 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues(7*4*12)."</div>";
			echo "</div>";
		echo "</div>";

		echo "<div class='col-md-6'>";
			echo "<center><strong><u>Durchschnitts-Werte (&oslash;)</u></strong></center>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>gestern</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(2)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 7 Tage</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 2 Wochen</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7*2)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 4 Wochen</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7*4)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 3 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7*4*3)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 6 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7*4*6)."</div>";
			echo "</div>";
			echo "<div class='row'>";
				echo "<div class='col-md-6 text-right'>letzten 12 Monate</div>";
				echo "<div class='col-md-6'>".AvgValues_Banned(7*4*12)."</div>";
			echo "</div>";
		echo "</div>";
	echo "</div>";
?>