<iframe src="assets/inc/main/admin/analytics/iframe_ytcounts.php" width="100%" height="320px" frameborder="0"></iframe>

<?
	function AvgValues($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $date = (strtotime(date("d.m.Y")) - 86400);
        $days = ($days - 1);

        $avg1_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".$date."'");
        $avg2_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".($date - ($days * 86400))."'");
        $avg1_row = $avg1_sql->fetch_assoc();
        $avg2_row = $avg2_sql->fetch_assoc();

        $avg = number_format(($avg1_row['yt_aufrufe'] - $avg2_row['yt_aufrufe']));

        if($avg < 0){
        	$avg = "<span class='text-danger'><strong>".$avg." Aufrufe</strong></span>";
        }elseif($avg == 0){
        	$avg = "<span class='text-default'><strong>".$avg." Aufrufe</strong></span>";
        }elseif($avg > 0){
        	$avg = "<span class='text-success'><strong>".$avg." Aufrufe</strong></span>";
        }
        return $avg;
    }

	echo "<center><strong><u>Durchschnitts-Werte (&oslash;)</u></strong></center>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>gestern</div>";
		echo "<div class='col-md-6'>".AvgValues(2)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 7 Tage</div>";
		echo "<div class='col-md-6'>".AvgValues(7)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 2 Wochen</div>";
		echo "<div class='col-md-6'>".AvgValues(7*2)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 4 Wochen</div>";
		echo "<div class='col-md-6'>".AvgValues(7*4)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 3 Monate</div>";
		echo "<div class='col-md-6'>".AvgValues(7*4*3)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 6 Monate</div>";
		echo "<div class='col-md-6'>".AvgValues(7*4*6)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-6 text-right'>letzten 12 Monate</div>";
		echo "<div class='col-md-6'>".AvgValues(7*4*12)."</div>";
	echo "</div>";
?>