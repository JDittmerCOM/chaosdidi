<div class="box">
	<h2>Playlisten</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Videos die sich in dieser Playliste befinden</th>
				</tr>
			</thead>
			<tbody>
				<?
					$playlists_sql = $db->query("SELECT * FROM playlists ORDER BY name ASC");
					while($row = $playlists_sql->fetch_assoc()){
						echo "<tr>";
							echo "<td><a href='Admin/Playlists/Edit/".$row['id']."'>".$row['name']."</a></td>";
							echo "<td>".$row['count']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>