<div class="box">
	<h2>Gesperrte Mitglieder</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Datum</th>
					<th>Benutzername</th>
					<th>Nachricht</th>
					<th>Gesperrt von</th>
				</tr>
			</thead>
			<tbody>
				<?
					$members_sql = $db->query("SELECT * FROM users_banned ORDER BY id DESC");
					while($row = $members_sql->fetch_assoc()){
						echo "<tr>";
							echo "<td>".date("d.m.Y", $row['date'])."</td>";
							echo "<td><a href='Admin/Members/User/".$row['userID']."' target='_blank'>".uda::ID($row['userID'], "username")."</a></td>";
							echo "<td>".$row['message']."</td>";
							echo "<td>".$row['by_admin']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>