<div class="box">
	<h2>Umfrage bearbeiten</h2>

	<?
		$questionID = $_GET['id'];
		$survey_sql = $db->query("SELECT * FROM surveys WHERE questionID = ".$questionID."");
		$survey_row = $survey_sql->fetch_assoc();

		if($_GET['success'] == '1'){
			echo good("Die <strong>Umfrage</strong> wurde <strong><u>gespeichert</u></strong>.");
		}

		if(isset($_POST['del_survey'])){
			$db->query("DELETE FROM surveys WHERE questionID = '".$questionID."'");
			header("Location:".SERVER_NAME."Admin/Surveys/All");
		}

		if(isset($_POST['sub_survey'])){
			$question = $_POST['question'];

			if($question != $survey_row['question']){$db->query("UPDATE surveys SET question = '".$question."' WHERE questionID = '".$questionID."'");}

			$survey_update_number = 0;
			while($survey_update_number <= 10){
				$survey_update_number++;
				$pos = $_POST['pos'.$survey_update_number];
				$answer = $_POST['answer'.$survey_update_number];
				$answerID = $_POST['answerID'.$survey_update_number];
				$answerOLD = $_POST['answerOLD'.$survey_update_number];
				
				$survey_answer_nums = $db->query("SELECT * FROM surveys WHERE questionID = '".$questionID."' AND answer = '".$answerOLD."'")->num_rows;
				if(strlen($answer) >= 1 && $survey_answer_nums == 0){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".time()."', '".$questionID."', '".$pos."', '".$question."', '".$answer."', '".rand(10000,99999)."')");}else{$db->query($db, "UPDATE surveys SET pos = '".$pos."', answer = '".$answer."' WHERE questionID = '".$questionID."' AND answerID = '".$answerID."'");}
			}

			header("Location:".SERVER_NAME."Admin/Surveys/Edit/".$questionID."/S1");
		}
	?>

	<form method="post">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<input type="text" name="question" class="form-control" placeholder="Frage der Umfrage" <? echo "value='".$survey_row['question']."'"; ?> required>
				</div>
				<hr/>
				<div class="row"><div class="col-md-2 text-center"><strong>Pos.</strong></div><div class="col-md-10"><strong>&emsp;Antwort</strong></div></div>
				<?
					$answer_count_sql = $db->query("SELECT * FROM surveys WHERE questionID = '".$questionID."'")->num_rows;
					$answer_sql = $db->query("SELECT * FROM surveys WHERE questionID = '".$questionID."' ORDER BY pos ASC");
					while($row = $answer_sql->fetch_assoc()){
						echo "<div id='fieldID_".$row['answerID']."' class='form-group'>";
							echo "<div class='row'>";
								echo "<div class='col-md-2'><input type='text' name='pos".$row['pos']."' class='form-control' placeholder='".$row['pos']."' value='".$row['pos']."' maxlenght='2'></div>";
								echo "<div class='col-md-9'><input type='text' name='answer".$row['pos']."' class='form-control' placeholder='Antwort ".$row['pos']."' value='".$row['answer']."'></div>";
								echo "<button type='button' class='btn btn-danger' onclick='delSurveyFieldEDIT(".$questionID.",".$row['pos'].",".$row['answerID'].");'><i class='fa fa-trash fa-fw'></i></button>";
								echo "<input type='hidden' name='answerID".$row['pos']."' value='".$row['answerID']."'>";
								echo "<input type='hidden' name='answerOLD".$row['pos']."' value='".$row['answer']."'>";
							echo "</div>";
						echo "</div>";
					}
					echo "<input type='hidden' id='SurveyCountEDIT' value='".$answer_count_sql."'>";
				?>
				<div id="addSurveyFieldEDIT"></div>
				<button type="button" id="addSurveyFieldButtonEDIT" class="btn btn-primary">weiteres Feld hinzufügen</button>
			</div>
			<div class="col-md-4">
				<button type="submit" name="sub_survey" class="btn btn-primary btn-block">Speichern</button>
				<button type="submit" name="del_survey" class="btn btn-danger btn-block" onclick="return confirm('Möchtest du diese Umfrage wirklich entfernen?');">Löschen</button>
			</div>
		</div>
	</form>
</div>