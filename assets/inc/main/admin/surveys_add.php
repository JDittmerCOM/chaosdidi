<div class="box">
	<h2>Umfrage hinzufügen</h2>

	<?
		if($_GET['success'] == '1'){
			echo good("Die <strong>Umfrage</strong> wurde <strong><u>veröffentlicht</u></strong>.");
		}

		if(isset($_POST['sub_survey'])){
			$date = time();
			$questionID = rand(1000000000, $date);
			$question = $_POST['question'];
			$pos1 = $_POST['pos1']; $answer1 = $_POST['answer1'];
			$pos2 = $_POST['pos2']; $answer2 = $_POST['answer2'];
			$pos3 = $_POST['pos3']; $answer3 = $_POST['answer3'];
			$pos4 = $_POST['pos4']; $answer4 = $_POST['answer4'];
			$pos5 = $_POST['pos5']; $answer5 = $_POST['answer5'];
			$pos6 = $_POST['pos6']; $answer6 = $_POST['answer6'];
			$pos7 = $_POST['pos7']; $answer7 = $_POST['answer7'];
			$pos8 = $_POST['pos8']; $answer8 = $_POST['answer8'];
			$pos9 = $_POST['pos9']; $answer9 = $_POST['answer9'];
			$pos10 = $_POST['pos10']; $answer10 = $_POST['answer10'];

			if(!empty($pos1) && !empty($answer1)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos1."', '".$question."', '".$answer1."', '".rand(10000,99999)."')");
			if(!empty($pos2) && !empty($answer2)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos2."', '".$question."', '".$answer2."', '".rand(10000,99999)."')");
			if(!empty($pos3) && !empty($answer3)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos3."', '".$question."', '".$answer3."', '".rand(10000,99999)."')");
			if(!empty($pos4) && !empty($answer4)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos4."', '".$question."', '".$answer4."', '".rand(10000,99999)."')");
			if(!empty($pos5) && !empty($answer5)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos5."', '".$question."', '".$answer5."', '".rand(10000,99999)."')");
			if(!empty($pos6) && !empty($answer6)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos6."', '".$question."', '".$answer6."', '".rand(10000,99999)."')");
			if(!empty($pos7) && !empty($answer7)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos7."', '".$question."', '".$answer7."', '".rand(10000,99999)."')");
			if(!empty($pos8) && !empty($answer8)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos8."', '".$question."', '".$answer8."', '".rand(10000,99999)."')");
			if(!empty($pos9) && !empty($answer9)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos9."', '".$question."', '".$answer9."', '".rand(10000,99999)."')");
			if(!empty($pos10) && !empty($answer10)){$db->query("INSERT INTO surveys (`date`, questionID, pos, question, answer, answerID) VALUES ('".$date."', '".$questionID."', '".$pos10."', '".$question."', '".$answer10."', '".rand(10000,99999)."')");

			header("Location:".SERVER_NAME."Admin/Surveys/Add/S1");
		}
	?>

	<form method="post">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<input type="text" name="question" class="form-control" placeholder="Frage der Umfrage" required>
				</div>
				<hr/>
				<div class="row"><div class="col-md-2 text-center"><strong>Pos.</strong></div><div class="col-md-10"><strong>&emsp;Antwort</strong></div></div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"><input type="text" name="pos1" class="form-control" placeholder="1" value="1" maxlength="2" required></div>
						<div class="col-md-10"><input type="text" name="answer1" class="form-control" placeholder="Antwort 1" required></div>
					</div>
				</div>
				<div id="addSurveyFieldADD"></div>
				<button type="button" id="addSurveyFieldButtonADD" class="btn btn-primary">weiteres Feld hinzufügen</button>
			</div>
			<div class="col-md-4">
				<button type="submit" name="sub_survey" class="btn btn-primary btn-block">Veröffentlichen</button>
			</div>
		</div>
	</form>
</div>