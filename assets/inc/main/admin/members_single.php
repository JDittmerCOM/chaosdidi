<div class="box">
	<h2><a href="Admin/Members/All">Mitglieder</a> <i class="fa fa-arrow-right fa-fw"></i> Informationen eines Mitglieds</h2>

	<?
		$user_sql = $db->query("SELECT * FROM users WHERE id = '".$_GET['id']."'");
		$user_row = $user_sql->fetch_assoc();

		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'></div>";
			echo "<div class='col-md-9'><img src='//images.chaosdidi.de/avatars/".$user_row['avatar']."' class='img-rounded img-responsive' style='max-height:100px;'></div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>IP-Adresse:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['ip']."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Letzte IP-Adresse:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['last_ip']."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Registrierungs Datum:</strong></div>";
			echo "<div class='col-md-9'>".date("d.m.Y H:i:s", $user_row['date'])." Uhr</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Letzte Aktivität:</strong></div>";
			echo "<div class='col-md-9'>".date("d.m.Y H:i:s", $user_row['last_activity'])." Uhr</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Letzter Pfad:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['last_path']."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Letzter User Agent:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['last_user_agent']."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>HTTP Language:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['http_lang']."</div>";
		echo "</div>";

		echo "<br>";

		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Benutzername:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['username']."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>eMail Adresse:</strong></div>";
			echo "<div class='col-md-9'>".$user_row['email']."</div>";
		echo "</div>";

		echo "<br>";

		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Facebook Link:</strong></div>";
			echo "<div class='col-md-9'><a href='".$user_row['link_facebook']."' target='_blank'>".$user_row['link_facebook']."</a></div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Twitter Link:</strong></div>";
			echo "<div class='col-md-9'><a href='".$user_row['link_twitter']."' target='_blank'>".$user_row['link_twitter']."</a></div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>YouTube Link:</strong></div>";
			echo "<div class='col-md-9'><a href='".$user_row['link_youtube']."' target='_blank'>".$user_row['link_youtube']."</a></div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Google Plus Link:</strong></div>";
			echo "<div class='col-md-9'><a href='".$user_row['link_gplus']."' target='_blank'>".$user_row['link_gplus']."</a></div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Steam Link:</strong></div>";
			echo "<div class='col-md-9'><a href='".$user_row['link_steam']."' target='_blank'>".$user_row['link_steam']."</a></div>";
		echo "</div>";

		echo "<br>";

		if($user_row['status'] == 0){
			$users_banned_sql = $db->query("SELECT * FROM users_banned WHERE userID = '".$user_row['id']."'");
			$users_banned_row = $users_banned_sql->fetch_assoc();

			$status = "<span class='text-danger'>ACCOUNT GESPERRT</span>";
			$status .= "<br>";
			$status .= "<div class='bg-danger'>".$users_banned_row['message']."</div>";
			$status .= "<br>";
			$activate_code = "https://chaosdidi.de/LoginActivate/".$user_row['activate_code'];
		}
		if($user_row['status'] == 1){
			$status = "<span class='text-info'>NOCH NICHT FREIGESCHALTET</span>";
			$activate_code = "https://chaosdidi.de/Activate/".$user_row['activate_code'];
		}
		if($user_row['status'] == 2){
			$status = "<span class='text-success'>FREIGESCHALTET</span>";
			$activate_code = "-/-";
		}
				
		if($user_row['online'] == 0 || (time() - $user_row['last_activity']) >= 1440){$online = "<span class='text-danger'>OFFLINE</span>";}
		if($user_row['online'] == 1 && (time() - $user_row['last_activity']) >= 300 && (time() - $user_row['last_activity']) < 1440){$online = "<span class='text-warning'>INAKTIV</span>";}
		if($user_row['online'] == 1 && (time() - $user_row['last_activity']) < 300){$online = "<span class='text-success'>ONLINE</span>";}
		
		if($user_row['mod'] == 0){$mod = "<span class='text-danger'>NEIN</span>";}else{$mod = "<span class='text-success'>JA</span>";}
		if($user_row['adm'] == 0){$adm = "<span class='text-danger'>NEIN</span>";}else{$adm = "<span class='text-success'>JA</span>";}

		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Account Status:</strong></div>";
			echo "<div class='col-md-9'>".$status."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Freischalt-Link:</strong></div>";
			echo "<div class='col-md-9'>".$activate_code."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Online Status:</strong></div>";
			echo "<div class='col-md-9'>".$online."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Moderatoren-Rechte:</strong></div>";
			echo "<div class='col-md-9'>".$mod."</div>";
		echo "</div>";
		echo "<div class='row'>";
			echo "<div class='col-md-3 text-right'><strong>Administratoren-Rechte:</strong></div>";
			echo "<div class='col-md-9'>".$adm."</div>";
		echo "</div>";

		echo "<hr/>";

		echo "<div class='row'>";
			echo "<div class='col-md-3 col-md-offset-3'>";
				echo "<button type='button' class='btn btn-danger btn-block' onclick='delMember(".$user_row['id'].");'>Benutzer Löschen</button>";
			echo "</div>";
			if($user_row['status'] != 0){
				echo "<div class='col-md-3'>";
					echo "<button type='button' class='btn btn-warning btn-block' data-toggle='modal' data-target='#BanUser'>Benutzer sperren</button>";
				echo "</div>";
			}else{
				echo "<div class='col-md-3'>";
					echo "<button type='button' class='btn btn-success btn-block' data-toggle='modal' data-target='#UnbanUser'>Benutzer entsperren</button>";
				echo "</div>";
			}
			echo "<div class='col-md-1'>";
				echo "<a href='http://print.chaosdidi.de/MemberDatas/".$user_row['id']."' class='btn btn-default btn-block' target='_blank'><i class='fa fa-print'></i></a>";
			echo "</div>";
		echo "</div>";
	?>
</div>

<? // BAN USER [start]
	if(isset($_POST['ban_sub'])){
		$date = time();
		$userID = $_GET['id'];
		$message = $db->real_escape_string($_POST['message']);
		$message = nl2br($message);
		$by_admin = USERNAME;
		$db->query("INSERT INTO users_banned (`date`, 
											  userID, 
											  message, 
											  by_admin) 
								VALUES ('".$date."', 
										'".$userID."', 
										'".$message."', 
										'".$by_admin."')
		");

		$db->query("UPDATE users SET status = 0 WHERE id = '".$_GET['id']."'");

		$db->query("DELETE FROM raffles_participate WHERE userID = '".$_GET['id']."'");

		/* EMAIL AN ADMIN [start] */
		$mail_to = "ban@chaosdidi.de";
		$mail_subject = "[User Banned] ".uda::ID($userID, "username")." wurde gesperrt";
		$mail_message = '
		<html>
			<head>
				<title>[User Banned] '.uda::ID($userID, "username").' wurde gesperrt</title>
				<link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
			</head>
			<body>
				<br>
				<div class="container">
					Das Mitglied <strong>'.uda::ID($userID, "username").'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>'.$by_admin.'</strong> gesperrt.<br>
					<hr/>
					<div class="row">
						<div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
						<div class="col-md-9">'.$message.'</div>
					</div>
				</div>
			</body>
		</html>
		';

		// Always set content-type when sending HTML email
		$mail_headers = "MIME-Version: 1.0" . "\n";
		$mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
		$mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

		mail($mail_to,$mail_subject,$mail_message,$mail_headers);
		/* EMAIL AN ADMIN [end] */


		/* EMAIL AN BENUTZER [start] */
		$mail2_to = uda::ID($userID, "email");
		$mail2_subject = "[ChaosDidi.de] Dein Benutzerkonto wurde gesperrt";
		$mail2_message = '
		<html>
			<head>
				<title>[ChaosDidi.de] Dein Benutzerkonto wurde gesperrt</title>
				<link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
			</head>
			<body>
				<br>
				<div class="container">
					<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
					<br><br>
					Dein Benutzerkonto <strong>'.uda::ID($userID, "username").'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>'.$by_admin.'</strong> gesperrt.<br>
					<hr/>
					<div class="row">
						<div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
						<div class="col-md-9">'.$message.'</div>
					</div>
					<hr/>
					<u>Missverständnis?</u> <a href="mailto:support@chaosdidi.de?subject=Mein Benutzerkonto wurde gesperrt&body=Nutzer-ID: '.$userID.'%0AGrund: (Hier bitte deinen Grund zur Entsperrung angeben)"><strong>Hier</strong></a> melden!
					<br><br><br>

					Einen schönen Tag wünscht dir,<br>
					Das ChaosDidi Team<br><br>
				</div>
			</body>
		</html>
		';

		// Always set content-type when sending HTML email
		$mail2_headers = "MIME-Version: 1.0" . "\n";
		$mail2_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
		$mail2_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

		mail($mail2_to,$mail2_subject,$mail2_message,$mail2_headers);
		/* EMAIL AN BENUTZER [end] */

		header("Location:".SERVER_NAME."Admin/Members/Closures");
	}
	// BAN USER [end]
?>


<? // UNBAN USER [start]
	if(isset($_POST['unban_sub'])){
		$date = time();
		$userID = $_GET['id'];
		$message = $db->real_escape_string($_POST['message']);
		$message = nl2br($message);
		$by_admin = USERNAME;

		$db->query("DELETE FROM users_banned WHERE userID = '".$userID."'");

		$db->query("UPDATE users SET status = 2 WHERE id = '".$_GET['id']."'");

		/* EMAIL AN ADMIN [start] */
		$mail_to = "ban@chaosdidi.de";
		$mail_subject = "[User Unbanned] ".uda::ID($userID, "username")." wurde entsperrt";
		$mail_message = '
		<html>
			<head>
				<title>[User Unbanned] '.uda::ID($userID, "username").' wurde entsperrt</title>
				<link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
			</head>
			<body>
				<br>
				<div class="container">
					Das Mitglied <strong>'.uda::ID($userID, "username").'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>'.$by_admin.'</strong> entsperrt.<br>
					<hr/>
					<div class="row">
						<div class="col-md-3 text-right"><strong>Grund für die Entsperrung:</strong></div>
						<div class="col-md-9">'.$message.'</div>
					</div>
				</div>
			</body>
		</html>
		';

		// Always set content-type when sending HTML email
		$mail_headers = "MIME-Version: 1.0" . "\n";
		$mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
		$mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

		mail($mail_to,$mail_subject,$mail_message,$mail_headers);
		/* EMAIL AN ADMIN [end] */
		

		/* EMAIL AN BENUTZER [start] */
		$mail2_to = uda::ID($userID, "email");
		$mail2_subject = "[ChaosDidi.de] Dein Benutzerkonto wurde entsperrt";
		$mail2_message = '
		<html>
			<head>
				<title>[ChaosDidi.de] Dein Benutzerkonto wurde entsperrt</title>
				<link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
			</head>
			<body>
				<br>
				<div class="container">
					<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
					<br><br>
					Dein Benutzerkonto <strong>'.uda::ID($userID, "username").'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>'.$by_admin.'</strong> entsperrt.<br>
					<hr/>
					<div class="row">
						<div class="col-md-3 text-right"><strong>Grund für die Entsperrung:</strong></div>
						<div class="col-md-9">'.$message.'</div>
					</div>
					<br><br>

					Einen schönen Tag wünscht dir,<br>
					Das ChaosDidi Team<br><br>
				</div>
			</body>
		</html>
		';

		// Always set content-type when sending HTML email
		$mail2_headers = "MIME-Version: 1.0" . "\n";
		$mail2_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
		$mail2_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

		mail($mail2_to,$mail2_subject,$mail2_message,$mail2_headers);
		/* EMAIL AN BENUTZER [end] */

		header("Location:".SERVER_NAME."Admin/Members/User/".$userID);
	}
?>

<form method="post">
	<div class="modal fade" id="BanUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Benutzer sperren</h4>
				</div>
				<div class="modal-body">
					<textarea name="message" class="form-control" placeholder="Meldegrund" required></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-inverse" data-dismiss="modal">Schließen</button>
					<button type="submit" name="ban_sub" class="btn btn-primary">Benutzer sperren</button>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="post">
	<div class="modal fade" id="UnbanUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Benutzer entsperren</h4>
				</div>
				<div class="modal-body">
					<textarea name="message" class="form-control" placeholder="Entsperrgrund" required></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-inverse" data-dismiss="modal">Schließen</button>
					<button type="submit" name="unban_sub" class="btn btn-primary">Benutzer entsperren</button>
				</div>
			</div>
		</div>
	</div>
</form>