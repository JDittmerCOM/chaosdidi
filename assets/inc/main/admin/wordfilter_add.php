<div class="box">
	<h2>Wortfilter <i class="fa fa-arrow-right fa-fw"></i> Wort hinzufügen</h2>

	<div class="row">
		<div class="col-md-4">
			<form method="post">
				<div class="form-group">
					<input type="text" name="word" id="word" class="form-control" placeholder="Wort (z. B. Arschloch)">
				</div>
				<button type="button" class="btn btn-primary btn-block" onclick="wordfilter_add();">Hinzufügen</button>
			</form>
		</div>
		<div class="col-md-8">
			<div id="tournament-groups">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Wort</th>
							<th>Ersetzen durch</th>
						</tr>
					</thead>
					<tbody id="addWord">
						<?
							$sql = $db->query("SELECT * FROM wordfilter ORDER BY word ASC");
							while($row = $sql->fetch_assoc()){
								echo "<tr>";
									echo "<td>".$row['word']."</td>";
									echo "<td>".$row['word_replace']."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>