<div class="box">
	<h2>Alle Umfragen</h2>

	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
                <tr>
                    <th>Frage</th>
                    <th>Daran teilgenommene Mitglieder</th>
                </tr>
            </thead>
            <tbody>
			<?
				$surveys_sql = $db->query("SELECT * FROM surveys GROUP BY questionID ORDER BY id DESC");
				while($row = $surveys_sql->fetch_assoc()){
					$survey_count = $db->query("SELECT SUM(reviews) AS reviews FROM surveys WHERE questionID = '".$row['questionID']."'")->fetch_assoc();
					echo "<tr>";
						echo "<td><a href='Admin/Surveys/Edit/".$row['questionID']."'>".$row['question']."</a></td>";
						echo "<td>".$survey_count['reviews']."</td>";
					echo "</tr>";
				}
			?>
			</tbody>
		</table>
	</div>
</div>