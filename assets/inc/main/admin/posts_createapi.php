<div class="box">
	<h2>Beitrag erstellen (YouTube API)</h2>

	<?
		if($_GET['success'] == '1'){
			echo "<div class='alert alert-success' id='success_report'>";
				echo "Dein Beitrag wurde veröffentlicht.";
			echo "</div>";
		}

		if(isset($_POST['sub_post'])){
			$yt_link = $db->real_escape_string(substr($_POST['yt_link'], -11, 11));

			$json = file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$yt_link.'&key=AIzaSyAXeU4P8HJRQKTTaErpt81Vyw6cBlLVyEo&part=snippet,contentDetails,statistics,status');
			$yt_obj = json_decode($json, true);

			
			$date = time();
			$author = USERNAME;
			$title = $db->real_escape_string($yt_obj['items'][0]['snippet']['title']);
			$published = substr($yt_obj['items'][0]['snippet']['publishedAt'], 8, 2)."-";
			$published .= substr($yt_obj['items'][0]['snippet']['publishedAt'], 5, 2)."-";
			$published .= substr($yt_obj['items'][0]['snippet']['publishedAt'], 0, 4);
			$published = strtotime($published);
			$description = $db->real_escape_string(str_replace(array("\r", "\n"), "<br>", $yt_obj['items'][0]['snippet']['description']));
			$playlists = $db->real_escape_string($_POST['playlists']);
			$tags = $db->real_escape_string($_POST['tags']);
			$image_link = $yt_obj['items'][0]['snippet']['thumbnails']['maxres']['url'];
			$image_link_mobi = $yt_obj['items'][0]['snippet']['thumbnails']['medium']['url'];

			// ---- THUMBNAIL FOR NON-MOBILES [start]
			$imageType = explode("maxresdefault.", $image_link);
            $image_link_short = $yt_link.".".$imageType[1];

			$remote_file = '/images.chaosdidi.de/uploads/'.$image_link_short;
            $file = $image_link;
            $conn_id = ftp_connect("hostblock.de");
            $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

            ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
            ftp_close($conn_id);
            // ---- THUMBNAIL FOR NON-MOBILES [end]

            // ---- THUMBNAIL FOR MOBILES [start]
			$imageType = explode("mqdefault.", $image_link_mobi);
            $image_link_short_mobi = $yt_link."-MOBI.".$imageType[1];

			$remote_file = '/images.chaosdidi.de/uploads/'.$image_link_short_mobi;
            $file = $image_link_mobi;
            $conn_id = ftp_connect("hostblock.de");
            $login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

            ftp_put($conn_id, $remote_file, $file, FTP_ASCII);
            ftp_close($conn_id);
            // ---- THUMBNAIL FOR MOBILES [end]

			$ytID = $yt_obj['items'][0]['id'];
			$planned_time = $_POST['planned_time'];

			//CHECK POST IMAGE
			if($_FILES['image_link']['size'] < 3072000){
				$__check_image_link = true;
			}else{
				$__check_image_link = false;
				echo bad("Das Beitragsbild ist zu groß.<br><strong>Maximal 3,0 MB!</strong>");
			}

			//CHECK TAGS
			if(strlen($tags) == 0){
				$count_null = 0;
				$count = count($yt_obj['items'][0]['snippet']['tags']);

				$auto_tags = "";
				while ($count_null < $count) {
					$auto_tags .= str_replace(array(" ", "(", ")", "[", "]", ":"), array(",", "", "", "", "", ""), $yt_obj['items'][0]['snippet']['tags'][$count_null]).",";
					$count_null++;
				}
				$auto_tags = implode(',', array_unique(explode(',', $auto_tags)));
				$tags = substr_replace($auto_tags, '', -1, 1);
				$tags = $db->real_escape_string($tags);
			}

			//PLANNED POST
			if(empty($planned_time)){
				$planned_status = "0";
				$__check__planned_post = true;
			}else{
				$planned_status = "1";
				$planned_time = strtotime($planned_time);
				$__check__planned_post = true;
			}


			if($__check_image_link == true && $__check__planned_post == true){
				$db->query("INSERT INTO videos (`date`,
											    published,
											    author,
											    title, 
											    description,
											    playlists,
											    tags, 
											    image_link,
											    ytID,
											    planned_status,
											    planned_time) 
								VALUES ('".$date."',
										'".$published."',
										'".$author."', 
										'".$title."', 
										'".$description."', 
										'".$playlists."', 
										'".$tags."', 
										'".$image_link_short."',
										'".$ytID."',
										'".$planned_status."',
										'".$planned_time."')
				");

				$db->query("UPDATE playlists SET count = (count + 1) WHERE name = '".$playlists."'");

				if(strlen($tags) > 0){
					$tag_size = array("12", "14", "16", "18", "20");

					$tags_count = explode(",", $tags);
					$tags_count_null = -1;
					while ($tags_count_null <= count($tags_count)-2) {
						$tags_count_null++;
						$db->query("INSERT INTO tags (tag, size) VALUES ('".$tags_count[$tags_count_null]."', '".$tag_size[array_rand($tag_size)]."')");
					}
				}

				// POST ON FACEBOOK [start]
			    define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/../../../facebook/');
			    require_once __DIR__ . '/../../../facebook/autoload.php';

			    $fb = new Facebook\Facebook([
			        'app_id' => FB_APP_ID,
			        'app_secret' => FB_APP_SECRET,
			        'default_graph_version' => FB_APP_VERSION,
			    ]);

			    $postid_sql = $db->query("SELECT * FROM videos WHERE title = '".$title."'");
			    $postid_row = $postid_sql->fetch_assoc();

			    $linkData = [
			        'description' => PAGE_DESCRIPTION,
			        'link' => "http://chaosdidi.de/Post/".$postid_row['id'],
			        'name' => $title,
			        'picture' => "http://images.chaosdidi.de/uploads/".$image_link_short,
			        'message' => str_replace("<br>", "\n", $description),
			    ];

			    try {
			        // Returns a `Facebook\FacebookResponse` object
			        $response = $fb->post('/me/feed', $linkData, FB_APP_ACCESS_TOKEN);
			    } catch(Facebook\Exceptions\FacebookResponseException $e) {
			        echo 'Graph returned an error: ' . $e->getMessage();
			        exit;
			    } catch(Facebook\Exceptions\FacebookSDKException $e) {
			        echo 'Facebook SDK returned an error: ' . $e->getMessage();
			        exit;
			    }

			    $graphNode = $response->getGraphNode();
			    $graphNodeID = explode("_", $graphNode['id']);
			    $db->query("UPDATE videos SET fb_id = '".FB_APP_ID."_".$graphNodeID[1]."' WHERE id = '".$postid_row['id']."'");
				// POST ON FACEBOOK [end]

				header("Location:http:".SERVER_NAME."Admin/Posts/CreateAPI/S1");
			}
		}
	?>

	<form method="post">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<input type="text" name="yt_link" class="form-control" placeholder="Gib hier den Video Link an" <? echo "value='".$_POST['yt_link']."'"; ?> required>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-5 text-right"><h2>Beitrag Planen</h2></div>
						<div class="col-md-7">
							<div class="input-group date" id="posts_createapi_datetimepicker">
								<input type='text' name="planned_time" class="form-control" placeholder="Für Planen, auf Icon rechts Klicken">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<button type="submit" name="sub_post" class="btn btn-primary btn-block">Veröffentlichen</button>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-6">
				<div id="tournament-groups">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Playlist</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="radio">
										<span id="addPlaylist"></span>
										<?
											$sql = $db->query("SELECT * FROM playlists ORDER BY name ASC");
											while($row = $sql->fetch_assoc()){
												echo "<label><input type='radio' name='playlists' value='".htmlspecialchars($row['name'], ENT_QUOTES)."'>[".$row['count']."] ".$row['name']."</label><br>";
											}
										?>
									</div>
									<hr/>
									<div class="row">
										<div class="col-md-6">
											<input type="text" name="namePlaylist" id="namePlaylist" class="form-control" placeholder="Playlist hinzufügen">
										</div>
										<div class="col-md-6">
											<button type="button" class="btn btn-inverse btn-block" onclick="addPlaylist();">Hinzufügen</button>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div id="tournament-groups">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Schlagworte</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="tagbox">
										<span class="tags">
											<span id="addTag"></span>
										</span>
										<input type="hidden" name="tags" class="taghidden" value="">
										<hr/>
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control nameTag" placeholder="Tag hinzufügen">
											</div>
											<div class="col-md-6">
												<button type="button" class="btn btn-inverse btn-block addTag">Hinzufügen</button>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>