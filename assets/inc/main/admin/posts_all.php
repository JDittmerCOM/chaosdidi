<div class="box">
	<h2>Alle Beiträge</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>Datum</th>
					<th>Titel</th>
					<th>Playlist</th>
					<th>Tags</th>
				</tr>
			</thead>
			<tbody>
				<?
					$videos_sql = $db->query("SELECT * FROM videos ORDER BY id DESC");
					while($row = $videos_sql->fetch_assoc()){
						if($row['planned_status'] == '0'){
							$planned_status = "<i class='fa fa-eye fa-fw'></i>";
						}

						if(strlen($row['title']) >= 35){
							$title = substr($row['title'], 0, 35)." ...";
						}else{
							$title = $row['title'];
						}

						if(strlen($row['tags']) > 0){
							$tags = count(explode(",", $row['tags']));	
						}else{
							$tags = "0";
						}
						echo "<tr>";
							echo "<td>".$planned_status."</td>";
							echo "<td>".$row['id']."</td>";
							echo "<td>".date("d.m.Y", $row['published'])."</td>";
							echo "<td><a href='Admin/Posts/Edit/".$row['id']."'>".$title."</a></td>";
							echo "<td>".$row['playlists']."</td>";
							echo "<td>".$tags."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>