<link href="assets/js/pagination/pagination.css" rel="stylesheet" type="text/css"></link>

<? $query_search = htmlentities($_POST['q']); ?>

<div class="box">
	<h2>Suche nach <u><? if(strlen($query_search) == 0){echo "<i>rein gar nichts</i>";}else{echo $query_search;} ?></u></h2>
</div>

<?
	// Tag Search [start]
	if(preg_match("/#\w+/", $query_search) && isset($_POST['sub_search'])){
		$query = $db->real_escape_string($query_search);
		$query = str_replace("#", "", $query);

		header("Location:".SERVER_NAME."Tag/".$query);
	}
	// Tag Search [end]


	// Member Search [start]
	if(preg_match("/@\w+/", $query_search) && isset($_POST['sub_search'])){
		echo "<div class='box'>";
			echo "<h2>Mitglieder</h2>";

			$query = $db->real_escape_string($query_search);
			$query = str_replace("@", "", $query);

			$query_sql = $db->query("SELECT * FROM users WHERE username LIKE '%".$query."%'");
			if($query_sql->num_rows == 0 || strlen($query) == 0){
				echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Benutzer</u></strong> finden.");
			}else{
				echo "<table class='table'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>Avatar</th>";
							echo "<th>Benutzername</th>";
							echo "<th class='text-center'>Mitglied seit</th>";
							echo "<th class='text-center'>Letzter Besuch</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
						while($row = $query_sql->fetch_assoc()){
							if($row['mod'] == 0 && $row['adm'] == 0){
								if($row['status'] == 2){
									$username = $row['username'];
									$group = "Mitglied";
								}elseif($row['status'] == 1){
									$username = $row['username'];
									$group = "Nicht Freigeschaltet";
								}elseif($row['status'] == 0){
									$username = "<s>".$row['username']."</s>";
									$group = "<p class='text-muted'>Gesperrt</p>";
								}
							}else{
								if($row['adm'] == 1){
									$username = $row['username'];
									$group = "<p class='text-danger'><strong>Administrator</strong></p>";
								}else{
									$username = $row['username'];
									$group = "<p class='text-success'><strong>Moderator</strong></p>";
								}
							}

							echo "<tr>";
								echo "<td><img src='//images.chaosdidi.de/avatars/".$row['avatar']."' class='members_avatar' alt='".$row['avatar']."'></td>";
								echo "<td><a href='MyProfile/".$row['id']."'>".$username."</a><br>".$group."</td>";
								echo "<td class='text-center'>".date("d.m.Y, H:i", $row['date'])."</td>";
								echo "<td class='text-center'>".date("d.m.Y, H:i", $row['last_activity'])."</td>";
							echo "</tr>";
						}
					echo "</tbody>";
				echo "</table>";
			}
		echo "</div>";
	}
	// Member Search [end]


	// Video Search [start]
	if(!preg_match("/@/", $query_search) && isset($_POST['sub_search'])){
		echo "<div class='box'>";
			echo "<h2>Videos</h2>";

			$query = $db->real_escape_string($query_search);

			$query = explode(" ", $query);
			$count_null = -1;
			$count_suche = count($query)-1;
			$query_erg = "";
			while ($count_null < $count_suche) {$count_null++; $query_erg .= "+".$query[$count_null]." ";}

			$query_sql = $db->query("SELECT * FROM videos WHERE MATCH(title) AGAINST('".$query_erg."*' IN BOOLEAN MODE)");
			if($query_sql->num_rows == 0 || strlen($query_search) == 0){
				echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Videos</u></strong> finden.");
			}else{
				echo "<div id='hiddenresult' style='display:none;'>";
					while($row = $query_sql->fetch_assoc()){
						if(strlen($row['description']) >= 300){
		                    $description = htmlspecialchars(str_replace("<br>", "\n", substr($row['description'], 0, 300)." ..."), ENT_QUOTES);
		                }else{
		                    $description = htmlspecialchars(str_replace("<br>", "\n", $row['description']), ENT_QUOTES);
		                }

		                if(!file("https://images.chaosdidi.de/uploads/".$row['image_link'])){
		                    $image_link = "no-cover.jpg";
		                }else{
		                    if(isMobile()){
		                        $resize_mobi = explode(".", $row['image_link']);
		                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
		                        $image_link = $resize_mobi;
		                    }else{
		                    	$resize_mobi = explode(".", $row['image_link']);
		                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
		                        $image_link = $resize_mobi;
		                        //$image_link = $row['image_link'];
		                    }
		                }

		                echo "<article class='post result'>";
		                    echo "<div class='post-date-wrapper'>";
		                        echo "<div class='post-date'>";
		                            echo "<div class='day'>".date("d", $row['published'])."</div>";
		                            echo "<div class='month'>".date("M Y", $row['published'])."</div>";
		                        echo "</div>";
		                        echo "<div class='post-type'>";
		                            echo "<i class='fa fa-video-camera'></i>";
		                        echo "</div>";
		                    echo "</div>";
		                    echo "<div class='post-body'>";
		                        echo "<h2>".htmlspecialchars($row['title'], ENT_QUOTES)."</h2>";
		                        echo "<p><img src='//images.chaosdidi.de/uploads/".$image_link."' class='img-responsive' alt='Thumbnail'>".$description."</p>";
		                        echo "<div class='post-info'>";
		                            echo "<span>Gepostet von: ".$row['author']."</span>";
		                            echo "<a href='Post/".$row['id']."' class='btn btn-inverse'>Zeige mehr</a>";
		                        echo "</div>";
		                    echo "</div>";
		                echo "</article>";
					}
				echo "</div>";

				echo "<div id='Pagination'></div>";
			    echo "<br style='clear:both;' />";
			    echo "<div id='Searchresult'>";
			        echo "<div class='alert alert-warning'>";
			            echo "<i class='fa fa-spinner fa-pulse fa-fw'></i>";
			            echo "Einen Augenblick bitte, wir laden gerade die letzten 100 Folgen für dich :)";
			        echo "</div>";
			    echo "</div>";
			}
		echo "</div>";
	}
	// Video Search [end]
?>