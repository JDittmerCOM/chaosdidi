<div class="box">
	<h2>Alle Mitglieder</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center">MOD/ADM</th>
					<th class="text-center" width="10%">UID</th>
					<th>Datum</th>
					<th>Benutzername</th>
					<th>eMail Adresse</th>
				</tr>
			</thead>
			<tbody>
				<?
					$members_sql = $db->query("SELECT * FROM users ORDER BY id DESC");
					while($row = $members_sql->fetch_assoc()){
						if($row['status'] == 0){$tr_class = "danger";}
						if($row['status'] == 1){$tr_class = "warning";}
						if($row['status'] == 2){$tr_class = "default";}

						if($row['mod'] == 1){$mod = "<i class='fa fa-life-ring fa-fw'></i>";}else{$mod = "";}
						if($row['adm'] == 1){$adm = "<i class='fa fa-diamond fa-fw'></i>";}else{$adm = "";}
						if($row['status'] == 0){$username = "<s>".$row['username']."</s>";}else{$username = $row['username'];}
						echo "<tr class='".$tr_class."'>";
							echo "<td class='text-center'>".$mod."&ensp;".$adm."</td>";
							echo "<td class='text-center'>".$row['id']."</td>";
							echo "<td>".date("d.m.Y", $row['date'])."</td>";
							echo "<td><a href='Admin/Members/User/".$row['id']."'>".$username."</a></td>";
							echo "<td>".$row['email']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>