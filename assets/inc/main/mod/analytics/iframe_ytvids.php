<?

	require("../../../../../db.php");

	for ($i = (1430517600 + SUMMER_TIME_TIMESTAMP); $i < strtotime(date("d.m.Y")); $i += 86400) { 
		$date_1_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".$i."'");
		$date_1_row = $date_1_sql->fetch_assoc();

		$vids .= "{ x: new Date(".date("Y", $i).",".(date("m", $i)-1).",".date("d", $i)."), y: ".$date_1_row['yt_videos']." },";

	}
	
	//echo $aufrufe;

?>


<!DOCTYPE HTML>
<html>

<head>  
	<script type="text/javascript">
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer",
			{

				title:{
					text: "YouTube Videos",
					fontSize: 30
				},
				animationEnabled: true,
				axisX:{
					gridColor: "Silver",
					tickColor: "silver",
					valueFormatString: "DD/MM/YY"

				},                        
				toolTip:{
					shared:true
				},
				theme: "theme2",
				axisY: {
					gridColor: "Silver",
					tickColor: "silver"
				},
				legend:{
					verticalAlign: "center",
					horizontalAlign: "right"
				},
				data: [
				{        
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "YT Videos",
					markerType: "",
					color: "#F08080",
					dataPoints: [
					<? echo $vids ?>
					]
				}

				
				],
			  	legend:{
					cursor:"pointer",
					itemclick:function(e){
				  		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
				  		}else{
							e.dataSeries.visible = true;
				  		}
				  		chart.render();
					}
			  	}
			});
			chart.render();
		}
	</script>
	<script type="text/javascript" src="canvasjs.min.js"></script>
</head>
<body>
	<div id="chartContainer" style="height: 300px; width: 100%;"></div>
</body>
</html>
