<iframe src="assets/inc/main/admin/analytics/iframe_cdvisitors.php" width="100%" height="320px" frameborder="0"></iframe>

<?
	$sql = $db->query("SELECT * FROM unique_visitors GROUP BY `date`")->num_rows;

	function AvgValues1($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $sql = $db->query("SELECT SUM(cd_visitors) AS cd_visitors FROM analytics_stats WHERE `date` BETWEEN '".(strtotime(date("d.m.Y")) - (86400 * $days))."' AND '".(strtotime(date("d.m.Y")) - 86400)."'")->fetch_assoc();
        $avg = "<strong>&asymp; ".number_format(floor($sql['cd_visitors'] / $days))."</strong>";
        return $avg;
    }

    function AvgValues2($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $sql = $db->query("SELECT SUM(cd_visitors_bots) AS cd_visitors_bots FROM analytics_stats WHERE `date` BETWEEN '".(strtotime(date("d.m.Y")) - (86400 * $days))."' AND '".(strtotime(date("d.m.Y")) - 86400)."'")->fetch_assoc();
        $avg = "<span class='text-danger'><strong>&asymp; ".number_format(floor($sql['cd_visitors_bots'] / $days))."</strong></span>";
        return $avg;
    }

    function AvgValues3($days){
        $db = new mysqli("localhost", "chaosdidi", "MlqlVxddvkHnSKCFWyf-aFRy7O-5_a76o2Z4ZksqgRzZ5F__1K", "chaosdidi");

        $sql = $db->query("SELECT SUM(cd_visitors) AS cd_visitors FROM analytics_stats WHERE `date` BETWEEN '".(strtotime(date("d.m.Y")) - (86400 * $days))."' AND '".(strtotime(date("d.m.Y")) - 86400)."'")->fetch_assoc();
        $sql2 = $db->query("SELECT SUM(cd_visitors_bots) AS cd_visitors_bots FROM analytics_stats WHERE `date` BETWEEN '".(strtotime(date("d.m.Y")) - (86400 * $days))."' AND '".(strtotime(date("d.m.Y")) - 86400)."'")->fetch_assoc();
        $erg_num = ($sql['cd_visitors'] - $sql2['cd_visitors_bots']);
        $avg = "<span class='text-success'><strong>&asymp; ".number_format(floor($erg_num / $days))."</strong></span>";
        return $avg;
    }

	echo "<center><strong><u>Durchschnitts-Werte (&oslash;)</u></strong></center>";
	echo "<div class='row'>";
		echo "<div class='col-md-3'></div>";
		echo "<div class='col-md-3'><strong><u>Besuche (Gesamt)</u></strong></div>";
		echo "<div class='col-md-3'><strong><u>Bots & Spiders</u></strong></div>";
		echo "<div class='col-md-3'><strong><u>Nur Besucher</u></strong></div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>täglich</div>";
		echo "<div class='col-md-3'>".AvgValues1($sql)."</div>";
		echo "<div class='col-md-3'>".AvgValues2($sql)."</div>";
		echo "<div class='col-md-3'>".AvgValues3($sql)."</div>";
	echo "</div>";
	echo "<br>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>gestern</div>";
		echo "<div class='col-md-3'>".AvgValues1(1)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(1)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(1)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 7 Tage</div>";
		echo "<div class='col-md-3'>".AvgValues1(7)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 2 Wochen</div>";
		echo "<div class='col-md-3'>".AvgValues1(7*2)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7*2)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7*2)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 4 Wochen</div>";
		echo "<div class='col-md-3'>".AvgValues1(7*4)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7*4)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7*4)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 3 Monate</div>";
		echo "<div class='col-md-3'>".AvgValues1(7*4*3)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7*4*3)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7*4*3)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 6 Monate</div>";
		echo "<div class='col-md-3'>".AvgValues1(7*4*6)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7*4*6)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7*4*6)."</div>";
	echo "</div>";
	echo "<div class='row'>";
		echo "<div class='col-md-3 text-right'>letzten 12 Monate</div>";
		echo "<div class='col-md-3'>".AvgValues1(7*4*12)."</div>";
		echo "<div class='col-md-3'>".AvgValues2(7*4*12)."</div>";
		echo "<div class='col-md-3'>".AvgValues3(7*4*12)."</div>";
	echo "</div>";
?>