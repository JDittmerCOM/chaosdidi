<?

	require("../../../../../db.php");

	for ($i = (1439589600 + SUMMER_TIME_TIMESTAMP); $i < strtotime(date("d.m.Y")); $i += 86400) { 
		$date_1_sql = $db->query("SELECT * FROM analytics_stats WHERE `date` = '".$i."'");
		$date_1_row = $date_1_sql->fetch_assoc();

		$cd_visitors .= "{ x: new Date(".date("Y", $i).",".(date("m", $i)-1).",".date("d", $i)."), y: ".$date_1_row['cd_visitors']." },";
		$cd_bots .= "{ x: new Date(".date("Y", $i).",".(date("m", $i)-1).",".date("d", $i)."), y: ".$date_1_row['cd_visitors_bots']." },";
		$cd_only_visitors .= "{ x: new Date(".date("Y", $i).",".(date("m", $i)-1).",".date("d", $i)."), y: ".($date_1_row['cd_visitors'] - $date_1_row['cd_visitors_bots'])." },";

	}
	
	//print_r($cd_visitors);

?>


<!DOCTYPE HTML>
<html>

<head>  
	<script type="text/javascript">
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer",
			{

				title:{
					text: "Einmalige Besucher",
					fontSize: 30
				},
				animationEnabled: true,
				axisX:{
					gridColor: "Silver",
					tickColor: "silver",
					valueFormatString: "DD/MM/YY"

				},                        
				toolTip:{
					shared:true
				},
				theme: "theme2",
				axisY: {
					gridColor: "Silver",
					tickColor: "silver"
				},
				legend:{
					verticalAlign: "center",
					horizontalAlign: "right"
				},
				data: [
				{        
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "Besuche (Gesamt)",
					markerType: "",
					color: "#000000",
					dataPoints: [
					<? echo $cd_visitors ?>
					]
				},
				{        
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "Bots & Spiders",
					markerType: "",
					color: "#FF0000",
					dataPoints: [
					<? echo $cd_bots ?>
					]
				},
				{        
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "Nur Besucher",
					markerType: "",
					color: "#00CC00",
					dataPoints: [
					<? echo $cd_only_visitors ?>
					]
				}

				
				],
			  	legend:{
					cursor:"pointer",
					itemclick:function(e){
				  		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
				  		}else{
							e.dataSeries.visible = true;
				  		}
				  		chart.render();
					}
			  	}
			});
			chart.render();
		}
	</script>
	<script type="text/javascript" src="canvasjs.min.js"></script>
</head>
<body>
	<div id="chartContainer" style="height: 300px; width: 100%;"></div>
</body>
</html>
