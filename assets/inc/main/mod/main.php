<?
	$posts_num = $db->query("SELECT * FROM videos")->num_rows;
	$comments_num = $db->query("SELECT * FROM videos_comments")->num_rows;

	$members_registered_num = $db->query("SELECT * FROM users")->num_rows;
	$members_banned_num = $db->query("SELECT * FROM users WHERE status = '0'")->num_rows;
    $members_inactive_num = $db->query("SELECT * FROM users WHERE status = '1'")->num_rows;
	$members_activated_num = $db->query("SELECT * FROM users WHERE status = '2'")->num_rows;

	$bugs_num = $db->query("SELECT * FROM bugs")->num_rows;
	$wish_public_num = $db->query("SELECT * FROM wishbox WHERE status = '1'")->num_rows;
	$wish_private_num = $db->query("SELECT * FROM wishbox WHERE status = '0'")->num_rows;

	$wordfilter_ban_num = $db->query("SELECT * FROM wordfilter")->num_rows;
?>

<div class="box">
	<h2>Moderatoren-Bereich</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="3">Alles im Überblick</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                	<td class="text-center" width="8%"><i class="fa fa-thumb-tack fa-fw"></i></td>
                	<td class="text-center" width="8%"><? echo $posts_num ?></td>
                	<td width="84%">Beiträge</td>
                </tr>
                <tr>
                	<td></td>
                	<td></td>
                	<td>&nbsp;</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-users fa-fw"></i></td>
                	<td class="text-center"><? echo $members_registered_num ?></td>
                	<td><a href="Mod/Members/All">Registrierte Mitglieder</a></td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-users fa-fw"></i></td>
                	<td class="text-center"><? echo $members_activated_num ?></td>
                	<td><a href="Mod/Members/Activated">Aktiverte Mitglieder</a></td>
                </tr>
                <tr>
                    <td class="text-center"><i class="fa fa-users fa-fw"></i></td>
                    <td class="text-center"><? echo $members_inactive_num ?></td>
                    <td><a href="Mod/Members/NotUnlocked">Nocht nicht Freigeschaltene Mitglieder</a></td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-user-times fa-fw"></i></td>
                	<td class="text-center"><? echo $members_banned_num ?></td>
                	<td><a href="Mod/Members/Closures">Gesperrte Mitglieder</a></td>
                </tr>
                <tr>
                	<td></td>
                	<td></td>
                	<td>&nbsp;</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-bug fa-fw"></i></td>
                	<td class="text-center"><? echo $bugs_num ?></td>
                	<td>Fehlermeldungen</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-lightbulb-o fa-fw"></i></td>
                	<td class="text-center"><? echo $wish_public_num ?></td>
                	<td>Öffentliche Wünsche</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-lightbulb-o fa-fw"></i></td>
                	<td class="text-center"><? echo $wish_private_num ?></td>
                	<td>Private Wünsche</td>
                </tr>
                <tr>
                	<td></td>
                	<td></td>
                	<td>&nbsp;</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-ban fa-fw"></i></td>
                	<td class="text-center"><? echo $wordfilter_ban_num ?></td>
                	<td>Gesperrte Wörter</td>
                </tr>
            </tbody>
        </table>
	</div>
</div>