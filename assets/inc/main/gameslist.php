<div class="box">
	<h2>Gameslist</h2>
	<div role="tabpanel">
		<!-- Nav tabs -->
  		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#current" aria-controls="current" role="tab" data-toggle="tab"><i class="fa fa-gamepad"></i>&ensp;Aktuell</a></li>
			<li role="presentation"><a href="#planned" aria-controls="planned" role="tab" data-toggle="tab"><i class="fa fa-clock-o"></i>&ensp;Geplant</a></li>
			<li role="presentation"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab"><i class="fa fa-unlock-alt"></i>&ensp;Projekt beendet</a></li>
  		</ul>

  		<!-- Tab panes -->
  		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="current">
				<br>
				<?
					echo "<ul class='list-group'>";
						$current_sql = $db->query("SELECT * FROM gameslist WHERE category = 'Aktuell' ORDER BY name ASC");
						while($row = $current_sql->fetch_assoc()){
							echo "<li class='list-group-item list-group-item-default'>".$row['name']."</li>";
						}
					echo "</ul>";
				?>
			</div>
			<div role="tabpanel" class="tab-pane" id="planned">
				<br>
				<?
					echo "<ul class='list-group'>";
						$planned_sql = $db->query("SELECT * FROM gameslist WHERE category = 'Geplant' ORDER BY name ASC");
						while($row = $planned_sql->fetch_assoc()){
							echo "<li class='list-group-item list-group-item-default'>".$row['name']."</li>";
						}
					echo "</ul>";
				?>
			</div>
			<div role="tabpanel" class="tab-pane" id="completed">
				<br>
				<?
					echo "<ul class='list-group'>";
						$completed_sql = $db->query("SELECT * FROM gameslist WHERE category = 'Abgeschlossen' ORDER BY name ASC");
						while($row = $completed_sql->fetch_assoc()){
							echo "<li class='list-group-item list-group-item-default'>".$row['name']."</li>";
						}
					echo "</ul>";
				?>
			</div>
  		</div>
	</div>
</div>