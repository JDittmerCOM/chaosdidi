<?
	$pnID = $_GET['pnID'];
	$sql = $db->query("SELECT * FROM pn_conversation WHERE pnID = '".$pnID."' ORDER BY id ASC LIMIT 1");
	$row = $sql->fetch_assoc();

	if($sql->num_rows == 0){
		echo bad("Tut uns Leid, aber diese <strong>Konversation</strong> konnte <strong><u>nicht gefunden</u></strong> werden :(");
		goto nonexist;
	}

	if($row['fromID'] != ID && $row['toID'] != ID){
		echo bad("Tut uns Leid, aber diese <strong>Konversation</strong> ist <strong><u>nicht für dich</u></strong> gedacht ;)");
		goto nonexist;
	}

	$message_read_sql = $db->query("SELECT * FROM pn_conversation WHERE pnID = '".$pnID."' AND toID = '".ID."'");
	while($row1 = $message_read_sql->fetch_assoc()){
		if($row1['message_read'] == 0){
			$db->query("UPDATE pn_conversation SET message_read = 1, message_read_date = ".time()." WHERE pnID = '".$pnID."' AND toID = '".ID."'");
		}
	}
?>

<?
	if(isset($_POST['message_sub'])){
		$date = time();
		$ip = IP;
		$fromID = ID;
		if(ID == $row['fromID']){$toID = $row['toID'];}else{$toID = $row['fromID'];}
		$subject = $row['subject'];
		$message = $_POST['message'];
			$message = trim($message);
			$message = nl2br($message);
			$DB_message = $db->real_escape_string($message);

		$original_message = $message;

		// WORDFILTER [start]
	    $wordfilter_sql = $db->query("SELECT * FROM wordfilter");
	    $bad_word = array();
	    $word_replace = array();
	    while ($row = $wordfilter_sql->fetch_assoc()) {
	       $bad_word[] = $row['word'];
	       $word_replace[] = $row['word_replace'];
	    }

	    $matches = array();
	    $matchFound = preg_match_all("/\b(" . implode($bad_word,"|") . ")\b/i", $message, $matches);

	    $users_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
	    $users_row = $users_sql->fetch_assoc();

	    if ($matchFound) {
	        $words = array_unique($matches[0]);
	        foreach($words as $word) {
	            if($users_row['reports'] >= 3){
	                $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

	                $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");
	            }else{
	                $db->query("UPDATE users SET reports = (reports + 1) WHERE username = '".USERNAME."'");

	                $db->query("INSERT INTO report_conversation (
                                                      			 `date`, 
                                                      			 ip, 
                                                      			 userID, 
                                                      			 username, 
                                                      			 bad_word, 
                                                      			 original_message) 
                                    VALUES ('".$date."', 
                                            '".$ip."', 
                                            '".ID."', 
                                            '".USERNAME."', 
                                            '".$word."', 
                                            '".$original_message."')
	                ");

	                $report_to = "report@chaosdidi.de";
	                $report_subject = "[Report] Es wurde eine Konversation gemeldet";
	                $report_message = '
	                <html>
	                    <head>
	                        <title>[Report] Es wurde eine Konversation gemeldet</title>
	                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	                    </head>
	                    <body>
	                        <br>
	                        <div class="container">
	                            Eine <strong>Konversation</strong> wurde heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> vom System gemeldet.
	                            <hr/>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
	                                <div class="col-md-9">'.$ip.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
	                                <div class="col-md-9">'.USERNAME.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Link zur Konversation:</strong></div>
	                                <div class="col-md-9"><a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a></div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Wort:</strong></div>
	                                <div class="col-md-9">'.$word.'</div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-3 text-right"><strong>Original Kommentar:</strong></div>
	                                <div class="col-md-9">'.$original_message.'</div>
	                            </div>
	                        </div>
	                    </body>
	                </html>
	                ';

	                // Always set content-type when sending HTML email
	                $report_headers = "MIME-Version: 1.0" . "\n";
	                $report_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
	                $report_headers .= 'From: ChaosDidi <report@chaosdidi.de>' . "\n";

	                mail($report_to,$report_subject,$report_message,$report_headers);
	            }
	        }
	    }

	    if($users_row['reports'] >= 3){
	        $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

	        $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");

	        $mail_to = "ban@chaosdidi.de";
            $mail_subject = "[User Banned] ".USERNAME." wurde gesperrt";
            $mail_message = '
            <html>
                <head>
                    <title>[User Banned] '.USERNAME.' wurde gesperrt</title>
                    <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                </head>
                <body>
                    <br>
                    <div class="container">
                        Das Mitglied <strong>'.USERNAME.'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
                        <hr/>
                        <div class="row">
                            <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
                            <div class="col-md-9">Dein Account wurde aufgrund zu vieler Meldungen gesperrt.</div>
                        </div>
                    </div>
                </body>
            </html>
            ';

            // Always set content-type when sending HTML email
            $mail_headers = "MIME-Version: 1.0" . "\n";
            $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
            $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

            mail($mail_to,$mail_subject,$mail_message,$mail_headers);
	    }

	    $message = str_ireplace($bad_word, $word_replace, $message);
	    $DB_message = str_ireplace($bad_word, $word_replace, $DB_message);
	    // WORDFILTER [end]

		$db->query("INSERT INTO pn_conversation (pnID,
										   		 `date`, 
										   		 ip, 
										   		 fromID, 
										   		 toID, 
										   		 subject, 
										   		 message) 
						VALUES ('".$pnID."',
								'".$date."', 
								'".$ip."', 
								'".$fromID."', 
								'".$toID."', 
								'".$subject."', 
								'".$DB_message."')
		");

		$db->query("INSERT INTO backup_pn_conversation (pnID,
										   			    `date`, 
										   			    ip, 
										   			    fromID, 
										   			    toID, 
										   			    subject, 
										   			    message) 
						VALUES ('".$pnID."',
								'".$date."', 
								'".$ip."', 
								'".$fromID."', 
								'".$toID."', 
								'".$subject."', 
								'".$DB_message."')
		");

		if(uda::ID($toID, "noti_new_pn") == 1){
			$mail_to = uda::ID($toID, "email");
	        $mail_subject = "[ChaosDidi] Neue Private Nachricht";
	        $mail_message = '
	        <html>
	            <head>
	                <title>[ChaosDidi] Neue Private Nachricht</title>
	                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	            </head>
	            <body>
	                <br>
	                <div class="container">
	                	<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
	                	<br><br>
	                    Du hast heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> eine neue Private Nachricht erhalten.
	                    <hr/>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Von:</strong></div>
	                        <div class="col-md-9">'.uda::ID(ID, "username").'</div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Betreff:</strong></div>
	                        <div class="col-md-9">'.$subject.'</div>
	                    </div>
	                    <div class="row">
	                        <div class="col-md-3 text-right"><strong>Nachricht:</strong></div>
	                        <div class="col-md-9">'.$message.'</div>
	                    </div>
	                    <hr/>
	                    <a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a>
	                	<br><br>
	                </div>
	            </body>
	        </html>
	        ';

	        // Always set content-type when sending HTML email
	        $mail_headers = "MIME-Version: 1.0" . "\n";
	        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
	        $mail_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

	        mail($mail_to,$mail_subject,$mail_message,$mail_headers);
	    }

	    header("Location:".SERVER_NAME."PN/Conversation/".$pnID);
	}
?>

<?
	echo "<strong><u>Thema:</u></strong> ".$row['subject']."<br>";
	echo "<strong><u>Konversation</u></strong> mit <u>".uda::ID($row['fromID'], "username")."</u> und <u>".uda::ID($row['toID'], "username")."</u>";
?>
<hr/>
<form method="post">
	<div class="form-group">
		<label>Nachricht:</label>
		<textarea name="message" class="form-control" required></textarea>
	</div>
	<div class="form-group">
		<button type="submit" name="message_sub" class="btn btn-primary">Absenden</button>
	</div>
</form>
<hr/>
<?
	$pns_sql = $db->query("SELECT * FROM pn_conversation WHERE pnID = '".$pnID."' ORDER BY id DESC");
	while($row = $pns_sql->fetch_assoc()){
		if(uda::ID($row['fromID'], "username") == USERNAME){
			$username = "mir";
			$read_msg = true;
		}else{
			$username = uda::ID($row['fromID'], "username");
			$read_msg = false;
		}

		if($row['message_read'] == 1 && $read_msg == true){
			$read_message = "<span class='text-success' data-toggle='tooltip' data-placement='bottom' title='Gelesen am ".date("d.m.Y", $row['message_read_date'])." um ".date("H:i", $row['message_read_date'])." Uhr'><i class='fa fa-check fa-fw'></i> <strong>gelesen</strong></span>";
		}elseif($row['message_read'] == 1 && $read_msg == false){
			$read_message = "";
		}
		if($row['message_read'] == 0 && $read_msg == true){
			$read_message = "<span class='text-muted'><i class='fa fa-times fa-fw'></i> <strong>noch nicht gelesen</strong></span>";
		}elseif($row['message_read'] == 0 && $read_msg == false){
			$read_message = "";
		}

		echo "<div class='box'>";
			echo "<div class='row'>";
				echo "<div class='col-md-6'><strong>Von <u>".$username."</u></strong></div>";
				echo "<div class='col-md-6 text-right'><strong>".date("d.m.Y H:i:s", $row['date'])." Uhr</strong></div>";
			echo "</div>";
			echo preg_replace("/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/", "<a href='$1$2' target='_blank'>$1$2 <sub>(Neues Fenster)</sub></a>", $row['message']);
			echo "<div class='row'>";
				echo "<div class='col-md-offset-6 col-md-6 text-right'>".$read_message."</div>";
			echo "</div>";
		echo "</div>";
	}
?>
<?
	nonexist:
?>