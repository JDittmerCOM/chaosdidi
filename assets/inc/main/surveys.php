<div class="box">
	<h2>Umfragen</h2>

	<?
		$survey_sql = $db->query("SELECT * FROM surveys GROUP BY questionID ORDER BY id DESC");
		while($row = $survey_sql->fetch_assoc()){
			echo "<div class='box'>";
				echo "<h2><u>".$row['question']."</u></h2>";

				$procent_reviews = $db->query("SELECT SUM(reviews) AS reviews FROM surveys WHERE questionID = '".$row['questionID']."'")->fetch_assoc();
				
				$answerBTN_sql = $db->query("SELECT * FROM surveys_reviews WHERE userID = '".ID."' AND questionID = '".$row['questionID']."'");

				$answer_sql = $db->query("SELECT * FROM surveys WHERE questionID = '".$row['questionID']."' ORDER BY pos ASC");
				while($row = $answer_sql->fetch_assoc()){
					$procent = round((100 / $procent_reviews['reviews']) * $row['reviews'], 0, PHP_ROUND_HALF_EVEN);

					echo "<div class='row'>";
						echo "<div class='col-md-1'>";
							if($answerBTN_sql->num_rows == 0 && ONLINE == '1'){
								echo "<button type='button' name='sbtn_".$row['questionID']."' class='btn btn-success' onclick='surveyBTN(".$row['questionID'].",".$row['id'].",".ID.");'><i class='fa fa-thumbs-up'></i></button>";
							}else{
								echo "<button type='button' class='btn btn-success' disabled><i class='fa fa-thumbs-up'></i></button>";
							}
						echo "</div>";
						echo "<div class='col-md-5'>".$row['answer']."</div>";
						echo "<div class='col-md-6'>";
							echo "<div class='progress'>";
								echo "<div class='progress-bar progress-bar-success progress-bar-striped' style='min-width:5%; width: ".$procent."%'>";
									echo $procent."%";
								echo "</div>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				}

			echo "</div>";
		}
	?>
</div>