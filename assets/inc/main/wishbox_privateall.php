<? if(ADM == '0' || ONLINE != '1'){header("Location:".SERVER_NAME."Wishbox");} ?>

<div class="box">
	<h2><a href="Wishbox">Wunschbox</a> <i class="fa fa-arrow-right fa-fw"></i> Alle privaten Wünsche</h2>

	<hr/>
	<?
		if(ADM != '1'){
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a>";
		}else{
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a><br>";
			echo "<a href='Wishbox/PrivateAll'>Alle aktuell privaten Wünsche einsehen</a>";
		}
	?>
	<hr/>

	<?
		$sql = $db->query("SELECT * FROM wishbox WHERE status = '0' ORDER BY id DESC");
		while($row = $sql->fetch_assoc()){
			echo "<div class='box' id='wish_box_".$row['id']."'>";
				echo "<blockquote>";
					echo $row['wish_message'];
					echo "<footer>".$row['username']."</footer>";
				echo "</blockquote>";

				echo "<div class='text-right'>";
					echo "<button type='button' id='wish_delete' class='btn btn-inverse' onclick='wish_delete(".$row['id'].")'>Löschen</button>";
				echo "</div>";
			echo "</div>";
		}
	?>
</div>