<div class="box">
	<h2>Wunschbox</h2>
	<p>
		Du hast Wünsche an ChaosDidi wie, Vorschläge für ein neues Let's Play oder für ein neues Feature für die Webseite?<br>
		Dann leg los und schreibe deine Wünsche in die Wunschbox!
	</p>
	<hr/>
	<?
		if(ADM != '1'){
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a>";
		}else{
			echo "<a href='Wishbox/PublicAll'>Alle aktuell öffentlichen Wünsche einsehen</a><br>";
			echo "<a href='Wishbox/PrivateAll'>Alle aktuell privaten Wünsche einsehen</a>";
		}
	?>
	<hr/>
	<?
		if($_GET['success'] == '1'){
			echo good("Dein Wunsch wurde erfolgreich abgesendet.");
		}

		if(isset($_POST['sub_wish'])){
			$ip = $_SERVER['REMOTE_ADDR'];
			$date = time();
			if(ONLINE != '1'){$username = "Gast";}else{$username = USERNAME;}
			$status = $_POST['status'];
			$wish_message = $db->real_escape_string($_POST['wish_message']);
			$secret_question = $db->real_escape_string($_POST['secret_question']);

			//CHECK WISH MESSAGE
			$wish_message = trim($wish_message);
			$wish_message = nl2br($wish_message);

			if(strlen($wish_message) < 10){
				echo bad("Dein Wunsch ist zu kurz.<br>Bitte mindestens 10 Zeichen verwenden.");
			}else{
				$check__message = true;
			}


			//CHECK QUESTION
			if(ONLINE != '1'){
				$secret_question = strtolower($secret_question);
				if(similar_text($secret_question, "chaosdidi") != '9'){
					echo bad("Die Sicherheitsfrage ist falsch.");
				}else{
					$check__secret_question = true;
				}
			}else{
				$check__secret_question = true;
			}


			if($check__message == true && $check__secret_question == true){
				$db->query("INSERT INTO wishbox (ip, 
												 `date`, 
												 username, 
												 status, 
												 wish_message) 
								VALUES ('".$ip."',
										'".$date."',
										'".$username."',
										'".$status."',
										'".$wish_message."')");

				$to = "wishbox@chaosdidi.de";
                $subject = "Neuer Wunsch auf ChaosDidi wurde veröffentlicht";
                $message = '
                <html>
                    <head>
                        <title>Neuer Wunsch auf ChaosDidi wurde veröffentlicht</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container">
                            '.$username.' hat auf ChaosDidi.de einen neuen Wunsch veröffentlicht.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername</strong></div>
                                <div class="col-md-9"><strong>Wunsch</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right">'.$username.'</div>
                                <div class="col-md-9">'.$wish_message.'</div>
                            </div>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers .= 'From: ChaosDidi <wishbox@chaosdidi.de>' . "\n";

                mail($to,$subject,$message,$headers);
				header("Location:".SERVER_NAME."Wishbox/S1");
			}
		}

		echo "<form method='post'>";
			echo "<div class='form-group'>";
				echo "<label>Status<span class='required'>*</span></label>";
				echo "<select name='status' class='form-control'>";
					echo "<option value='1'>Öffentlich (Dein Wunsch wird öffentlich zu sehen sein)</option>";
					echo "<option value='0'>Privat (Dein Wunsch wird nur für den Admin zu sehen sein)</option>";
				echo "</select>";
			echo "</div>";
			echo "<div class='form-group'>";
				echo "<label>Dein Wunsch<span class='required'>*</span></label>";
				echo "<textarea name='wish_message' class='form-control' placeholder='Beschreibe deinen Wunsch' required>".$wish_message."</textarea>";
			echo "</div>";
			if(ONLINE != '1'){
				echo "<div class='form-group'>";
					echo "<label>Sicherheitsfrage<span class='required'>*</span></label>";
					echo "<input type='text' name='secret_question' class='form-control' placeholder='Wie lautet der Nutzername vom Let&apos;s Player dieser Webseite?' value='".$secret_question."' required>";
				echo "</div>";
			}
			echo "<button type='submit' name='sub_wish' class='btn btn-primary'>Absenden</button>";
		echo "</form>";
	?>
</div>