<div class="box">
	<h2>Fehler gefunden?</h2>
	<p>
		Du hast einen Fehler auf ChaosDidi.de gefunden?<br>
		Dann melde diesen Fehler und helfe uns damit, ChaosDidi.de zu verbessern :)
	</p>

	<?
		if($_GET['success'] == 1){
			echo good("Deine <strong>Fehlermeldung</strong> wurde <strong><u>abgesendet</u></strong> und wird vom Administrator überprüft.<br>Vielen Dank für deine Mitarbeit :)");
		}

		if(isset($_POST['sub_bug'])){
			$ip = $_SERVER['REMOTE_ADDR'];
			$date = time();
			$username = $db->real_escape_string($_POST['username']);
			$username = $db->real_escape_string($_POST['username']);
			$subject = $db->real_escape_string($_POST['subject']);
			$message = $db->real_escape_string($_POST['message']);
			$secret_question = $db->real_escape_string($_POST['secret_question']);

			//CHECK SUBJECT
			if(strlen($subject) < 5){
				echo bad("Dein <strong>Betreff</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong>mindestens 5 Zeichen</strong> verwenden.");
			}else{
				$check__subject = true;
			}

			if(!preg_match("/^[a-zA-Z0-9\!\"\&\/\(\)\?\*\-\+\. ]+$/", $subject)){
				echo bad("Dein <strong>Betreff</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong>a-z A-Z 0-9 ! &quot; &amp; / ( ) ? * - + .</strong> verwenden.");
			}else{
				$check__subject2 = true;
			}


			//CHECK MESSAGE
			$message = trim($message);
			$message = nl2br($message);

			if(strlen($message) < 10){
				echo bad("Deine <strong>Fehlerbeschreibung</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong>mindestens 10 Zeichen</strong> verwenden.");
			}else{
				$check__message = true;
			}


			//CHECK QUESTION
			if(ONLINE != '1'){
				$secret_question = strtolower($secret_question);
				if(similar_text($secret_question, "chaosdidi") != '9'){
					echo bad("Die <strong>Sicherheitsfrage</strong> ist <strong><u>falsch</u></strong>.");
				}else{
					$check__secret_question = true;
				}
			}else{
				$check__secret_question = true;
			}


			if(
				$check__subject == true && $check__subject2 == true
				&& $check__message == true
				&& $check__secret_question == true){
				$db->query("INSERT INTO bugs (ip, 
											  `date`, 
											  userID, 
											  username, 
											  subject, 
											  message) 
								VALUES ('".$ip."',
										'".$date."',
										'".$userID."',
										'".$username."',
										'".$subject."',
										'".$message."')");


				$to = "bug@chaosdidi.de";
				$mail_subject = "[Bug] Es wurde ein Fehler gemeldet";
				$mail_message = '
				<html>
					<head>
						<title>[Bug] Es wurde ein Fehler gemeldet</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
					</head>
					<body>
						<br>
						<div class="container">
							Eine Person hat so eben einen Fehler auf <strong>ChaosDidi.de</strong> gemeldet.
							<hr/>
							<div class="row">
								<div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
								<div class="col-md-9">'.$ip.'</div>
							</div>
							<div class="row">
								<div class="col-md-3 text-right"><strong>Datum:</strong></div>
								<div class="col-md-9">'.date("d.m.Y - H:i:s", $date).'</div>
							</div>
							<div class="row">
								<div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
								<div class="col-md-9">'.$username.'</div>
							</div>

							<br>

							<div class="row">
								<div class="col-md-3 text-right"><strong>Betreff:</strong></div>
								<div class="col-md-9">'.$subject.'</div>
							</div>
							<div class="row">
								<div class="col-md-3 text-right"><strong>Fehlerbeschreibung:</strong></div>
								<div class="col-md-9">'.$message.'</div>
							</div>
						</div>
					</body>
				</html>
				';

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\n";
				$headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

				mail($to,$mail_subject,$mail_message,$headers);

				header("Location:".SERVER_NAME."Bug/S1");
			}
		}

		echo "<form method='post'>";
			if(ONLINE != '1'){
				echo "<div class='form-group'>";
					echo "<label>Von</label>";
					echo "<input type='text' name='username' class='form-control' value='Gast' readonly required>";
				echo "</div>";
			}else{
				echo "<div class='form-group'>";
					echo "<label>Von</label>";
					echo "<input type='text' name='username' class='form-control' value='".USERNAME."' readonly required>";
					echo "<input type='hidden' name='userID' value='".ID."'>";
				echo "</div>";
			}
			echo "<div class='form-group'>";
				echo "<label>Betreff<span class='required'>*</span></label>";
				echo "<input type='text' name='subject' class='form-control' placeholder='Betreff' value='".$subject."' maxlenght='150' required>";
			echo "</div>";
			echo "<div class='form-group'>";
				echo "<label>Fehler<span class='required'>*</span></label>";
				echo "<textarea name='message' class='form-control' placeholder='Fehlerbeschreibung' required>".$message."</textarea>";
			echo "</div>";
			if(ONLINE != '1'){
				echo "<div class='form-group'>";
					echo "<label>Sicherheitsfrage<span class='required'>*</span></label>";
					echo "<input type='text' name='secret_question' class='form-control' placeholder='Wie lautet der Nutzername vom Let&apos;s Player dieser Webseite?' value='".$secret_question."' required>";
				echo "</div>";
			}
			echo "<button type='submit' name='sub_bug' class='btn btn-primary'>Absenden</button>";
		echo "</form>";
	?>
</div>