<?
	if(!$_GET['id']){$id = ID;}else{$id = $_GET['id'];}
	$checkProfile_sql = $db->query("SELECT * FROM users WHERE id = '".$id."'");
	$checkProfile_row = $checkProfile_sql->fetch_assoc();
	
	if($checkProfile_sql->num_rows == 0){
		echo "<div class='box'>";
			echo bad("Tut uns Leid, aber dieses Profil existiert nicht :(");
		echo "</div>";
		goto nonexist;
	}

	if($checkProfile_row['status'] == 0){
		echo "<div class='box'>";
			echo bad("Profilseite von: <strong>".$checkProfile_row['username']."</strong><br>Tut uns Leid, aber dieses Profil wurde gesperrt :(");
		echo "</div>";
		goto nonexist;
	}
?>

<div class="box">
	
	<!-- TEAM MEMBER - START -->   
		<div class="team-member single">
			<div class="row">
				<?
					$users_sql = $db->query("SELECT * FROM users WHERE id = '".$id."'");
					$users_row = $users_sql->fetch_assoc();

					if(strlen($users_row['link_facebook']) >= 7){
						$link_facebook = "<li><a href='".$users_row['link_facebook']."' target='_blank' class='brands-facebook'><i class='fa fa-facebook'></i></a></li>";
					}
					if(strlen($users_row['link_twitter']) >= 7){
						$link_twitter = "<li><a href='".$users_row['link_twitter']."' target='_blank' class='brands-twitter'><i class='fa fa-twitter'></i></a></li>";
					}
					if(strlen($users_row['link_youtube']) >= 7){
						$link_youtube = "<li><a href='".$users_row['link_youtube']."' target='_blank' class='brands-youtube'><i class='fa fa-youtube'></i></a></li>";
					}
					if(strlen($users_row['link_gplus']) >= 7){
						$link_gplus = "<li><a href='".$users_row['link_gplus']."' target='_blank' class='brands-google-plus'><i class='fa fa-google-plus'></i></a></li>";
					}
					if(strlen($users_row['link_steam']) >= 7){
						$link_steam = "<li><a href='".$users_row['link_steam']."' target='_blank' class='brands-steam'><i class='fa fa-steam'></i></a></li>";
					}

					echo "<div class='col-sm-4 col-md-3'>";
						$AgetHeaders = @get_headers("https://images.chaosdidi.de/avatars/".$users_row['avatar']);
						if (preg_match("|200|", $AgetHeaders[0])) {
							echo "<img src='//images.chaosdidi.de/avatars/".$users_row['avatar']."' class='img-responsive user-profile-borderradius center-block' alt='".$users_row['username']."'>";
						} else {
							echo "<img src='//images.chaosdidi.de/avatars/default.png' class='img-responsive user-profile-borderradius center-block' alt='".$users_row['username']."'>";
						}

						echo "<ul class='brands brands-tn brands-circle brands-colored brands-inline text-center'>";
							echo $link_facebook;
							echo $link_twitter;
							echo $link_youtube;
							echo $link_gplus;
							echo $link_steam;
						echo "</ul>";
					echo "</div>";
					echo "<div class='col-sm-8 col-md-9'>";
						if(!$_GET['id'] || $_GET['id'] == ID){
							//
						}else{
							echo "<div class='pull-right'>";
								echo "<button class='btn btn-danger btn-sm' data-toggle='modal' data-target='#reportUser' title='Melden'><i class='fa fa-flag fa-fw'></i></button>";
								echo "&ensp;";
								echo "<button class='btn btn-warning btn-sm' data-toggle='modal' data-target='#startConversation' title='Konversation beginnen'><i class='fa fa-comments fa-fw'></i></button>";
							echo "</div>";


							// REPORT USER [start]
							if(isset($_POST['report_sub'])){
								$date = time();
								$ip = IP;
								$report_userID = $id;
								$report_from_userID = ID;
								$reason = $db->real_escape_string($_POST['reason']);
								$reason2 = $_POST['reason2'];
									$reason2 = trim($reason2);
									$reason2 = nl2br($reason2);
									$DB_reason2 = $db->real_escape_string($reason2);

								if(strlen($reason2) == 0){
									$reason2 = "--";
									$DB_reason2 = "--";
								}else{
									$reason2 = $reason2;
									$DB_reason2 = $DB_reason2;
								}

								$db->query("INSERT INTO report_user (`date`, 
																	 ip, 
																	 report_userID, 
																	 report_from_userID, 
																	 reason,
																	 reason2) 
												VALUES ('".$date."', 
														'".$ip."', 
														'".$report_userID."', 
														'".$report_from_userID."', 
														'".$reason."',
														'".$DB_reason2."')
								");


								// EMAIL AN CHAOSDIDI.DE [start]
								$to = "report@chaosdidi.de";
				                $subject = "[Report] Es wurde ein Mitglied gemeldet";
				                $message = '
				                <html>
				                    <head>
				                        <title>[Report] Es wurde ein Mitglied gemeldet</title>
				                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
				                    </head>
				                    <body>
				                        <br>
				                        <div class="container">
				                            Ein <strong>Mitglied</strong> wurde heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> von <u>'.USERNAME.'</u> gemeldet.
				                            <hr/>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
				                                <div class="col-md-9">'.$ip.'</div>
				                            </div>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Gemeldet von:</strong></div>
				                                <div class="col-md-9"><a href="http://chaosdidi.de/MyProfile/'.ID.'">'.USERNAME.'</a></div>
				                            </div>
				                            <br>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Gemeldetes Mitglied:</strong></div>
				                                <div class="col-md-9"><a href="http://chaosdidi.de/MyProfile/'.$report_userID.'">'.$users_row['username'].'</a></div>
				                            </div>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Grund:</strong></div>
				                                <div class="col-md-9">'.$reason.'</div>
				                            </div>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Zusatzinformation:</strong></div>
				                                <div class="col-md-9">'.$reason2.'</div>
				                            </div>
				                        </div>
				                    </body>
				                </html>
				                ';

				                // Always set content-type when sending HTML email
				                $headers = "MIME-Version: 1.0" . "\n";
				                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
				                $headers .= 'From: ChaosDidi <report@chaosdidi.de>' . "\n";

				                mail($to,$subject,$message,$headers);
								// EMAIL AN CHAOSDIDI.DE [end]


								// EMAIL AN MITGLIED [start]
								$to = EMAIL;
				                $subject = "[ChaosDidi] Deine Meldung";
				                $message = '
				                <html>
				                    <head>
				                        <title>[ChaosDidi] Deine Meldung</title>
				                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
				                    </head>
				                    <body>
				                        <br>
				                        <div class="container">
				                            Du hast ein <strong>Mitglied</strong> heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> gemeldet.
				                            <hr/>
				                            Vielen Dank für deine Meldung! Wir werden das ganze schnellstmöglich überprüfen.
				                            <br><br>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Gemeldetes Mitglied:</strong></div>
				                                <div class="col-md-9"><a href="http://chaosdidi.de/MyProfile/'.$report_userID.'">'.$users_row['username'].'</a></div>
				                            </div>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Grund:</strong></div>
				                                <div class="col-md-9">'.$reason.'</div>
				                            </div>
				                            <div class="row">
				                                <div class="col-md-3 text-right"><strong>Zusatzinformation:</strong></div>
				                                <div class="col-md-9">'.$reason2.'</div>
				                            </div>
				                        </div>
				                    </body>
				                </html>
				                ';

				                // Always set content-type when sending HTML email
				                $headers = "MIME-Version: 1.0" . "\n";
				                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
				                $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

				                mail($to,$subject,$message,$headers);
								// EMAIL AN MITGLIED [end]
							}


							echo "<div class='modal fade' id='reportUser' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
								echo "<div class='modal-dialog' role='document'>";
							    	echo "<div class='modal-content'>";
							      		echo "<div class='modal-header'>";
							        		echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
							        		echo "<h4 class='modal-title' id='myModalLabel'>Mitglied melden</h4>";
							      		echo "</div>";
							        	echo "<form method='post'>";
								      		echo "<div class='modal-body'>";
								        		echo "Bitte wähle einen der folgenden Kategorien aus, um diesen Benutzer zu melden.<br>";
								        			echo "<div class='form-group'>";
								        				echo "<select name='reason' class='form-control' required>";
								        					echo "<option value='Pornografische oder geschmacklose Fotos'>Pornografische oder geschmacklose Fotos</option>";
								        					echo "<option value='Beleidigung oder Belästigung'>Beleidigung oder Belästigung</option>";
								        					echo "<option value='Geklaute Fotos'>Geklaute Fotos</option>";
								        					echo "<option value='Fake-Profil'>Fake-Profil</option>";
								        					echo "<option value='Werbeinhalt oder Betrugsabsicht'>Werbeinhalt oder Betrugsabsicht</option>";
								        				echo "</select>";
								        			echo "</div>";
								        			echo "<div class='form-group'>";
								        				echo "<textarea name='reason2' class='form-control' placeholder='Hinterlasse gerne weitere Informationen für uns, damit wir deine Meldung schneller nachvollziehen und bearbeiten können.'></textarea>";
								        			echo "</div>";
								        		echo info("Falls diese Meldung fehlerhaft sein sollte, kann es dazuführen das wir deinen Account sperren.");
								      		echo "</div>";
								      		echo "<div class='modal-footer'>";
								        		echo "<button type='button' class='btn btn-inverse' data-dismiss='modal'>Schließen</button>";
								        		echo "<button name='report_sub' type='submit' class='btn btn-primary'>Absenden</button>";
								      		echo "</div>";
							        	echo "</form>";
							    	echo "</div>";
							  	echo "</div>";
							echo "</div>";
							// REPORT USER [end]


							// START CONVERSATION [start]
							if(isset($_POST['message_sub'])){
								$pnID = hash("adler32", time().rand(100,999));
								$date = time();
								$ip = IP;
								$fromID = ID;
								$toID = $users_row['id'];
								$subject = $db->real_escape_string($_POST['subject']);
								$message = $_POST['message'];
									$message = trim($message);
									$message = nl2br($message);
									$DB_message = $db->real_escape_string($message);

								$original_message = $message;

								// WORDFILTER [start]
							    $wordfilter_sql = $db->query("SELECT * FROM wordfilter");
							    $bad_word = array();
							    $word_replace = array();
							    while ($row = $wordfilter_sql->fetch_assoc()) {
							       $bad_word[] = $row['word'];
							       $word_replace[] = $row['word_replace'];
							    }

							    $matches = array();
							    $matchFound = preg_match_all("/\b(" . implode($bad_word,"|") . ")\b/i", $message, $matches);

							    $users_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
							    $users_row = $users_sql->fetch_assoc();

							    if ($matchFound) {
							        $words = array_unique($matches[0]);
							        foreach($words as $word) {
							            if($users_row['reports'] >= 3){
							                $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

							                $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");

							                $mail_to = "ban@chaosdidi.de";
							                $mail_subject = "[User Banned] ".USERNAME." wurde gesperrt";
							                $mail_message = '
							                <html>
							                    <head>
							                        <title>[User Banned] '.USERNAME.' wurde gesperrt</title>
							                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
							                    </head>
							                    <body>
							                        <br>
							                        <div class="container">
							                            Das Mitglied <strong>'.USERNAME.'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
							                            <hr/>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
							                                <div class="col-md-9">Dein Account wurde aufgrund zu vieler Meldungen gesperrt.</div>
							                            </div>
							                        </div>
							                    </body>
							                </html>
							                ';

							                // Always set content-type when sending HTML email
							                $mail_headers = "MIME-Version: 1.0" . "\n";
							                $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
							                $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

							                mail($mail_to,$mail_subject,$mail_message,$mail_headers);
							            }else{
							                $db->query("UPDATE users SET reports = (reports + 1) WHERE username = '".USERNAME."'");

							                $db->query("INSERT INTO report_conversation (`date`, 
					                                                          			 ip, 
					                                                          			 userID, 
					                                                          			 username, 
					                                                          			 bad_word, 
					                                                          			 original_message) 
					                                        VALUES ('".$date."', 
					                                                '".$ip."', 
					                                                '".ID."', 
					                                                '".USERNAME."', 
					                                                '".$word."', 
					                                                '".$original_message."')
							                ");

							                $report_to = "report@chaosdidi.de";
							                $report_subject = "[Report] Es wurde eine Konversation gemeldet";
							                $report_message = '
							                <html>
							                    <head>
							                        <title>[Report] Es wurde eine Konversation gemeldet</title>
							                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
							                    </head>
							                    <body>
							                        <br>
							                        <div class="container">
							                            Eine <strong>Konversation</strong> wurde heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> vom System gemeldet.
							                            <hr/>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>IP-Adresse:</strong></div>
							                                <div class="col-md-9">'.$ip.'</div>
							                            </div>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
							                                <div class="col-md-9">'.USERNAME.'</div>
							                            </div>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>Link zur Konversation:</strong></div>
							                                <div class="col-md-9"><a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a></div>
							                            </div>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>Wort:</strong></div>
							                                <div class="col-md-9">'.$word.'</div>
							                            </div>
							                            <div class="row">
							                                <div class="col-md-3 text-right"><strong>Original Kommentar:</strong></div>
							                                <div class="col-md-9">'.$original_message.'</div>
							                            </div>
							                        </div>
							                    </body>
							                </html>
							                ';

							                // Always set content-type when sending HTML email
							                $report_headers = "MIME-Version: 1.0" . "\n";
							                $report_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
							                $report_headers .= 'From: ChaosDidi <report@chaosdidi.de>' . "\n";

							                mail($report_to,$report_subject,$report_message,$report_headers);
							            }
							        }
							    }

							    $users_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
							    $users_row = $users_sql->fetch_assoc();

							    if($users_row['reports'] >= 3){
							        $db->query("INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".$date."', '".ID."', 'Dein Account wurde aufgrund zu vieler Meldungen gesperrt.', 'System')");

							        $db->query("UPDATE users SET status = '0' WHERE username = '".USERNAME."'");

							        $mail_to = "ban@chaosdidi.de";
						            $mail_subject = "[User Banned] ".USERNAME." wurde gesperrt";
						            $mail_message = '
						            <html>
						                <head>
						                    <title>[User Banned] '.USERNAME.' wurde gesperrt</title>
						                    <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
						                </head>
						                <body>
						                    <br>
						                    <div class="container">
						                        Das Mitglied <strong>'.USERNAME.'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
						                        <hr/>
						                        <div class="row">
						                            <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
						                            <div class="col-md-9">Dein Account wurde aufgrund zu vieler Meldungen gesperrt.</div>
						                        </div>
						                    </div>
						                </body>
						            </html>
						            ';

						            // Always set content-type when sending HTML email
						            $mail_headers = "MIME-Version: 1.0" . "\n";
						            $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
						            $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

						            mail($mail_to,$mail_subject,$mail_message,$mail_headers);
							    }

							    $message = str_ireplace($bad_word, $word_replace, $message);
							    $DB_message = str_ireplace($bad_word, $word_replace, $DB_message);
							    // WORDFILTER [end]

								$db->query("INSERT INTO pn_conversation (pnID,
																   		 `date`, 
																   		 ip, 
																   		 fromID, 
																   		 toID, 
																   		 subject, 
																   		 message) 
												VALUES ('".$pnID."',
														'".$date."', 
														'".$ip."', 
														'".$fromID."', 
														'".$toID."', 
														'".$subject."', 
														'".$DB_message."')
								");

								$db->query("INSERT INTO backup_pn_conversation (pnID,
																   			    `date`, 
																   			    ip, 
																   			    fromID, 
																   			    toID, 
																   			    subject, 
																   			    message) 
												VALUES ('".$pnID."',
														'".$date."', 
														'".$ip."', 
														'".$fromID."', 
														'".$toID."', 
														'".$subject."', 
														'".$DB_message."')
								");

								if(uda::ID($toID, "noti_new_pn") == 1){
									$mail_to = uda::ID($toID, "email");
							        $mail_subject = "[ChaosDidi] Neue Private Nachricht";
							        $mail_message = '
							        <html>
							            <head>
							                <title>[ChaosDidi] Neue Private Nachricht</title>
							                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
							            </head>
							            <body>
							                <br>
							                <div class="container">
							                	<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
							                	<br><br>
							                    Du hast heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> eine neue Private Nachricht erhalten.
							                    <hr/>
							                    <div class="row">
							                        <div class="col-md-3 text-right"><strong>Von:</strong></div>
							                        <div class="col-md-9">'.uda::ID(ID, "username").'</div>
							                    </div>
							                    <div class="row">
							                        <div class="col-md-3 text-right"><strong>Thema:</strong></div>
							                        <div class="col-md-9">'.$subject.'</div>
							                    </div>
							                    <div class="row">
							                        <div class="col-md-3 text-right"><strong>Nachricht:</strong></div>
							                        <div class="col-md-9">'.$message.'</div>
							                    </div>
							                    <hr/>
							                    <a href="http://chaosdidi.de/PN/Conversation/'.$pnID.'">http://chaosdidi.de/PN/Conversation/'.$pnID.'</a>
							                	<br><br>
							                </div>
							            </body>
							        </html>
							        ';

							        // Always set content-type when sending HTML email
							        $mail_headers = "MIME-Version: 1.0" . "\n";
							        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
							        $mail_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

							        mail($mail_to,$mail_subject,$mail_message,$mail_headers);

							        header("Location: ".SERVER_NAME."PN/Conversation/".$pnID);
							    }
							}

							echo "<div class='modal fade' id='startConversation' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
								echo "<div class='modal-dialog' role='document'>";
							    	echo "<div class='modal-content'>";
							      		echo "<div class='modal-header'>";
							        		echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
							        		echo "<h4 class='modal-title' id='myModalLabel'>Konversation mit <strong>".$users_row['username']."</strong> starten</h4>";
							      		echo "</div>";
							        	echo "<form method='post'>";
								      		echo "<div class='modal-body'>";
												echo "<div class='form-group'>";
													echo "<label>An:</label>";
													echo "<input type='text' name='username' id='username' class='form-control' autocomplete='off' value='".$users_row['username']."' disabled required>";
												echo "</div>";
												echo "<div class='form-group'>";
													echo "<label>Thema:</label>";
													echo "<input type='text' name='subject' class='form-control' placeholder='Thema' required>";
												echo "</div>";
												echo "<div class='form-group'>";
													echo "<label>Nachricht:</label>";
													echo "<textarea name='message' class='form-control' placeholder='Deine Nachricht' required></textarea>";
												echo "</div>";
								      		echo "</div>";
								      		echo "<div class='modal-footer'>";
								        		echo "<button type='button' class='btn btn-inverse' data-dismiss='modal'>Schließen</button>";
												echo "<button type='submit' name='message_sub' class='btn btn-primary'>Absenden</button>";
								      		echo "</div>";
							        	echo "</form>";
							    	echo "</div>";
							  	echo "</div>";
							echo "</div>";
							// START CONVERSATION [end]
						}
						echo "<h2>Benutzername<small>".$users_row['username']."</small></h2>";
						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Mitglied seit:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".date("d.m.Y, H:i", $users_row['date'])." Uhr</div>";
						echo "</div>";
						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Letzte Aktivität am:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".date("d.m.Y, H:i", $users_row['last_activity'])." Uhr</div>";
						echo "</div>";

						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Rang:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".points_to_level($users_row['credits_total'], "level")."</div>";
						echo "</div>";
						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Punkte:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".$users_row['credits_total']."</div>";
						echo "</div>";

						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Über mich:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".$users_row['about_me']."</div>";
						echo "</div>";

						echo "<br>";
						if($users_row['online'] == 0 || (time() - $users_row['last_activity']) >= 1440){$user_online = "<span class='text-danger'>OFFLINE</span>";}
						if($users_row['online'] == 1 && (time() - $users_row['last_activity']) >= 300 && (time() - $users_row['last_activity']) < 1440){$user_online = "<span class='text-warning'>INAKTIV</span>";}
						if($users_row['online'] == 1 && (time() - $users_row['last_activity']) < 300){$user_online = "<span class='text-success'>ONLINE</span>";}
						echo "<div class='row'>";
							echo "<div class='col-xs-5 col-md-4 text-right'><strong>Online Status:</strong></div>";
							echo "<div class='col-xs-7 col-md-8'>".$user_online."</div>";
						echo "</div>";
					echo "</div>";
				?>
			</div>
		</div>
	<!-- TEAM MEMBER - END --> 
</div>

<div class="box profile-gallery">
	<?
		$awards_sql = $db->query("SELECT * FROM users_awards WHERE userID = '".$id."'");
		$awards_row = $awards_sql->num_rows;
	?>
	<h2>Auszeichnungen (<? echo $awards_row ?>/29)</h2>
	<div class="gallery-page">
		<div class="row">
			<?
				$awards_list = array(
					"0-days" 			=> "Trete der Community bei.", 
					"1-day" 			=> "Sei ein Tag in der Community registriert.", 
					"1-week" 			=> "Sei eine Woche in der Community registriert.",
					"1-month" 			=> "Sei ein Monat in der Community registriert.",
					"6-months" 			=> "Sei sechs Monate in der Community registriert.",
					"1-year" 			=> "Sei ein Jahr in der Community registriert.",
					"2-years" 			=> "Sei zwei Jahre in der Community registriert.",
					"3-years" 			=> "Sei drei Jahre in der Community registriert.",
					"5-years" 			=> "Sei fünf Jahre in der Community registriert.",
					"10-badges" 		=> "Verdiene 10 Abzeichen.",
					"10-balance" 		=> "Sammle 10 Punkte (Gesamt).",
					"50-balance" 		=> "Sammle 50 Punkte (Gesamt).",
					"100-balance" 		=> "Sammle 100 Punkte (Gesamt).",
					"500-balance" 		=> "Sammle 500 Punkte (Gesamt).",
					"1k-balance" 		=> "Sammle 1.000 Punkte (Gesamt).",
					"5k-balance" 		=> "Sammle 5.000 Punkte (Gesamt).",
					"10k-balance" 		=> "Sammle 10.000 Punkte (Gesamt).",
					"50k-balance" 		=> "Sammle 50.000 Punkte (Gesamt).",
					"100k-balance"		=> "Sammle 100.000 Punkte (Gesamt).",
					"250k-balance"		=> "Sammle 250.000 Punkte (Gesamt).",
					"1-good-idea" 		=> "Erhalte für Wünsche von dir 1 Daumen nach oben.",
					"5-good-idea" 		=> "Erhalte für Wünsche von dir 5 Daumen nach oben.",
					"10-good-idea" 		=> "Erhalte für Wünsche von dir 10 Daumen nach oben.",
					"50-good-idea" 		=> "Erhalte für Wünsche von dir 50 Daumen nach oben.",
					"100-good-idea" 	=> "Erhalte für Wünsche von dir 100 Daumen nach oben.",
					"500-good-idea" 	=> "Erhalte für Wünsche von dir 500 Daumen nach oben.",
					"1k-good-idea" 		=> "Erhalte für Wünsche von dir 1.000 Daumen nach oben.",
					"10k-good-idea" 	=> "Erhalte für Wünsche von dir 10.000 Daumen nach oben.",
					"100k-good-idea" 	=> "Erhalte für Wünsche von dir 100.000 Daumen nach oben."
				); 

				while (list($award_name, $award_title) = each($awards_list)) {
					$users_awards_sql = $db->query("SELECT * FROM users_awards WHERE userID = ".$id." AND award_name = '".$award_name."'")->num_rows;
				    if($users_awards_sql == 1){
				    	echo "<div class='col-xs-4 col-md-2'>";
				    		echo "<img src='//images.chaosdidi.de/awards/".$award_name.".png' class='img-responsive' alt='".$award_title."' data-toggle='tooltip' data-placement='top' title='".$award_title."'>";
				    	echo "</div>";
				    }else{
				    	echo "<div class='col-xs-4 col-md-2'>";
				    		echo "<img src='//images.chaosdidi.de/awards/".$award_name.".png' class='award-disabled img-responsive' alt='".$award_title."' data-toggle='tooltip' data-placement='top' title='".$award_title."'>";
				    	echo "</div>";
				    }
				}
			?>
		</div>
	</div>
	
</div>

<div class="box profile-gallery">
	<?
		$profileImages_sql = $db->query("SELECT * FROM users_images WHERE userID = '".$id."'");
		$profileImages_row = $profileImages_sql->num_rows;
	?>
	<h2>Profil Gallerie (<? echo $profileImages_row; ?>)</h2>
	<div class="gallery-page">
		<div class="row masonry">
			<?
				if($users_row['secret_profile_imgs'] == 0 || ADM == 1 || $id == ID){
					$profile_gallery_sql = $db->query("SELECT * FROM users_images WHERE userID = '".$id."' ORDER BY id DESC");
					while($row = $profile_gallery_sql->fetch_assoc()){
						if(ADM == '1'){
	                        $delete_image = "<button type='button' class='btn btn-danger btn-block btn-xs' onclick='del_profile_image(".$row['id'].");'>Foto Löschen</button>";
	                    }
						echo "<div id='profile_image_".$row['id']."' class='masonry-item col-md-4 col-xs-4 img-thumbnail'>";
							echo "<a href='//images.chaosdidi.de/avatars/".$row['image_link']."'><img src='//images.chaosdidi.de/avatars/".$row['image_link']."' class='img-responsive' alt='".$row['image_link']."'></a>";
							echo $delete_image;
							echo "<center><strong>Hochgeladen am:</strong> ".date("d.m.Y H:i", $row['date'])."</center>";
						echo "</div>";
					}
				}else{
					echo bad("Dieses Mitglied hält seine Profilbilder Geheim.");
				}
			?>
		</div>
	</div>
</div>
<?
	nonexist:
?>