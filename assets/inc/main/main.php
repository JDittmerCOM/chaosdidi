<link href="assets/js/pagination/pagination.css" rel="stylesheet" type="text/css"></link>
<div class="box">

    <!-- POST - START -->
    <div id="hiddenresult" style="display:none;">
        <?
            $post_sql = $db->query("SELECT * FROM videos WHERE planned_status = '0' ORDER BY published DESC LIMIT 100");
            while($row = $post_sql->fetch_assoc()){
                
                if(strlen($row['description']) >= 300){
                    $description = htmlspecialchars(str_replace("<br>", "\n", substr($row['description'], 0, 300)." ..."), ENT_QUOTES);
                }else{
                    $description = htmlspecialchars(str_replace("<br>", "\n", $row['description']), ENT_QUOTES);
                }

                if(!file("http://images.chaosdidi.de/uploads/".$row['image_link'])){
                    $image_link = "no-cover.jpg";
                }else{
                    if(isMobile()){
                        $resize_mobi = explode(".", $row['image_link']);
                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
                        $image_link = $resize_mobi;
                    }else{
                        $resize_mobi = explode(".", $row['image_link']);
                        $resize_mobi = $resize_mobi[0]."-MOBI.".$resize_mobi[1];
                        $image_link = $resize_mobi;
                        //$image_link = $row['image_link'];
                    }
                }

                echo "<article class='post result'>";
                    echo "<div class='post-date-wrapper'>";
                        echo "<div class='post-date'>";
                            echo "<div class='day'>".date("d", $row['published'])."</div>";
                            echo "<div class='month'>".date("M Y", $row['published'])."</div>";
                        echo "</div>";
                        echo "<div class='post-type'>";
                            echo "<i class='fa fa-video-camera'></i>";
                        echo "</div>";
                    echo "</div>";
                    echo "<div class='post-body'>";
                        echo "<h2>".htmlspecialchars($row['title'], ENT_QUOTES)."</h2>";
                        echo "<p><img src='//images.chaosdidi.de/uploads/".$image_link."' class='img-responsive' alt='Thumbnail'>".$description."</p>";
                        echo "<div class='post-info'>";
                            echo "<span>Gepostet von: ".$row['author']."</span>";
                            echo "<a href='Post/".$row['id']."' class='btn btn-inverse'>Zeige mehr</a>";
                        echo "</div>";
                    echo "</div>";
                echo "</article>";
            }
        ?>
    </div>
    <!-- POST - END -->

    <div id="Pagination"></div>
    <br style="clear:both;" />
    <div id="Searchresult">
        <div class="alert alert-warning">
            <i class="fa fa-spinner fa-pulse fa-fw"></i>
            Einen Augenblick bitte, wir laden gerade die letzten 100 Folgen für dich :)
        </div>
    </div>

</div>