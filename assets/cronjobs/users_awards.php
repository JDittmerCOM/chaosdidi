<?
	if(strlen(ID) != '0'){
		//REGISTERED DAYS [start]
		$registered_sql = $db->query("SELECT * FROM users WHERE id = '".ID."'");
		$registered_row = $registered_sql->fetch_assoc();

		function seDay($begin,$end,$format,$sep){
			$pos1 = strpos($format, 'd');
			$pos2 = strpos($format, 'm');
			$pos3 = strpos($format, 'Y');

			$begin = explode($sep,$begin);
			$end = explode($sep,$end);

			$first = GregorianToJD($end[$pos2],$end[$pos1],$end[$pos3]);
			$second = GregorianToJD($begin[$pos2],$begin[$pos1],$begin[$pos3]);

			if($first > $second)
			  	return $first - $second;
			else
			  	return $second - $first;
		}

		$member_since = seDay(date("d.m.Y", $registered_row['date']), date("d.m.Y"), "dmY", ".");

		if($member_since >= '0'){
			$uan = ID."_0-days";
			$award_name = "0-days";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1'){
			$uan = ID."_1-day";
			$award_name = "1-day";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '7'){
			$uan = ID."_1-week";
			$award_name = "1-week";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '30'){
			$uan = ID."_1-month";
			$award_name = "1-month";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '182'){
			$uan = ID."_6-months";
			$award_name = "6-months";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '365'){
			$uan = ID."_1-year";
			$award_name = "1-year";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '730'){
			$uan = ID."_2-years";
			$award_name = "2-years";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1095'){
			$uan = ID."_3-years";
			$award_name = "3-years";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1825'){
			$uan = ID."_5-years";
			$award_name = "5-years";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//REGISTERED DAYS [end]



		//10 AWARDS [start]
		$awards = $db->query("SELECT * FROM users_awards WHERE userID = '".ID."'")->num_rows;

		if($awards >= 10){
			$uan = ID."_10-badges";
			$award_name = "10-badges";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//10 AWARDS [end]



		//BALANCE [start]
		$balance = $db->query("SELECT * FROM users WHERE id = '".ID."'");
		$balance_row = $balance->fetch_assoc();

		if($balance_row['credits_total'] >= 10){
			$uan = ID."_10-balance";
			$award_name = "10-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 50){
			$uan = ID."_50-balance";
			$award_name = "50-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 100){
			$uan = ID."_100-balance";
			$award_name = "100-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 500){
			$uan = ID."_500-balance";
			$award_name = "500-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 1000){
			$uan = ID."_1k-balance";
			$award_name = "1k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 5000){
			$uan = ID."_5k-balance";
			$award_name = "5k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 10000){
			$uan = ID."_10k-balance";
			$award_name = "10k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 50000){
			$uan = ID."_50k-balance";
			$award_name = "50k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 100000){
			$uan = ID."_100k-balance";
			$award_name = "100k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 250000){
			$uan = ID."_250k-balance";
			$award_name = "250k-balance";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//BALANCE [end]



		//GOOD IDEA [start]
		$good_idea = $db->query("SELECT SUM(good) AS good FROM wishbox WHERE username = '".USERNAME."'")->fetch_assoc();
		
		if($good_idea['good'] >= 1){
			$uan = ID."_1-good-idea";
			$award_name = "1-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 5){
			$uan = ID."_5-good-idea";
			$award_name = "5-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 10){
			$uan = ID."_10-good-idea";
			$award_name = "10-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 50){
			$uan = ID."_50-good-idea";
			$award_name = "50-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 100){
			$uan = ID."_100-good-idea";
			$award_name = "100-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 500){
			$uan = ID."_500-good-idea";
			$award_name = "500-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 1000){
			$uan = ID."_1k-good-idea";
			$award_name = "1k-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 10000){
			$uan = ID."_10k-good-idea";
			$award_name = "10k-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 100000){
			$uan = ID."_100k-good-idea";
			$award_name = "100k-good-idea";
			$db->query("INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//GOOD IDEA [end]
	}
?>