<? require("db.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <base href="//chaosdidi.de"></base>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
    <meta name="description" content="Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen. Meist PS3 Spiele, aber auf Wunsch auch PC.">
    <? echo "<meta name='keywords' content='ChaosDidi, YouTube, Facebook, Twitter, Videos, PS3, PS4, PC, Stimmen, Spiele, ".META_TAGS."Games'>"; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>ChaosDidi | Let's Plays - Verlosung</title>
        
    <!-- ==========================
    	Fonts 
    =========================== -->
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="assets/css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.vegas.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
    	JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
	
    <!-- HEADER - START  -->
	<? require("assets/inc/topbar.php"); ?>
    <!-- HEADER - END  -->  
    

    <!-- TITLE - START  -->
    <div class="container hidden-xs">
    	<div class="header-title">
        	<div class="pull-left">
        		<h2><a href="./"><span class="text-primary">Chaos</span>Didi</a></h2>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <!-- TITLE - END  -->
    

    <!-- JUMBOTRON - START  -->
    <div class="container">
    	<div class="jumbotron jumbotron_social">
        	<div class="jumbotron-panel jumbotron_social">
            	<div class="panel panel-primary collapse-horizontal">
                    <div class="jumbotron-brands">
                    	<ul class="brands brands-sm brands-inline brands-circle">
                            <? echo "<li><a href='".SC_FACEBOOK."' target='_blank'><i class='fa fa-facebook'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_TWITTER."' target='_blank'><i class='fa fa-twitter'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_YOUTUBE."' target='_blank'><i class='fa fa-youtube'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_GPLUS."' target='_blank'><i class='fa fa-google-plus'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_STEAM."' target='_blank'><i class='fa fa-steam'></i></a></li>"; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- JUMBOTRON - END -->
    

    <!-- CONTENT - START  -->
    <div class="container">
        <section class="content-wrapper">
        	<div class="row">
            
            	<!-- SIDEBAR - START -->
            	<? require("assets/inc/sidebar.php"); ?>
                <!-- SIDEBAR - END -->
                
                <!-- CONTENT BODY - START -->
                <div class="col-sm-8">
                    <?
                        $keys_sql = $db->query("SELECT * FROM raffle_keys WHERE key_status = 1");
                        $keys_row = $keys_sql->fetch_assoc();
                    ?>
                    
                	<div class="box">
                        <h2 class="text-center">Steam Key Verlosung</h2>
                        <div class="box">
                            <div class="row">
                                <div class="col-md-4 col-md-push-8">
                                    <?
                                        $raffle_infos_sql = $db->query("SELECT * FROM raffles_participate");
                                        $raffle_infos_row = $raffle_infos_sql->fetch_assoc();

                                        //AVG GEWINNCHANCE [start]
                                        $avg_SUM_sql = $db->query("SELECT SUM(participates) AS participates FROM raffle_keys WHERE key_status = 0")->fetch_assoc();
                                        $avg_sql = $db->query("SELECT * FROM raffle_keys WHERE key_status = 0");
                                        $avg_win_chance = number_format(100 / ($avg_SUM_sql['participates'] / $avg_sql->num_rows), 1, ',', '');
                                        //AVG GEWINNCHANCE [end]

                                        // AKTUELLE GEWINNCHANCE [start]
                                        if($raffle_infos_sql->num_rows == 0){
                                            $win_chance = "--";
                                        }else{
                                            $win_chance = number_format(100 / $raffle_infos_sql->num_rows, 1, ',', '');
                                        }
                                        // AKTUELLE GEWINNCHANCE [end]


                                        // AKTUELLE TEILNEHMER [start]
                                        $participates = $raffle_infos_sql->num_rows;
                                        // AKTUELLE TEILNEHMER [end]
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-9 col-md-9 text-right">&oslash; Gewinnchance:</div>
                                        <div class="col-xs-3 col-md-3"><? echo $avg_win_chance."%"; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-9 col-md-9 text-right">Aktuelle Gewinnchance:</div>
                                        <div class="col-xs-3 col-md-3"><? echo $win_chance."%"; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-9 col-md-9 text-right">Teilnehmer:</div>
                                        <div class="col-xs-3 col-md-3"><? echo $participates ?></div>
                                    </div>
                                </div>
                                <?
                                    $bs_count = count(explode(", ", $keys_row['bs']));
                                    $bs_explode = explode(", ", $keys_row['bs']);
                                    if($bs_count == 1){
                                        $bs = "<strong>".$bs_explode[0]."</strong>";
                                    }
                                    if($bs_count == 2){
                                        $bs = "<strong>".$bs_explode[0]."</strong> oder <strong>".$bs_explode[1]."</strong>";
                                    }
                                    if($bs_count == 3){
                                        $bs = "<strong>".$bs_explode[0]."</strong>, <strong>".$bs_explode[1]."</strong> oder <strong>".$bs_explode[2]."</strong>";
                                    }
                                    if($bs_count == 4){
                                        $bs = "<strong>".$bs_explode[0]."</strong>, <strong>".$bs_explode[1]."</strong>, <strong>".$bs_explode[2]."</strong> oder <strong>".$bs_explode[3]."</strong>";
                                    }
                                ?>
                                <div class="col-md-8 col-md-pull-4">
                                    <strong><u>Teilnahmebedingungen:</u></strong><br>
                                    Um an diesem Gewinnspiel teilnehmen zu können, musst du:
                                    <ol>
                                        <li><? echo $bs ?> als Betriebssystem haben.</li>
                                        <li><strong>Steam</strong> installiert haben.</li>
                                        <li>auf <strong>ChaosDidi.de</strong> registriert sein.</li>
                                        <li>mindestens <strong><? echo $keys_row['game_usk'] ?> Jahre</strong> alt sein.</li>
                                    </ol>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Zu Gewinnen:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_name'] ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Steam Preis:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_price']." €"; ?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong><u>Details:</u></strong></div>
                                        <div class="col-xs-7 col-md-7"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Genre:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_genre'] ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Entwickler:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_developer'] ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Publisher:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_publisher'] ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Veröffentlichung:</strong></div>
                                        <div class="col-xs-7 col-md-7"><? echo $keys_row['game_publishing'] ?></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-5 col-md-5 text-right"><strong>Weitere Informationen:</strong></div>
                                        <div class="col-xs-7 col-md-7">
                                            <?
                                                if(strlen($keys_row['game_more_info']) > 0){
                                                    $game_more_info = preg_replace("/(sub|app):([0-9]{1,6})/", "<a href='http://store.steampowered.com/$1/$2/' target='_blank'>".$keys_row['game_name']." <sub>(Neuer Tab)</sub></a>", $keys_row['game_more_info']);
                                                    echo "<i class='fa fa-steam fa-fw'></i> ".$game_more_info;
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <?
                                $raffles_participate = $db->query("SELECT * FROM raffles_participate WHERE userID = '".ID."'")->num_rows;
                                
                                if(isset($_POST['sub_raffle'])){
                                    if($raffles_participate == 0){
                                        $db->query("INSERT INTO raffles_participate (userID) VALUES ('".ID."')");
                                        header("Location:".SERVER_NAME."KeyRaffle");
                                    }else{
                                        echo bad("Nicht schummeln ;)");
                                    }
                                }

                                if(ONLINE == 1 && ADM == 0 && $raffles_participate == 0){
                                    echo "<form method='post'>";
                                        echo "<div class='form-group'>";
                                            echo "<div class='checkbox'>";
                                                echo "<label>";
                                                    echo "<input type='checkbox' data-switch-no-init required> Ich akzeptiere die Teilnahmebedingungen und habe dass Mindestalter von ".$keys_row['game_usk']." Jahren erreicht habe.";
                                                echo "</label>";
                                            echo "</div>";
                                        echo "</div>";
                                        echo "<div class='form-group'>";
                                            echo "<button type='submit' name='sub_raffle' class='btn btn-primary'>Am Gewinnspiel teilnehmen</button>";
                                        echo "</div>";
                                    echo "</form>";
                                }elseif(ONLINE == 0){
                                    echo info("Du musst <strong><a href='Login/Login'>Eingeloggt</a></strong> sein, um an diesem Gewinnspiel teilnehmen zu können.");
                                }elseif(ADM == 1){
                                    echo info("Du als Administrator darfst doch gar nicht teilnehmen ;)");
                                }elseif($raffles_participate >= 1){
                                    echo info("Du hast bereits an diesem Gewinnspiel teilgenommen.");
                                }
                            ?>
                        </div>

                        <div class="box">
                            <strong><u>Fragen oder Probleme mit deinem Steam-Key oder zur Verlosung?</u></strong>
                            <br><br>
                            <div class="box">
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Frage:</strong></div>
                                    <div class="col-md-10">
                                        Was mache ich, wenn mein <strong>Steam-Key ungültig</strong> ist?
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Antwort:</strong></div>
                                    <div class="col-md-10">
                                        Am besten du meldest dich unverzüglich bei uns per eMail unter <a href="mailto:support@chaosdidi.de?subject=Mein Steam-Key ist ungültig">support@chaosdidi.de</a>.<br>
                                        Bitte hinterlasse in deiner eMail folgende wichtige Informationen:<br>
                                        <ul>
                                            <li>Wie lautet <strong>dein Benutzername</strong>?</li>
                                            <li>Wann hast du deinen <strong>Key erhalten</strong>?</li>
                                            <li>Wie lautet der <strong>Code vom Key</strong>?</li>
                                        </ul>
                                        Wir werden uns dann bei dir melden, sobald wir eine Lösung gefunden haben :)
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Frage:</strong></div>
                                    <div class="col-md-10">
                                        <strong>Wie lange geht</strong> so <strong>eine Verlosung</strong> immer?
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Antwort:</strong></div>
                                    <div class="col-md-10">
                                        Eine von uns erstellte Verlosung, geht immer eine ganze Woche (Montag - Sonntag).<br>
                                        Jeden Montag zwischen 0:00 Uhr und 0:01 Uhr, wird ganz Zufällig ein Teilnehmer mit Hilfe eines Skriptes ausgesucht und über eine eMail Benachricht, ob dieser Gewonnen hat. Alle restlichen Teilnehmer, die nicht Gewonnen haben, erhalten ebenfalls eine entsprechende eMail, das diese nicht Gewonnen haben.
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Frage:</strong></div>
                                    <div class="col-md-10">
                                        Darf ich mein <strong>Steam-Key weitergeben</strong>?
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Antwort:</strong></div>
                                    <div class="col-md-10">
                                        Natürlich.<br>
                                        Wichtig ist, das du diesen Key noch NICHT auf Steam eingelöst hast.
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Frage:</strong></div>
                                    <div class="col-md-10">
                                        Darf ich mein <strong>Steam-Key verkaufen</strong>?
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 text-right"><strong>Antwort:</strong></div>
                                    <div class="col-md-10">
                                        Nein.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box">
                            <!-- COMMENT WRAPPER - START -->
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                /* * * CONFIGURATION VARIABLES * * */
                                var disqus_shortname = 'chaosdidi';
                                
                                /* * * DON'T EDIT BELOW THIS LINE * * */
                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the comments.</noscript>
                            <!-- COMMENT WRAPPER - END -->
                        </div>
                    </div>

                </div>
                <!-- CONTENT BODY - END -->
                
            </div>
        </section>
    </div>
    <!-- CONTENT - END  -->

   
   	<!-- FOOTER - START  -->
    <? require("assets/inc/footer.php"); ?>
    <!-- FOOTER - END  -->
   

    <!-- JS  -->
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/bootstrap-switch/bootstrap-switch.js"></script>
    <script src="assets/js/bootstrap-switch/highlight.js"></script>
    <script src="assets/js/bootstrap-switch/main.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/creative-brands.js"></script>
    <script src="assets/js/jquery.vegas.min.js"></script>
    <script src="assets/js/twitterFetcher_min.js"></script>
    <script src="assets/js/jquery.countdown.min.js"></script>
    <script src="assets/js/realtime_search/custom.js"></script>
    <script src="assets/js/jquery.amaran.js"></script>
    <script src="assets/js/detectmobilebrowser.js"></script>

    <script src="assets/js/INDEX-custom.js"></script>
    <script src="assets/js/hp_bg.js"></script>

    <script src="assets/js/pagination/jquery.pagination.js"></script>
    <script src="assets/js/pagination/custom.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43848786-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>