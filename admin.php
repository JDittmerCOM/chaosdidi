<? require("db.php"); ?>
<? if(ADM != '1' || ONLINE == '0'){header("Location:".SERVER_NAME);} ?>

<!DOCTYPE html>
<html>
<head>
    <base href="//chaosdidi.de/Admin"></base>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
    <meta name="description" content="Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen. Meist PS3 Spiele, aber auf Wunsch auch PC.">
    <? echo "<meta name='keywords' content='ChaosDidi, YouTube, Videos, PS3, PS4, PC, Stimmen, Spiele, ".META_TAGS."Games'>"; ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>ACP - ChaosDidi | Let's Plays</title>
        
    <!-- ==========================
    	Fonts 
    =========================== -->
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="assets/css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.vegas.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/js/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="assets/css/amaran.min.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
    	JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
	
    <!-- HEADER - START  -->
	<? require("assets/inc/topbar.php"); ?>
    <!-- HEADER - END  -->  
    

    <!-- TITLE - START  -->
    <div class="container hidden-xs">
    	<div class="header-title">
        	<div class="pull-left">
        		<h2><a href="./"><span class="text-primary">Chaos</span>Didi</a></h2>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <!-- TITLE - END  -->
    

    <!-- JUMBOTRON - START  -->
    <div class="container">
    	<div class="jumbotron jumbotron_social">
        	<div class="jumbotron-panel jumbotron_social">
            	<div class="panel panel-primary collapse-horizontal">
                    <div class="jumbotron-brands">
                    	<ul class="brands brands-sm brands-inline brands-circle">
                            <? echo "<li><a href='".SC_FACEBOOK."' target='_blank'><i class='fa fa-facebook'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_TWITTER."' target='_blank'><i class='fa fa-twitter'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_YOUTUBE."' target='_blank'><i class='fa fa-youtube'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_GPLUS."' target='_blank'><i class='fa fa-google-plus'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_STEAM."' target='_blank'><i class='fa fa-steam'></i></a></li>"; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- JUMBOTRON - END -->
    

    <!-- CONTENT - START  -->
    <div class="container">
        <section class="content-wrapper">
        	<div class="row">
            
            	<!-- SIDEBAR - START -->
                <div class="col-sm-3">
            	   <? require("assets/inc/admin/sidebar.php"); ?>
                </div>
                <!-- SIDEBAR - END -->
                
                <!-- CONTENT BODY - START -->
                <div class="col-sm-9">
                    
                	<?
                        if(!$_GET['s']){require("assets/inc/main/admin/main.php");}

                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'CDMembers'){require("assets/inc/main/admin/analytics/analytics_cdmembers.php");}
                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'CDVisitors'){require("assets/inc/main/admin/analytics/analytics_cdvisitors.php");}
                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'FBLikes'){require("assets/inc/main/admin/analytics/analytics_fblikes.php");}
                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'YTAbos'){require("assets/inc/main/admin/analytics/analytics_ytabos.php");}
                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'YTVids'){require("assets/inc/main/admin/analytics/analytics_ytvids.php");}
                        if($_GET['s'] == 'Analytics' && $_GET['site'] == 'YTCounts'){require("assets/inc/main/admin/analytics/analytics_ytcounts.php");}
                        
                        if($_GET['s'] == 'Raffles' && $_GET['site'] == 'All'){require("assets/inc/main/admin/raffles_all.php");}
                        if($_GET['s'] == 'Raffles' && $_GET['site'] == 'Create'){require("assets/inc/main/admin/raffles_create.php");}
                        if($_GET['s'] == 'Raffles' && $_GET['site'] == 'Single'){require("assets/inc/main/admin/raffles_single.php");}
                        
                        if($_GET['s'] == 'Bugs' && $_GET['site'] == 'All'){require("assets/inc/main/admin/bugs_all.php");}
                        if($_GET['s'] == 'Bugs' && $_GET['site'] == 'Single'){require("assets/inc/main/admin/bugs_single.php");}
                        
                        if($_GET['s'] == 'Posts' && $_GET['site'] == 'All'){require("assets/inc/main/admin/posts_all.php");}
                        if($_GET['s'] == 'Posts' && $_GET['site'] == 'Create'){require("assets/inc/main/admin/posts_create.php");}
                        if($_GET['s'] == 'Posts' && $_GET['site'] == 'CreateAPI'){require("assets/inc/main/admin/posts_createapi.php");}
                        if($_GET['s'] == 'Posts' && $_GET['site'] == 'Edit'){require("assets/inc/main/admin/posts_edit.php");}
                        
                        if($_GET['s'] == 'Comments' && $_GET['site'] == 'All'){require("assets/inc/main/admin/comments_all.php");}
                        
                        if($_GET['s'] == 'Playlists' && $_GET['site'] == 'All'){require("assets/inc/main/admin/playlists_all.php");}
                        if($_GET['s'] == 'Playlists' && $_GET['site'] == 'Edit'){require("assets/inc/main/admin/playlists_edit.php");}
                        
                        if($_GET['s'] == 'Gameslist' && $_GET['site'] == 'Edit'){require("assets/inc/main/admin/gameslist_edit.php");}
                        
                        if($_GET['s'] == 'Logs'){require("assets/inc/main/admin/logs.php");}
                        
                        if($_GET['s'] == 'Surveys' && $_GET['site'] == 'All'){require("assets/inc/main/admin/surveys_all.php");}
                        if($_GET['s'] == 'Surveys' && $_GET['site'] == 'Add'){require("assets/inc/main/admin/surveys_add.php");}
                        if($_GET['s'] == 'Surveys' && $_GET['site'] == 'Edit'){require("assets/inc/main/admin/surveys_edit.php");}
                        
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'All'){require("assets/inc/main/admin/members_all.php");}
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'User'){require("assets/inc/main/admin/members_single.php");}
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'Create'){require("assets/inc/main/admin/members_create.php");}
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'Activated'){require("assets/inc/main/admin/members_activated.php");}
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'NotUnlocked'){require("assets/inc/main/admin/members_notunlocked.php");}
                        if($_GET['s'] == 'Members' && $_GET['site'] == 'Closures'){require("assets/inc/main/admin/members_closures.php");}
                        
                        if($_GET['s'] == 'Wordfilter' && $_GET['site'] == 'Add'){require("assets/inc/main/admin/wordfilter_add.php");}
                        if($_GET['s'] == 'Wordfilter' && $_GET['site'] == 'Edit'){require("assets/inc/main/admin/wordfilter_edit.php");}
                        if($_GET['s'] == 'Wordfilter' && $_GET['site'] == 'Delete'){require("assets/inc/main/admin/wordfilter_delete.php");}
                    ?> 

                </div>
                <!-- CONTENT BODY - END -->
                
            </div>
        </section>
    </div>
    <!-- CONTENT - END  -->

   
   	<!-- FOOTER - START  -->
    <? require("assets/inc/footer.php"); ?>
    <!-- FOOTER - END  -->
   

   	<!-- JS  -->        
	<script src="assets/js/jquery-1.11.2.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/creative-brands.js"></script>
    <script src="assets/js/jquery.vegas.min.js"></script>
    <script src="assets/js/filtertable/jquery.filtertable.js"></script>
    <script src="assets/js/realtime_search/custom.js"></script>
    <script src="assets/js/realtime_create_raffle/custom.js"></script>
    <script src="assets/js/jquery.amaran.js"></script>
    <script src="assets/js/detectmobilebrowser.js"></script>

    <script src="assets/js/moment.js"></script>
    <script src="assets/js/datetimepicker/bootstrap-datetimepicker.js"></script>

    <script src="assets/js/ADMIN-custom.js"></script>
    <script src="assets/js/hp_bg.js"></script>

</body>
</html>