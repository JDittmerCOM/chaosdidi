<? require("db.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <base href="//chaosdidi.de"></base>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
    <meta name="description" content="Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen. Meist PS3 Spiele, aber auf Wunsch auch PC.">
    <? echo "<meta name='keywords' content='ChaosDidi, YouTube, Videos, PS3, PS4, PC, Stimmen, Spiele, ".META_TAGS."Games'>"; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>ChaosDidi | Let's Plays</title>
        
    <!-- ==========================
    	Fonts 
    =========================== -->
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/bootstrap-switch.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="assets/css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.vegas.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/amaran.min.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
    	JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
	
    <!-- HEADER - START  -->
	<? require("assets/inc/topbar.php"); ?>
    <!-- HEADER - END  -->  
    

    <!-- TITLE - START  -->
    <div class="container hidden-xs">
    	<div class="header-title">
        	<div class="pull-left">
        		<h2><a href="./"><span class="text-primary">Chaos</span>Didi</a></h2>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <!-- TITLE - END  -->
    

    <!-- JUMBOTRON - START  -->
    <div class="container">
    	<div class="jumbotron jumbotron_social">
        	<div class="jumbotron-panel jumbotron_social">
            	<div class="panel panel-primary collapse-horizontal">
                    <div class="jumbotron-brands">
                    	<ul class="brands brands-sm brands-inline brands-circle">
                            <? echo "<li><a href='".SC_FACEBOOK."' target='_blank'><i class='fa fa-facebook'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_TWITTER."' target='_blank'><i class='fa fa-twitter'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_YOUTUBE."' target='_blank'><i class='fa fa-youtube'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_GPLUS."' target='_blank'><i class='fa fa-google-plus'></i></a></li>"; ?>
                            <? echo "<li><a href='".SC_STEAM."' target='_blank'><i class='fa fa-steam'></i></a></li>"; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- JUMBOTRON - END -->
    

    <!-- CONTENT - START  -->
    <div class="container">
        <section class="content-wrapper">
        	<div class="row">
            
            	<!-- SIDEBAR - START -->
            	<? require("assets/inc/sidebar.php"); ?>
                <!-- SIDEBAR - END -->
                
                <!-- CONTENT BODY - START -->
                <div class="col-sm-8">
                    
                	<?
                        if(!$_GET['s']){require("assets/inc/main/main.php");}
                        if($_GET['s'] == 'Post'){require("assets/inc/main/post.php");}
                        if($_GET['s'] == 'Raffle'){require("assets/inc/main/raffle.php");}
                        if($_GET['s'] == 'Event'){require("assets/inc/main/event.php");}
                        if($_GET['s'] == 'Playlist'){require("assets/inc/main/playlist.php");}
                        if($_GET['s'] == 'Gameslist'){require("assets/inc/main/gameslist.php");}
                        if($_GET['s'] == 'Surveys'){require("assets/inc/main/surveys.php");}
                        if($_GET['s'] == 'Wishbox'){require("assets/inc/main/wishbox.php");}
                        if($_GET['s'] == 'Members'){require("assets/inc/main/members.php");}
                        if($_GET['s'] == 'MyProfile'){require("assets/inc/main/myprofile.php");}
                        if($_GET['s'] == 'Search'){require("assets/inc/main/search.php");}
                        if($_GET['s'] == 'Tag'){require("assets/inc/main/tag.php");}

                        if($_GET['s'] == 'Balance'){require("assets/inc/main/balance.php");}
                        if($_GET['s'] == 'Notifications'){require("assets/inc/main/notifications.php");}
                        if($_GET['s'] == 'PN'){require("assets/inc/main/pn.php");}

                        if($_GET['s'] == 'Activate'){require("assets/inc/main/activate.php");}
                        if($_GET['s'] == 'LoginActivate'){require("assets/inc/main/login_activate.php");}
                        if($_GET['s'] == 'Bug'){require("assets/inc/main/bug.php");}

                        if($_GET['s'] == 'Imprint'){require("assets/inc/main/imprint.php");}
                        if($_GET['s'] == 'Dpd'){require("assets/inc/main/dpd.php");}
                        if($_GET['s'] == 'Licenses'){require("assets/inc/main/licenses.php");}

                        if($_GET['s'] == 'Login' && $_GET['site'] == 'Login'){require("assets/inc/main/login/login.php");}
                        if($_GET['s'] == 'Login' && $_GET['site'] == 'Register'){require("assets/inc/main/login/register.php");}
                        if($_GET['s'] == 'Login' && $_GET['site'] == 'Logout'){require("assets/inc/main/login/logout.php");}
                    ?> 

                </div>
                <!-- CONTENT BODY - END -->
                
            </div>
        </section>
    </div>
    <!-- CONTENT - END  -->

   
   	<!-- FOOTER - START  -->
    <? require("assets/inc/footer.php"); ?>
    <!-- FOOTER - END  -->
   

   	<!-- JS  -->
	<script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/bootstrap-switch/bootstrap-switch.js"></script>
    <script src="assets/js/bootstrap-switch/highlight.js"></script>
    <script src="assets/js/bootstrap-switch/main.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/creative-brands.js"></script>
    <script src="assets/js/jquery.vegas.min.js"></script>
    <script src="assets/js/twitterFetcher_min.js"></script>
    <script src="assets/js/jquery.countdown.min.js"></script>
    <script src="assets/js/realtime_search/custom.js"></script>
    <script src="assets/js/realtime_conversation_username_search/custom.js"></script>
    <script src="assets/js/jquery.amaran.js"></script>
    <script src="assets/js/detectmobilebrowser.js"></script>
    
    <script src="assets/js/INDEX-custom.js"></script>
    <script src="assets/js/hp_bg.js"></script>

    <script src="assets/js/pagination/jquery.pagination.js"></script>
    <script src="assets/js/pagination/custom.js"></script>

    <!-- Masonry [start] -->
    <script src="assets/js/masonry/masonry.pkgd.min.js"></script>
    <script src="assets/js/masonry/imagesloaded.pkgd.min.js"></script>
    <script>
        $(document).ready(function () {
            var $container = $('.masonry');

            $container.imagesLoaded(function () {
                $container.masonry({
                    itemSelector: '.masonry-item',
                    columnWidth: '.masonry-item',
                    transitionDuration: 0
                });
            });
        });
    </script>
    <!-- Masonry [end] -->

    <!-- CKEditor [start] -->
    <script src="assets/js/ckeditor/ckeditor.js"></script>
    <script src="assets/js/ckeditor/adapters/jquery.js"></script>
    <script>$('textarea').ckeditor();</script>
    <!-- CKEditor [end] -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43848786-1', 'auto');
      ga('send', 'pageview');
    </script>

    <!-- EASTER EGG [start] -->
    <script type="text/javascript" src="https://cdn.rawgit.com/mikeflynn/egg.js/master/egg.min.js"></script>
    <style type="text/css">.audioEgg {display: none;}</style>
    <audio class="audioEgg" controls preload="none">
       <source src="assets/sounds/easter_egg.wav" type="audio/wav">
    </audio>
    <script>
        var egg = new Egg("up,up,down,down,left,right,left,right,b,a", function() {
          $(".audioEgg").trigger('load');
          $(".audioEgg").trigger('play');
        }).listen();
    </script>
    <!-- EASTER EGG [end] -->
</body>
</html>