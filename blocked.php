<? require("db.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <base href="//chaosdidi.de"></base>
    <!-- ==========================
        Meta Tags 
    =========================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- ==========================
        Title 
    =========================== -->
    <title>ChaosDidi</title>
        
    <!-- ==========================
        Fonts 
    =========================== -->
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- ==========================
        CSS 
    =========================== -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="assets/css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.vegas.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
        JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
	
    <!-- CONTENT - START  -->
    <div class="container">
        <section class="content-wrapper">
        	<div class="box">
                            
                <!-- ERROR - START -->
                <div class="error">
                	<h2>Blockierte IP-Adresse</h2>
                	<p>
                        Deine IP-Adresse wurde von ChaosDidi.de blockiert.<br>
                        Falls dies ein Missverständnis gewesen sein sollte, dann kontaktiere bitte den Support und schildere dein Problem.
                    </p>
                	<a href="mailto:support@chaosdidi.de" class="btn btn-primary btn-lg">Support kontaktieren</a>
                </div>
                <!-- ERROR - END -->
                
            </div>
        </section>
    </div>
    <!-- CONTENT - END  -->

   
    <!-- FOOTER - START  -->
    <? require("assets/inc/footer.php"); ?>
    <!-- FOOTER - END  -->

   
   	<!-- JS  -->   
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-43848786-1', 'auto');
      ga('send', 'pageview');
    </script>

	<script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/creative-brands.js"></script>
    <script src="assets/js/jquery.vegas.min.js"></script>
    <script src="assets/js/twitterFetcher_min.js"></script>
    <script src="assets/js/jquery.countdown.min.js"></script>
    <script src="assets/js/detectmobilebrowser.js"></script>
    
    <script src="assets/js/INDEX-custom.js"></script>
</body>
</html>